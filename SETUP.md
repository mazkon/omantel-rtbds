# DB2

## Increase LOGFILSIZ db parameter to prevent too fast fill

`LOGFILSIZ` was changed to value *65536*

Use the following procedure to increase the size of the DB2 transaction log (logfilsiz):

Determine the current log file size setting by issuing the command:

```
su - <db2instance>  

db2 list db directory # to list the database name  

db2 connect to <databaseName>  

db2 get db config for <databaseName> | grep -i logfilsiz  
```

Example:

```
su - icmadmin  

db2 connect to icmnlsdb  

db2 get db config for icmnlsdb | grep -i logfilsiz  
```

Increase the size of the log file size setting by issuing the command:

```
db2 UPDATE db cfg for <databaseName> using LOGFILSIZ <new_value>  
```

Example:

```
db2 UPDATE db cfg for icmlsdb using LOGFILSIZ 5000  
```

Issue the commands:

```
db2 force applications all  

db2stop force  

db2start  
```

## Check tablespace utilization

```
select 
substr(tbsp_name,1,30) as Tablespace_Name, 
tbsp_type as Type, 
substr(tbsp_state,1,20) as Status, 
(tbsp_total_size_kb / 1024 ) as Size_Meg, 
decimal((float(tbsp_total_size_kb - tbsp_free_size_kb)/ float(tbsp_total_size_kb))*100,4,1) as Percent_used_Space, 
bigint((tbsp_free_size_kb) / 1024 )as Meg_Free_Space 
from sysibmadm.tbsp_utilization where tbsp_name='TSN_REG_RTBDS'
```


# DS Project

## Project specific user envinronment variables

Required variables: SMTP, SENDER_EMAIL, RECIPIENTS_EMAIL, DB_NAME, DB_USER, DB_PASS

![view](https://bytebucket.org/tuatarapl/omantel-rtbds/raw/6940499c668beeb084ec103ff6405c9ffef200a6/doc/img/project_envinronment_variables_user_definied.png)
