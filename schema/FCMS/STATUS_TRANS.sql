/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'FCMS';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema FCMS'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = FCMS;

/*
    Attention: No PK here
*/

CREATE TABLE FCMS.STATUS_TRANS (
  OBJECTID	VARCHAR(8),
  STATUSSEQ	INTEGER,
  STATUSCODE	INTEGER,
  STATUSDATE	TIMESTAMP,
  STATUSTIME	TIME,
  ACTIONTO	INTEGER,
  ACTIVESTATUS	INTEGER,
  ACTIONDESC	VARCHAR(32000),
  USERID	INTEGER,
  ACTIONFROM	INTEGER,
  ACTIONTOOLD	INTEGER,
  USERIDOLD	INTEGER,
  ACTIONFROMOLD	INTEGER,
  OBJECTIDOLD	INTEGER,
  STATUSCODEOLD	INTEGER,
  USERNAMEOLD	VARCHAR(10)
  ) 
  ORGANIZE BY ROW;

ALTER TABLE FCMS.STATUS_TRANS
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMENT ON FCMS.STATUS_TRANS ( 
	OBJECTID IS 'Complaint Numer',
	STATUSSEQ IS 'Action initiated sequence Number',
	STATUSCODE IS 'Status code Id',
	STATUSDATE IS 'Action initiated date on complaint',
	STATUSTIME IS 'Action initiated time on complaint',
	ACTIONTO IS 'Assigned ThirdLine groupid',
	ACTIONDESC IS 'Action comments',
	USERID IS 'Action initiated Staff  Userid',
	ACTIONFROM IS 'Action initiated Staff GroupId' );

COMMIT;

RUNSTATS ON TABLE FCMS.STATUS_TRANS
	ALLOW WRITE ACCESS;

COMMIT;

