/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'FCMS';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema FCMS'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = FCMS;

CREATE TABLE FCMS.STATUS_MASTER (
  STATUSCODE	INTEGER	NOT NULL,
  STATUSDESC	VARCHAR(100),
  STATUSALIAS	VARCHAR(100),
  CONSTRAINT PK_STATUS_MASTER PRIMARY KEY (STATUSCODE)
  ) 
  ORGANIZE BY ROW;

ALTER TABLE FCMS.STATUS_MASTER
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMENT ON FCMS.STATUS_MASTER ( 
	STATUSCODE IS 'StatusCode Id',
	STATUSDESC IS 'StatusCode Description',
	STATUSALIAS IS 'AliasName of StatusCode' );

COMMIT;

RUNSTATS ON TABLE FCMS.STATUS_MASTER
	ALLOW WRITE ACCESS;

COMMIT;