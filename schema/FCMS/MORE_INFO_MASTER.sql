/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'FCMS';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema FCMS'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = FCMS;

CREATE TABLE FCMS.MORE_INFO_MASTER (
  ID	BIGINT	NOT NULL,
  FEEDBACKTYPEID	BIGINT,
  SEQNO	INTEGER,
  FIELDNAME	VARCHAR(100),
  FIELDLABEL	VARCHAR(100),
  FIELDTYPE	VARCHAR(10),
  ISMANDATORY	INTEGER,
  FEEDBACKDESCID	BIGINT,
  CONSTRAINT PK_MORE_INFO_MASTER PRIMARY KEY (ID)
  ) 
  ORGANIZE BY ROW;

ALTER TABLE FCMS.MORE_INFO_MASTER
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMENT ON FCMS.MORE_INFO_MASTER ( 
	ID IS 'Row number',
	FEEDBACKTYPEID IS 'FeedbackType Id',
	SEQNO IS 'Order Number of Moreinfo field',
	FIELDNAME IS 'MoreInfo Field Name',
	FIELDLABEL IS 'MoreInfo Label Name',
	FIELDTYPE IS 'More Info field Type',
	ISMANDATORY IS 'Binary code of Mandatory Fields',
	FEEDBACKDESCID IS 'feedbackdescription id' );

COMMIT;

RUNSTATS ON TABLE FCMS.MORE_INFO_MASTER
	ALLOW WRITE ACCESS;

COMMIT;