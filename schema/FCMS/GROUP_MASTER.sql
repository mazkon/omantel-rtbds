/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'FCMS';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema FCMS'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = FCMS;

CREATE TABLE FCMS.GROUP_MASTER (
  GROUPID	INTEGER	NOT NULL,
  GROUPNAME	VARCHAR(255),
  SECTIONID	INTEGER,
  GROUPTYPE	INTEGER,
  TYPE	INTEGER,
  SUBGROUPOF	INTEGER,
  CONSTRAINT PK_GROUP_MASTER PRIMARY KEY (GROUPID)
  ) 
  ORGANIZE BY ROW;

ALTER TABLE FCMS.GROUP_MASTER
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMENT ON FCMS.GROUP_MASTER ( 
	GROUPID IS 'Group Id',
	GROUPNAME IS 'Name of Group',
	SECTIONID IS 'Section Id of Group',
	TYPE IS 'Typeid of group belongs to (Ex:- Mobile,Fixed Internet)',
	SUBGROUPOF IS 'ParentGroup Id' );

COMMIT;

RUNSTATS ON TABLE FCMS.GROUP_MASTER
	ALLOW WRITE ACCESS;

COMMIT;