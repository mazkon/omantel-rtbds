/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'FCMS';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema FCMS'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = FCMS;

CREATE TABLE FCMS.TICKET_DETAILS (
  TICKETNO	VARCHAR(8)	NOT NULL,
  FEEDBACKTYPEID	INTEGER,
  FEEDBACKTYPE	VARCHAR(255),
  FEEDBACKDESCID	BIGINT,
  FEEDBACKDESCRIPTION	VARCHAR(25),
  GROUPID	INTEGER,
  GROUPNAME	VARCHAR(255),
  STATUSCODE	INTEGER,
  STATUSDESC	VARCHAR(100),
  NAME	VARCHAR(100),
  POBOX	INTEGER,
  PCODE	INTEGER,
  MSISDN	VARCHAR(11),
  CONTACTNO	VARCHAR(11),
  FAX	VARCHAR(11),
  EMAIL	VARCHAR(100),
  INITIATOR	VARCHAR(20),
  FEEDBACKDETAILS	VARCHAR(32000),
  RECVCHANNEL	VARCHAR(20),
  PREFERREDCOMMUNICATIONLANGUAGE	VARCHAR(10),
  PREFERREDCOMMUNICATIONCHANNEL	VARCHAR(20),
  CREATEDON	TIMESTAMP,
  CREATEDTIME	TIME,
  REGISTEREDBY	INTEGER,
  OWNERID	INTEGER,
  BCKOWNER	VARCHAR(10),
  COMMENTS	VARCHAR(255),
  PARENTBRANCH	INTEGER,
  PARENTBRANCHNAME	VARCHAR(20),
  REGIONID	INTEGER,
  REGION	VARCHAR(20),
  ROOTCAUSE	INTEGER,
  CONTYPE	VARCHAR(10),
  VIPFLAG	INTEGER,
  CLOSESTATUS	VARCHAR(255),
  COMPLAINTTYPE	INTEGER,
  TICKETSTATUS	VARCHAR(100),
  GENUINESTATUS	VARCHAR(20),
  CUSTOMERTYPE	INTEGER,
  ACCOUNTNO	INTEGER,
  INPROCESS	VARCHAR(10),
  CATEGORY	VARCHAR(20),
  GRANDSLA	INTEGER,
  SECONDLINEOLA	INTEGER,
  THIRDLINEOLA	INTEGER,
  FOURTLINEOLA	INTEGER,
  CLOSINGOLA	INTEGER,
  TRASLA	INTEGER,
  CATEGORYID	INTEGER,
  CHANNELID	INTEGER,
  SERVICEID	INTEGER,
  PROVIDERID	INTEGER,
  MEDIAID	INTEGER,
  BRANCHID	INTEGER,
  AREANAME	VARCHAR(50),
  EXPR1	VARCHAR(10),
  EXC_CODE	VARCHAR(10),
  OBC	VARCHAR(10),
  CR_NUMBER	VARCHAR(10),
  CONSTRAINT PK_TICKET_DETAILS PRIMARY KEY (TICKETNO)
  ) 
  ORGANIZE BY ROW;

ALTER TABLE FCMS.TICKET_DETAILS
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMENT ON FCMS.TICKET_DETAILS ( 
	TICKETNO IS 'Complaint Number',
	FEEDBACKTYPEID IS 'FeedbackType Id',
	FEEDBACKTYPE IS 'Feedback Type Name',
	FEEDBACKDESCID IS 'FeedbackDescription Id of complaint',
	FEEDBACKDESCRIPTION IS 'feedbackdescription Name',
	GROUPID IS 'Group Id',
	GROUPNAME IS 'Name of Group',
	STATUSCODE IS 'Present Statuscode of Complaint',
	STATUSDESC IS 'StatusCode Description',
	NAME IS 'Name of the Customer',
	POBOX IS 'POBOX of Customer',
	PCODE IS 'PCode of Customer',
	MSISDN IS 'MSISDN of Customer',
	CONTACTNO IS 'Contact Number of Customer',
	FAX IS 'Fax of Customer',
	EMAIL IS 'Email of Customer',
	INITIATOR IS 'Complaint Registered Group',
	FEEDBACKDETAILS IS 'customer problem details',
	RECVCHANNEL IS 'Channel of complaint raised',
	PREFERREDCOMMUNICATIONLANGUAGE IS 'Preferred Communication Language of Customer',
	PREFERREDCOMMUNICATIONCHANNEL IS 'Preferred Communication Channel of Customer',
	CREATEDON IS 'Complaint Registered Date',
	CREATEDTIME IS 'Complaint Registered Time',
	REGISTEREDBY IS 'Userid of FirstLine User who registered the complaint',
	OWNERID IS 'Userid of SecondLine User who own complaint',
	COMMENTS IS 'resolve or reply comments by ThirdLine or Fourthline Users',
	PARENTBRANCH IS 'AreaID of Current complaint Number',
	PARENTBRANCHNAME IS 'Parent branch name',
	REGIONID IS 'RegionID of Current complaint Number',
	REGION IS 'Region name',
	ROOTCAUSE IS 'RootcauseId selected by ThirdLine User while reply/resolve',
	VIPFLAG IS 'Vipstatus of Customer',
	CLOSESTATUS IS 'Rootcause selected by SecondLine User while closing complaint',
	TICKETSTATUS IS 'Customer Satisfaction details selected by SecondLine User while closing complaint',
	GENUINESTATUS IS 'Genuinity of complaint selected by SecondLine User while closing complaint',
	CUSTOMERTYPE IS 'Customertype (Consumer/Corporate)',
	ACCOUNTNO IS 'AccountNumber of Customer',
	CATEGORY IS 'Category of Customer',
	GRANDSLA IS 'Target SLA (Business Hours Only)',
	SECONDLINEOLA IS 'Traget Secondline OLA  (Business Hours Only)',
	THIRDLINEOLA IS 'Traget Thirdline OLA  (Business Hours Only)',
	FOURTLINEOLA IS 'Traget Fourthline OLA  (Business Hours Only)',
	CLOSINGOLA IS 'Traget closingline OLA  (Business Hours Only)',
	TRASLA IS 'Target TRA SLA',
	AREANAME IS 'Exchange Name of Customer Number',
	EXC_CODE IS 'Exchange code of Customer Number',
	OBC IS 'OBC Tag detail of Customer Number' );

COMMIT;

RUNSTATS ON TABLE FCMS.TICKET_DETAILS
	ALLOW WRITE ACCESS;

COMMIT;