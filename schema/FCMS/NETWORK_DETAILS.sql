/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'FCMS';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema FCMS'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = FCMS;

CREATE TABLE FCMS.NETWORK_DETAILS (
  TICKETID	VARCHAR(10) NOT NULL,
  MSISDN	VARCHAR(11),
  MDF_VERTICAL	VARCHAR(20),
  PPAIR_NUMBER	VARCHAR(10),
  CABINET	VARCHAR(10),
  PRIMARY_CABLE	VARCHAR(10),
  SECONDARY_CABLE	VARCHAR(10),
  DP_NUMBER	VARCHAR(10),
  SPAIR_NUMBER	VARCHAR(10),
  BSW	VARCHAR(10),
  SBSWPAIR	VARCHAR(10),
  HOUSE_NO	VARCHAR(20),
  WAY_NO	VARCHAR(10),
  CONSTRAINT PK_NETWORK_DETAILS PRIMARY KEY (TICKETID)
  ) 
  ORGANIZE BY ROW;

ALTER TABLE FCMS.NETWORK_DETAILS
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMENT ON FCMS.NETWORK_DETAILS ( 
	TICKETID IS 'Complaint Number',
	MSISDN IS 'Customer Complaint Number',
	MDF_VERTICAL IS 'MDF Vertical Number of Customer Number',
	PPAIR_NUMBER IS 'Primary Cable Pair Number of Customer Number',
	CABINET IS 'Cabinet of of Customer Number',
	PRIMARY_CABLE IS 'Primar Cable Number of Customer Number',
	SECONDARY_CABLE IS 'Secondary Cable Number of Customer Number',
	DP_NUMBER IS 'DP Number  of Customer Number',
	SPAIR_NUMBER IS 'Secondary Cable Pair Number of Customer Number',
	BSW IS 'BSW Number of Customer Number',
	SBSWPAIR IS 'BSW Pair Number of Customer Number',
	HOUSE_NO IS 'House Number of Customer',
	WAY_NO IS 'Way Number of Customer' );

COMMIT;

RUNSTATS ON TABLE FCMS.NETWORK_DETAILS
	ALLOW WRITE ACCESS;

COMMIT;