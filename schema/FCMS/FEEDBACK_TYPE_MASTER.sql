/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'FCMS';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema FCMS'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = FCMS;

CREATE TABLE FCMS.FEEDBACK_TYPE_MASTER (
  FEEDBACKTYPEID	INTEGER	NOT NULL,
  FEEDBACKTYPE	VARCHAR(255),
  STATUS	INTEGER,
  FEEDBACKCLASSID	INTEGER,
  CUSTOMERTYPE	INTEGER,
  CONSTRAINT PK_FEEDBACK_TYPE_MASTER PRIMARY KEY (FEEDBACKTYPEID)
  ) 
  ORGANIZE BY ROW;

ALTER TABLE FCMS.FEEDBACK_TYPE_MASTER
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMENT ON FCMS.FEEDBACK_TYPE_MASTER ( 
	FEEDBACKTYPEID IS 'FeedbackType Id',
	FEEDBACKTYPE IS 'Feedback Type Name',
	STATUS IS 'Status of  FeedbackType(Enable/Disable)',
	FEEDBACKCLASSID IS 'TypeId of Feedback(Ex:- Mobile,Fixed Internet)',
	CUSTOMERTYPE IS 'Type of FeedbackType Delas with based on CustomerType' );

COMMIT;

/* CAUTION: this alter create PK in strange way - in some situations like create FK this PK may be invisible 
ALTER TABLE FCMS.FEEDBACK_TYPE_MASTER
  ADD CONSTRAINT PK_FEEDBACK_TYPE_MASTER PRIMARY KEY
    (FEEDBACKTYPEID)
    NOT ENFORCED;

COMMIT;
*/

RUNSTATS ON TABLE FCMS.FEEDBACK_TYPE_MASTER
	ALLOW WRITE ACCESS;

COMMIT;