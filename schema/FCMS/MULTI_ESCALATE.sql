/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'FCMS';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema FCMS'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = FCMS;

CREATE TABLE FCMS.MULTI_ESCALATE (
  TICKETNO	VARCHAR(10)	NOT NULL,
  SEQNO	INTEGER NOT NULL,
  SLSOWNER	INTEGER,
  BCKOWNER	INTEGER,
  STATUSCODE	INTEGER,
  ACTIVESTATUS	INTEGER,
  ESCALATEDGROUP	INTEGER,
  BRANCHID	INTEGER,
  FRLOWNER	INTEGER,
  FRLSTATUSCODE	INTEGER,
  FRLGROUP	INTEGER,
  BCKOLA	INTEGER,
  FRLOLA	INTEGER,
  AREAID	INTEGER,
  SLESCDATE	TIMESTAMP,
  TLESCDATE	TIMESTAMP,
  TLREPDATE	TIMESTAMP,
  FRLREPDATE	TIMESTAMP,
  SLSOWNEROLD	INTEGER,
  BCKOWNEROLD	INTEGER,
  ESCGRPOLD	INTEGER,
  BRANCHIDOLD	INTEGER,
  TICKETNOOLD	INTEGER,
  STATUSCODEOLD	INTEGER,
  ONHOLDDATE	TIMESTAMP,
  RELEASEDATE	TIMESTAMP,
  PRESTATUSCODE	INTEGER,
  RETURNDATE	TIMESTAMP,
  CONSTRAINT PK_MULTI_ESCALATE PRIMARY KEY (TICKETNO, SEQNO)
  ) 
  ORGANIZE BY ROW;

ALTER TABLE FCMS.MULTI_ESCALATE
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMENT ON FCMS.MULTI_ESCALATE ( 
	TICKETNO IS 'Complaint Numer',
	SEQNO IS 'Number of Times Assigned',
	SLSOWNER IS 'Assigned SecondLine Staff UserId',
	BCKOWNER IS 'ThirdLine Staff UserId who processing that complaint',
	STATUSCODE IS 'Action Code in ThirdLine',
	ACTIVESTATUS IS 'Active /In Active Status',
	ESCALATEDGROUP IS 'GroupId of ThirdLine who processing complaint',
	BRANCHID IS 'BranchId of Customer Number',
	FRLOWNER IS 'FourthLine Staff UserId who processing that complaint',
	FRLSTATUSCODE IS 'Action Code in FourthLine',
	FRLGROUP IS 'GroupId of FourthLine who processing complaint',
	BCKOLA IS 'Time Taken By ThirdLine(in minutes) for Reply/Resolve',
	FRLOLA IS 'Time Taken By FourthLine(in minutes) for Reply/Resolve',
	AREAID IS 'AreaId of Customer Number',
	SLESCDATE IS 'Assigned Date',
	TLESCDATE IS 'ThirdLIne Assigned Date',
	TLREPDATE IS 'ThirdLIne Reply/Resolve Date',
	FRLREPDATE IS 'FourthLIne Reply/Resolve Date' );

COMMIT;

RUNSTATS ON TABLE FCMS.MULTI_ESCALATE
	ALLOW WRITE ACCESS;

COMMIT;