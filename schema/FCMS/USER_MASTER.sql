/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'FCMS';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema FCMS'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = FCMS;

CREATE TABLE FCMS.USER_MASTER (
  USERID	INTEGER	NOT NULL,
  USERNAME	VARCHAR(20),
  PASSWORD	VARCHAR(50),
  DISPLAYNAME	VARCHAR(100),
  FIRSTNAME	VARCHAR(100),
  LASTNAME	VARCHAR(100),
  WORKTITLE	VARCHAR(50),
  EMAIL	VARCHAR(100),
  SECTIONID	INTEGER,
  GROUPID	INTEGER,
  ROLEID	INTEGER,
  BRANCHID	INTEGER,
  PROBLEMCLASS	INTEGER,
  MOBILE	VARCHAR(50),
  USERTYPE	INTEGER,
  AREAID	INTEGER,
  USERIDOLD	INTEGER,
  USERNAMEOLD	VARCHAR(50),
  GROUPIDOLD	INTEGER,
  BRANCHIDOLD	INTEGER,
  CONSTRAINT PK_USER_MASTER PRIMARY KEY (USERID)
  ) 
  ORGANIZE BY ROW;

ALTER TABLE FCMS.USER_MASTER
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMENT ON FCMS.USER_MASTER ( 
	USERID IS 'Userid Of Staff',
	USERNAME IS 'LoginName of Staff',
	DISPLAYNAME IS 'Name of Staff',
	FIRSTNAME IS 'First Name of Staff',
	LASTNAME IS 'Last Name of Staff',
	WORKTITLE IS 'Designation of Staff',
	EMAIL IS 'EmailId of Staff',
	SECTIONID IS 'Section of staff',
	GROUPID IS 'GroupId of Staff',
	ROLEID IS 'RoleId of Staff',
	BRANCHID IS 'Branch Id of Staff',
	PROBLEMCLASS IS 'UserPermissionCode',
	MOBILE IS 'Mobile Number of Staff',
	USERTYPE IS 'Type of Complaints Delas with based on CustomerType',
	AREAID IS 'Area Id of Staff' );

COMMIT;

RUNSTATS ON TABLE FCMS.USER_MASTER
	ALLOW WRITE ACCESS;

COMMIT;

