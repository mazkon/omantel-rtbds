/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'FCMS';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema FCMS'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = FCMS;

CREATE TABLE FCMS.DETAILED_TICKET_MASTER (
  TICKETNO	VARCHAR(10)	NOT NULL,
  CREATEDDATE	TIMESTAMP	NOT NULL,
  RECEIVEDDATE	TIMESTAMP,
  SLUSERID	INTEGER,
  SLGROUPID	INTEGER,
  RECENTESCDATE	TIMESTAMP,
  TLRECVDATE	TIMESTAMP,
  TLUSERID	INTEGER,
  TLGROUPID	INTEGER,
  RECENTREPLYDATE	TIMESTAMP,
  FRLRECVDATE	TIMESTAMP,
  FRLUSERID	INTEGER,
  FRLGROUPID	INTEGER,
  CLOSEDUSERID	INTEGER,
  CLOSEDGROUPID	INTEGER,
  CLOSEDDATE	TIMESTAMP,
  REOPENDATE	TIMESTAMP,
  SECONDLINEOLA	INTEGER,
  THIRDLINEOLA	INTEGER,
  FOURTHLINEOLA	INTEGER,
  GRANDSLA	INTEGER,
  PRESENTSTATUSCODE	INTEGER,
  DETAILSUPDATEDON	TIMESTAMP,
  REGBY	INTEGER,
  REGBYGROUPID	INTEGER,
  ONHOLDDATE	TIMESTAMP,
  RELEASEDATE	TIMESTAMP,
  PREVSTATUSCODE	INTEGER,
  HOLDRELEASEOLA	INTEGER,
  CONSTRAINT PK_DETAILED_TICKET_MASTER PRIMARY KEY (TICKETNO)
  ) 
  ORGANIZE BY ROW;

ALTER TABLE FCMS.DETAILED_TICKET_MASTER
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMENT ON FCMS.DETAILED_TICKET_MASTER ( 
	TICKETNO IS 'Complaint Numer',
	CREATEDDATE IS 'Complaint Registered Date',
	RECEIVEDDATE IS 'Complaint Received Date by Secondline Staff',
	SLUSERID IS 'Userid of SecondLine User who own complaint',
	SLGROUPID IS 'Groupid of SecondLine User who own complaint',
	RECENTESCDATE IS 'Recent Assign date  by secondline Staff',
	TLRECVDATE IS 'Complaint Received Date by Thirdline Staff',
	TLUSERID IS 'Userid of Thirdline User who own complaint',
	TLGROUPID IS 'Groupid of Thirdline User who own complaint',
	RECENTREPLYDATE IS 'Recent Replied/Resolved date  by Thirdline Staff',
	FRLRECVDATE IS 'Recent Replied/Resolved date  by Fourthline Staff',
	FRLUSERID IS 'Userid of Fourthline User who own complaint',
	FRLGROUPID IS 'Groupid of Fourthline User who own complaint',
	CLOSEDUSERID IS 'Userid of Secondline User who Closed complaint',
	CLOSEDGROUPID IS 'Groupid of Secondline User who Closed complaint',
	CLOSEDDATE IS 'Complaint Closed Date',
	REOPENDATE IS 'Complaint Reopened Date after Closing',
	SECONDLINEOLA IS 'Total Time Taken By SecondLine(in minutes)',
	THIRDLINEOLA IS 'Total  Time Taken By ThirdLine(in minutes)',
	FOURTHLINEOLA IS 'Total  Time Taken By FourthLine(in minutes)',
	GRANDSLA IS 'Total Time Taken By All Lines(in minutes)',
	PRESENTSTATUSCODE IS 'Current Status Code of Complaint',
	DETAILSUPDATEDON IS 'Complaint details Updated Date',
	REGBY IS 'Userid of FirstLine User who registered the complaint',
	REGBYGROUPID IS 'Groupid of FirstLine User who registered the complaint' );

COMMIT;

/* CAUTION: this alter create PK in strange way - in some situations like create FK this PK may be invisible
ALTER TABLE FCMS.DETAILED_TICKET_MASTER
  ADD CONSTRAINT PK_DETAILED_TICKET_MASTER PRIMARY KEY
    (TICKETNO)
    NOT ENFORCED;

COMMIT;
*/

RUNSTATS ON TABLE FCMS.DETAILED_TICKET_MASTER
	ALLOW WRITE ACCESS;

COMMIT;