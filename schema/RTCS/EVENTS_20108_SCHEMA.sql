/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'RTCS';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema RTCS'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = RTCS;
           
/* ## 2 ##################################################################################################################### 

    Table EVENTS_RATINGINPUT_20108 
    Source Events.ratingInput

*/

CREATE TABLE RTCS.EVENTS_RATINGINPUT_20108 (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  Customer_ROP_s_OfferId VARCHAR(20),
  OnTouchHandling_LogInfo VARCHAR(50),
  Event_FarmNodeId VARCHAR(50),
  Event_ProcessId VARCHAR(20),
  Event_ContextId VARCHAR(20),
  LogicProcessing_LogInfo VARCHAR(30),
  Customer_CDP_DateOfBirth DATE,
  Customer_CDP_Gender INTEGER,
  Customer_User_ServiceProviderId VARCHAR(20),
  PRIMARY KEY ( USEID, EVENTID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGINPUT_20108
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMENT ON RTCS.EVENTS_RATINGINPUT_20108 ( 
	USEID IS '-----'
	);

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGINPUT_20108
	ALLOW WRITE ACCESS;

COMMIT;

/* ## 13 ##################################################################################################################### 

    Table EVENTS_RATINGIRRELEVANTOUTPUT_20108 
    Source Events.RATINGIRRELEVANTOUTPUT
    
*/

CREATE TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_20108 (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  ct INTEGER,
  ec INTEGER,
  ee INTEGER,
  PRIMARY KEY ( USEID, EVENTID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_20108
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_20108
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_PROJECTADDON_20108 
    Source Events.PROJECTADDON
    
*/

CREATE TABLE RTCS.EVENTS_PROJECTADDON_20108 (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  slcOutpayment_balanceName VARCHAR(50),
  slcOutpayment_outpayedAmount BIGINT,
  SLC_CurrentPeriod VARCHAR(20),
  PRIMARY KEY ( USEID, EVENTID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_PROJECTADDON_20108
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_PROJECTADDON_20108
	ALLOW WRITE ACCESS;

COMMIT;
