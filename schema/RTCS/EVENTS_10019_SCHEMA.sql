/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'RTCS';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema RTCS'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = RTCS;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGINPUT_10019 
    Source Events.ratingInput

*/

CREATE TABLE RTCS.EVENTS_RATINGINPUT_10019 (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  GEX_UsedUnits BIGINT,
  GEX_RequestedUnits BIGINT,
  GEX_RatingAmount BIGINT,
  GEX_Currency VARCHAR(20),
  GEX_MerchantId VARCHAR(20),
  GEX_ProductId VARCHAR(20),
  GEX_Purpose VARCHAR(255),
  Event_is3G CHARACTER(1),
  rating_CC_Request_Type INTEGER,
  rating_isRoaming CHARACTER(1),
  rating_RoamingZone VARCHAR(100),
  rating_RoamingTimeZone VARCHAR(6),
  rating_Message_Size INTEGER,
  rating_Message_Type INTEGER,
  rating_normalizedDestination VARCHAR(50),
  rating_normalizedSmsMessageCenter VARCHAR(20),
  rating_normalizedVisitedMscNumber VARCHAR(20),
  rating_normalizedVLR_Destination VARCHAR(20),
  rating_normalizedVLR_Originator VARCHAR(20),
  rating_Originator_3GPP_IMSI_MCC_MNC VARCHAR(6),
  rating_Originator_Address_Type INTEGER,
  rating_Originator_Domain_Name VARCHAR(100),
  rating_Recipient_3GPP_IMSI_MCC_MNC_0 VARCHAR(6),
  rating_Recipient_Recipient_Domain_Name_0 VARCHAR(100),
  rating_Recipient_Recipient_L INTEGER,
  rating_Recipient_RecipientAddress_Type_0 INTEGER,
  rating_Recipient_typeOfDestination INTEGER,
  rating_VAS_Id VARCHAR(20),
  rating_VASP_Id VARCHAR(20),
  Entry_ss7DialedNumber VARCHAR(50),
  SS7_normalizedCalled VARCHAR(50),
  SS7_normalizedCaller VARCHAR(50),
  Event_Duration BIGINT,
  Event_CallSetupTime INTEGER,
  Event_Volume BIGINT,
  Event_StopTime BIGINT,
  Event_ReguidingInfo INTEGER,
  Event_isSecondary CHARACTER(1),
  wmca_EventOrigin INTEGER,
  Event_ReGuidingTargetOfferId INTEGER,
  Event_ReGuidingFallbackOfferId INTEGER,
  Event_OriginalEventId BIGINT,
  Customer_User_IMSI VARCHAR(20),
  CallReferenceNumber VARCHAR(20),
  Event_ReguidingSourceCustomerInfo INTEGER,
  Event_ReGuidingSourceCustomerTargetOfferId INTEGER,
  Event_ReGuidingSourceCustomerFallbackOfferId INTEGER,
  Event_ErrorInfo_FailedTableName VARCHAR(20),
  Event_ErrorInfo_FailedSearchKey VARCHAR(20),
  Event_ErrorInfo_FailedSearchKeyContent VARCHAR(20),
  wmca_Event_QuotaDimensionSize INTEGER,
  wmca_Event_QuotaDimensionType1 INTEGER,
  wmca_Event_QuotaPrecision1 INTEGER,
  wmca_Event_QuotaScalingFactor1 BIGINT,
  wmca_Event_QuotaDimensionType2 INTEGER,
  wmca_Event_QuotaPrecision2 INTEGER,
  wmca_Event_QuotaScalingFactor2 BIGINT,
  Customer_ROP_s_OfferId VARCHAR(20),
  OnTouchHandling_LogInfo VARCHAR(50),
  Event_FarmNodeId VARCHAR(50),
  Event_ProcessId VARCHAR(20),
  Event_ContextId VARCHAR(20),
  LogicProcessing_LogInfo VARCHAR(30),
  Customer_CDP_DateOfBirth DATE,
  Customer_CDP_Gender INTEGER,
  EventInfo VARCHAR(50),
  ExternalClientUserName VARCHAR(20),
  ExternalClientIpAddress VARCHAR(15),
  ExternalClientIfType VARCHAR(20),
  SS7_IMSI VARCHAR(20),
  Customer_User_ServiceProviderId VARCHAR(20),
  PRIMARY KEY ( USEID, EVENTID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGINPUT_10019
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMENT ON RTCS.EVENTS_RATINGINPUT_10019 ( 
	USEID IS '---',
	EVENTID IS '---'
	);

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGINPUT_10019
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGIRRELEVANTOUTPUT_10019_OK
    Source Events.RATINGIRRELEVANTOUTPUT
    
*/

CREATE TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_10019_OK (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  DonatorAccessKey VARCHAR(50),
  CurrentStatus VARCHAR(20),
  PreviousStatus VARCHAR(20),
  gseid VARCHAR(20),
  gscid VARCHAR(20),
  gnsnmn INTEGER,
  gnsnai INTEGER,
  gnsnai1 INTEGER,
  gnsnai2 INTEGER,
  gnsnai3 INTEGER,
  gnsnai4 INTEGER,
  gnsnai5 INTEGER,
  gnsnai6 INTEGER,
  gnsnai7 INTEGER,
  gnsnai8 INTEGER,
  gnsnai9 INTEGER,
  gnsnaf VARCHAR(20),
  gexpi TIMESTAMP(0),
  goexpi TIMESTAMP(0),
  gtast INTEGER,
  gtaid VARCHAR(20),
  cuc INTEGER,
  gexse VARCHAR(100),
  OldActiveEndDate TIMESTAMP(0),
  NewActiveEndDate TIMESTAMP(0),
  OldDeactiveEndDate TIMESTAMP(0),
  NewDeactiveEndDate TIMESTAMP(0),
  OldGraceEndDate TIMESTAMP(0),
  NewGraceEndDate TIMESTAMP(0),
  OldPostGraceEndDate TIMESTAMP(0),
  NewPostGraceEndDate TIMESTAMP(0),
  PRIMARY KEY ( USEID, EVENTID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_10019_OK
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_10019_OK
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGIRRELEVANTOUTPUT_10019_ERR 
    Source Events.RATINGIRRELEVANTOUTPUT.ERR
    
*/

CREATE TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_10019_ERR (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  gseid VARCHAR(20),
  gscid VARCHAR(20),
  DonatorAccessKey VARCHAR(15),
  gnsnmn INTEGER,
  gnsnai INTEGER,
  gnsnps VARCHAR(20),
  gtaid VARCHAR(20),
  gevcau INTEGER,
  geveff INTEGER,
  gerri VARCHAR(255),
  gexse VARCHAR(100),
  PRIMARY KEY ( USEID, EVENTID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_10019_ERR
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_10019_ERR
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_PROJECTADDON_10019 
    Source Events.PROJECTADDON
    
*/

CREATE TABLE RTCS.EVENTS_PROJECTADDON_10019 (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  GEX_RecipientFee INTEGER,
  ccsExtension_recipientFee INTEGER,
  ccsExtension_PPITransactionID VARCHAR(20),
  PRIMARY KEY ( USEID, EVENTID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_PROJECTADDON_10019
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_PROJECTADDON_10019
	ALLOW WRITE ACCESS;

COMMIT;
