/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'RTCS';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema RTCS'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = RTCS;
           
/* ## 2 ##################################################################################################################### 

    Table EVENTS_RATINGINPUT_40000 
    Source Events.ratingInput

*/

CREATE TABLE RTCS.EVENTS_RATINGINPUT_40000 (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  CA_Modify_PackageItem_ChargeMode INTEGER,
  CA_Modify_PackageItem_BalanceAdjustMode INTEGER,
  CA_Modify_PackageItem_BalanceAdjustValue BIGINT,
  CA_Modify_PackageItem_BalanceAdjustChargeCode INTEGER,
  CA_Modify_PackageItem_FocSubscribeAvailable INTEGER,
  CA_Modify_PackageItem_FocModifyAvailable INTEGER,
  CA_Modify_PackageItem_EndOfValidity TIMESTAMP(0),
  CA_Modify_PackageItem_UserNotification VARCHAR(10),
  CA_Modify_PackageItem_ActivationMode INTEGER,
  CA_Modify_PackageItem_FreeOfChargeAvailable INTEGER,
  CA_Modify_PackageItem_AccountExpiryDate TIMESTAMP(0),
  CA_Modify_PackageItem_AccountExpiryDatePolicy INTEGER,
  CA_Modify_PackageItem_SecondsOfExtension INTEGER,
  CA_Modify_PackageItem_NumberListMode INTEGER,
  CA_Modify_PackageItem_CreditLimit BIGINT,
  CA_Modify_PackageItem_LifeTimeStart TIMESTAMP(0),
  CA_Read_PackageItem_ChargeMode INTEGER,
  CA_Read_PackageItem_Prices CHARACTER(1),
  CA_Subscribe_PackageItem_ChargeMode INTEGER,
  CA_Subscribe_PackageItem_UserNotification VARCHAR(10),
  CA_Subscribe_PackageItem_Recharge INTEGER,
  CA_Subscribe_PackageItem_ChargeCode INTEGER,
  CA_Subscribe_PackageItem_ActivationTime TIMESTAMP(0),
  CA_Subscribe_PackageItem_UnsubscribeTime TIMESTAMP(0),
  CA_Subscribe_PackageItem_LifeTimeStart TIMESTAMP(0),
  TMAF_Request_Package VARCHAR(20),
  TMAF_Request_CouponGroup VARCHAR(50),
  TMAF_Request_ROPKey INTEGER,
  TMAF_Request_PackageType INTEGER,
  TMAF_Request_SelfCare CHARACTER(1),
  AdminUseCase INTEGER,
  AdminKind INTEGER,
  RechargingParameters_StornoIdofDelayedReward VARCHAR(50),
  ENTRY_SS7DIALEDNUMBER VARCHAR(50),
  SS7_NORMALIZEDCALLED VARCHAR(50),
  SS7_NORMALIZEDCALLER VARCHAR(50),
  EVENT_DURATION BIGINT,
  Event_CallSetupTime INTEGER,
  Event_Volume BIGINT,
  Event_StopTime BIGINT,
  Event_ReguidingInfo INTEGER,
  Event_isSecondary CHARACTER(10),
  wmca_EventOrigin INTEGER,
  Event_ReGuidingTargetOfferId INTEGER,
  Event_ReGuidingFallbackOfferId INTEGER,
  Event_OriginalEventId BIGINT,
  Customer_User_IMSI VARCHAR(20),
  CALLREFERENCENUMBER VARCHAR(20),
  Customer_ROP_s_OfferId VARCHAR(20),
  OnTouchHandling_LogInfo VARCHAR(50),
  Event_FarmNodeId VARCHAR(50),
  Event_ProcessId VARCHAR(20),
  Event_ContextId VARCHAR(20),
  LogicProcessing_LogInfo VARCHAR(30),
  Customer_CDP_DateOfBirth DATE,
  Customer_CDP_Gender INTEGER,
  EventInfo VARCHAR(50),
  ExternalClientUserName VARCHAR(20),
  ExternalClientIpAddress VARCHAR(15),
  ExternalClientIfType VARCHAR(20),
  Customer_User_ServiceProviderId VARCHAR(20),
  CA_Create_Coupon_Name VARCHAR(20),
  CA_Create_Coupon_NameUnique CHARACTER(1),
  CA_Create_Coupon_ActivationStartTime TIMESTAMP(0),
  CA_Create_Coupon_ActivationEndTime TIMESTAMP(0),
  CA_Create_Coupon_Type INTEGER,
  CA_Create_Coupon_Priority INTEGER,
  CA_Create_Coupon_FilterRuleId VARCHAR(20),
  CA_Create_Coupon_Merchant VARCHAR(20),
  CA_Create_Coupon_Value BIGINT,
  CA_Create_Coupon_Applicability INTEGER,
  CA_Create_Coupon_TransparentData VARCHAR(50),
  CA_Create_Coupon_BalanceAmount BIGINT,
  CA_Modify_Coupon_Key INTEGER,
  CA_Modify_Coupon_Name VARCHAR(20),
  CA_Modify_Coupon_Applicability INTEGER,
  CA_Modify_Coupon_TransparentData VARCHAR(50),
  CA_Modify_Coupon_BalanceAdjustMode INTEGER,
  CA_Modify_Coupon_BalanceAdjustValue BIGINT,
  CA_Delete_Coupon_Key INTEGER,
  CA_Delete_Coupon_Name VARCHAR(20),
  PRIMARY KEY ( USEID, EVENTID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGINPUT_40000
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMENT ON RTCS.EVENTS_RATINGINPUT_40000 ( 
	USEID IS '-----'
	);

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGINPUT_40000
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGINPUT_CA_Modify_PackageItem_CUGList_append 
    Source Events.RATINGINPUT
    
*/
CREATE TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_CUGList_append (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  GroupId VARCHAR(20)
  /* ,
  PRIMARY KEY ( USEID, EVENTID ) */
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_CUGList_append
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_CUGList_append
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGINPUT_CA_Modify_PackageItem_CUGList_remove 
    Source Events.RATINGINPUT
    
*/
CREATE TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_CUGList_remove (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  GroupId VARCHAR(20)
  /* ,
  PRIMARY KEY ( USEID, EVENTID ) */
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_CUGList_remove
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_CUGList_remove
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGINPUT_CA_Modify_PackageItem_CUGList_delete 
    Source Events.RATINGINPUT
    
*/
CREATE TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_CUGList_delete (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  GroupId VARCHAR(20)
  /* ,
  PRIMARY KEY ( USEID, EVENTID ) */
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_CUGList_delete
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_CUGList_delete
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGINPUT_CA_Modify_PackageItem_FavoriteAreaList_append 
    Source Events.RATINGINPUT
    
*/
CREATE TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_FavoriteAreaList_append (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  favorite_area_destination_number VARCHAR(20)
  /* ,
  PRIMARY KEY ( USEID, EVENTID ) */
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_FavoriteAreaList_append
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_FavoriteAreaList_append
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGINPUT_CA_Modify_PackageItem_FavoriteAreaList_remove 
    Source Events.RATINGINPUT
    
*/
CREATE TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_FavoriteAreaList_remove (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  favorite_area_destination_number VARCHAR(20)
  /* ,
  PRIMARY KEY ( USEID, EVENTID ) */
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_FavoriteAreaList_remove
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_FavoriteAreaList_remove
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGINPUT_CA_Modify_PackageItem_FavoriteAreaList_delete 
    Source Events.RATINGINPUT
    
*/
CREATE TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_FavoriteAreaList_delete (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  favorite_area_destination_number VARCHAR(20)
  /* ,
  PRIMARY KEY ( USEID, EVENTID ) */
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_FavoriteAreaList_delete
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_FavoriteAreaList_delete
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGINPUT_CA_Modify_PackageItem_FnFList_append 
    Source Events.RATINGINPUT
    
*/
CREATE TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_FnFList_append (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  fnf_number VARCHAR(20)
  /* ,
  PRIMARY KEY ( USEID, EVENTID ) */
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_FnFList_append
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_FnFList_append
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGINPUT_CA_Modify_PackageItem_FnFList_remove 
    Source Events.RATINGINPUT
    
*/
CREATE TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_FnFList_remove (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  fnf_number VARCHAR(20)
  /* ,
  PRIMARY KEY ( USEID, EVENTID ) */
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_FnFList_remove
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_FnFList_remove
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGINPUT_CA_Modify_PackageItem_FnFList_delete 
    Source Events.RATINGINPUT
    
*/
CREATE TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_FnFList_delete (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  fnf_number VARCHAR(20)
  /* ,
  PRIMARY KEY ( USEID, EVENTID ) */
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_FnFList_delete
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_FnFList_delete
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGINPUT_CA_Modify_PackageItem_FnFList_usePID 
    Source Events.RATINGINPUT
    
*/
CREATE TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_FnFList_usePID (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  fnf_number VARCHAR(20)
  /* ,
  PRIMARY KEY ( USEID, EVENTID ) */
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_FnFList_usePID
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_FnFList_usePID
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGINPUT_CA_Modify_PackageItem_ForbiddenList_append 
    Source Events.RATINGINPUT
    
*/
CREATE TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_ForbiddenList_append (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  forbidden_number VARCHAR(20)
  /* ,
  PRIMARY KEY ( USEID, EVENTID ) */
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_ForbiddenList_append
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_ForbiddenList_append
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGINPUT_CA_Modify_PackageItem_ForbiddenList_remove 
    Source Events.RATINGINPUT
    
*/
CREATE TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_ForbiddenList_remove (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  forbidden_number VARCHAR(20)
  /* ,
  PRIMARY KEY ( USEID, EVENTID ) */
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_ForbiddenList_remove
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_ForbiddenList_remove
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGINPUT_CA_Modify_PackageItem_ForbiddenList_delete 
    Source Events.RATINGINPUT
    
*/
CREATE TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_ForbiddenList_delete (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  forbidden_number VARCHAR(20)
  /* ,
  PRIMARY KEY ( USEID, EVENTID ) */
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_ForbiddenList_delete
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_ForbiddenList_delete
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGINPUT_CA_Modify_PackageItem_ForbiddenList_usePID 
    Source Events.RATINGINPUT
    
*/
CREATE TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_ForbiddenList_usePID (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  forbidden_number VARCHAR(20)
  /* ,
  PRIMARY KEY ( USEID, EVENTID ) */
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_ForbiddenList_usePID
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_ForbiddenList_usePID
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGINPUT_CA_Modify_PackageItem_HomeZoneList_append 
    Source Events.RATINGINPUT
    
*/
CREATE TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_HomeZoneList_append (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  home_zone VARCHAR(20)
  /* ,
  PRIMARY KEY ( USEID, EVENTID ) */
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_HomeZoneList_append
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_HomeZoneList_append
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGINPUT_CA_Modify_PackageItem_HomeZoneList_remove 
    Source Events.RATINGINPUT
    
*/
CREATE TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_HomeZoneList_remove (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  home_zone VARCHAR(20)
  /* ,
  PRIMARY KEY ( USEID, EVENTID ) */
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_HomeZoneList_remove
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_HomeZoneList_remove
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGINPUT_CA_Modify_PackageItem_HomeZoneList_delete 
    Source Events.RATINGINPUT
    
*/
CREATE TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_HomeZoneList_delete (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  home_zone VARCHAR(20)
  /* ,
  PRIMARY KEY ( USEID, EVENTID ) */
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_HomeZoneList_delete
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGINPUT_CA_Modify_PackageItem_HomeZoneList_delete
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGOUTPUT_ADDI1_40000
    Source Events.RATINGOUTPUT.ADDI1
    
*/

CREATE TABLE RTCS.EVENTS_RATINGOUTPUT_ADDI1_40000 (
  USEID INTEGER	NOT NULL,
  EVENTID BIGINT NOT NULL,
  res VARCHAR(50),
  PRIMARY KEY ( USEID, EVENTID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGOUTPUT_ADDI1_40000
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGOUTPUT_ADDI1_40000
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGOUTPUT_ADDI1_40000_FOC
    Source Events.RATINGOUTPUT.ADDI1
    
*/

CREATE TABLE RTCS.EVENTS_RATINGOUTPUT_ADDI1_40000_FOC (
  USEID INTEGER	NOT NULL,
  EVENTID BIGINT NOT NULL,
  ind INTEGER,
  "use" INTEGER,
  "max" INTEGER
  /* ,
  PRIMARY KEY ( USEID, EVENTID ) */
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGOUTPUT_ADDI1_40000_FOC
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGOUTPUT_ADDI1_40000_FOC
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGIRRELEVANTOUTPUT_40000 
    Source Events.RATINGIRRELEVANTOUTPUT
    
*/

CREATE TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_40000 (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  BundleItems VARCHAR(50),
  PRIMARY KEY ( USEID, EVENTID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_40000
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_40000
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGIRRELEVANTOUTPUT_40000_PAC1
    Source Events.RATINGIRRELEVANTOUTPUT.PAC1
    
*/

CREATE TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_40000_PAC1 (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  SubscriptionID VARCHAR(20),
  SubscriptionState VARCHAR(12),
  SubscriptionCurrPeriod VARCHAR(10),
  SubscriptionNextPeriod VARCHAR(10),
  PRIMARY KEY ( USEID, EVENTID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_40000_PAC1
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_40000_PAC1
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGIRRELEVANTOUTPUT_40000_POP1
    Source Events.RATINGIRRELEVANTOUTPUT.pop1
    
*/

CREATE TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_40000_POP1 (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  EventCause INTEGER,
  EventEffect INTEGER,
  PRIMARY KEY ( USEID, EVENTID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_40000_POP1
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_40000_POP1
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGIRRELEVANTOUTPUT_40000_SERR
    Source Events.RATINGIRRELEVANTOUTPUT.serr
    
*/

CREATE TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_40000_POP1 (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  Reason VARCHAR(50),
  PRIMARY KEY ( USEID, EVENTID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_40000_POP1
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_40000_POP1
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGIRRELEVANTOUTPUT_40000_SADC
    Source Events.RATINGIRRELEVANTOUTPUT.sadc
    
*/

CREATE TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_40000_SADC (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  ExtBalError VARCHAR(16),
  PRIMARY KEY ( USEID, EVENTID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_40000_SADC
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_40000_SADC
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGIRRELEVANTOUTPUT_40000_DMOD
    Source Events.RATINGIRRELEVANTOUTPUT.dmod
    
*/

CREATE TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_40000_DMOD (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  ModData VARCHAR(50),
  OldValue VARCHAR(50),
  NewValue VARCHAR(50),
  PRIMARY KEY ( USEID, EVENTID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_40000_DMOD
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_40000_DMOD
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGIRRELEVANTOUTPUT_40000_REWD_SUB
    Source Events.RATINGIRRELEVANTOUTPUT.rewd.sub
    
*/

CREATE TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_40000_REWD_SUB (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  RewSubAdd VARCHAR(20),
  RewSubDel VARCHAR(20)
  /* ,
  PRIMARY KEY ( USEID, EVENTID ) */
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_40000_REWD_SUB
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_40000_REWD_SUB
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGIRRELEVANTOUTPUT_40000_REWD_ACT
    Source Events.RATINGIRRELEVANTOUTPUT.rewd.act
    
*/

CREATE TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_40000_REWD_ACT (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  RewSubAct VARCHAR(20),
  RewSubDeact VARCHAR(20)
  /* ,
  PRIMARY KEY ( USEID, EVENTID ) */
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_40000_REWD_ACT
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_40000_REWD_ACT
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGIRRELEVANTOUTPUT_40000_REWD_EXP
    Source Events.RATINGIRRELEVANTOUTPUT.rewd.exp
    
*/

CREATE TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_40000_REWD_EXP (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  BalanceName VARCHAR(20),
  RewExpOld VARCHAR(23),
  RewExpNew VARCHAR(23)
  /* ,
  PRIMARY KEY ( USEID, EVENTID ) */
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_40000_REWD_EXP
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_40000_REWD_EXP
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGIRRELEVANTOUTPUT_40000_REWD_SLC
    Source Events.RATINGIRRELEVANTOUTPUT.rewd.slc
    
*/

CREATE TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_40000_REWD_SLC (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  OldSLCDate VARCHAR(23),
  NewSLCDate VARCHAR(23)
  /* ,
  PRIMARY KEY ( USEID, EVENTID ) */
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_40000_REWD_SLC
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_40000_REWD_SLC
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGIRRELEVANTOUTPUT_40000_PCRF
    Source Events.RATINGIRRELEVANTOUTPUT.pcrf
    
*/

CREATE TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_40000_PCRF (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  pcpKey INTEGER,
  packageId VARCHAR(20),
  pcpState VARCHAR(20),
  pcpStateInvalidFrom VARCHAR(10),
  pcpNextState VARCHAR(20)
  /* ,
  PRIMARY KEY ( USEID, EVENTID ) */
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_40000_PCRF
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_40000_PCRF
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGIRRELEVANTOUTPUT_40000_BNDL
    Source Events.RATINGIRRELEVANTOUTPUT.bndl
    
*/

CREATE TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_40000_BNDL (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  BundleItems VARCHAR(20),
  PRIMARY KEY ( USEID, EVENTID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_40000_BNDL
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_40000_BNDL
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_PROJECTADDON_40000 
    Source Events.PROJECTADDON
    
*/

CREATE TABLE RTCS.EVENTS_PROJECTADDON_40000 (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  RPP_Resubscription_isRepurchased CHARACTER(1),
  RPP_Remove_pendingReservation_Amount BIGINT,
  PRIMARY KEY ( USEID, EVENTID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_PROJECTADDON_40000
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_PROJECTADDON_40000
	ALLOW WRITE ACCESS;

COMMIT;
