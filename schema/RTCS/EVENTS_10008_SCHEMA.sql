/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'RTCS';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema RTCS'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = RTCS;

/* ## 48 ##################################################################################################################### 

    Table EVENTS_RATINGINPUT_10008
    Source Events.ratingInput

*/

CREATE TABLE RTCS.EVENTS_RATINGINPUT_10008 (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  CommonParameter_APN VARCHAR(50),
  CommonParameter_MCC VARCHAR(3),
  CommonParameter_MNC VARCHAR(3),
  CommonParameter_RoamingInformationIncomingAParty VARCHAR(50),
  CommonParameter_RoamingInformationEvaluatedAParty	INTEGER,
  Event_is3G CHARACTER(1),
  rating_originatorLocationInfo VARCHAR(50),
  Event_LocationInfoType INTEGER,
  DMI_REQUEST_SERVICE_INFORMATION_PS_INFORMATION_3GPP_RAT_TYPE INTEGER,
  CommonParameter_QoSProfileID INTEGER,
  rating_RadioAccessTechnology INTEGER,
  Event_Order VARCHAR(50),
  Event_UserSessionDuration BIGINT,
  rating_IMEI VARCHAR(20),
  rating_IMEISV VARCHAR(10),
  rating_isRoaming CHARACTER(1),
  rating_RoamingZone VARCHAR(100),
  rating_RoamingTimeZone VARCHAR(6),
  CommonParameter_CustomerReguidingOrigKey VARCHAR(50),
  Entry_ss7DialedNumber VARCHAR(50),
  SS7_normalizedCalled VARCHAR(50), 
  SS7_normalizedCaller VARCHAR(50),
  Event_Duration BIGINT,
  Event_CallSetupTime INTEGER,
  Event_Volume BIGINT,
  Event_StopTime BIGINT,
  Event_ReguidingInfo INTEGER,
  Event_isSecondary CHARACTER(1),
  wmca_EventOrigin INTEGER,
  Event_ReGuidingTargetOfferId INTEGER,
  Event_ReGuidingFallbackOfferId INTEGER,
  Event_OriginalEventId BIGINT,
  Customer_User_IMSI VARCHAR(15),
  CallReferenceNumber VARCHAR(20),
  Event_ReguidingSourceCustomerInfo INTEGER,
  Event_ReGuidingSourceCustomerTargetOfferId INTEGER,
  Event_ReGuidingSourceCustomerFallbackOfferId INTEGER,
  Event_ErrorInfo_FailedTableName VARCHAR(20),
  Event_ErrorInfo_FailedSearchKey VARCHAR(20),
  Event_ErrorInfo_FailedSearchKeyContent VARCHAR(20),
  FBC_SvcTypeVolumeAndTime INTEGER,
  FBC_SvcTypeVolume INTEGER,
  FBC_SvcTypeTime INTEGER,
  FBC_SvcTypeServiceSpecific INTEGER,
  FBC_SvcQuotaType INTEGER,
  FBC_RoamingZone INTEGER,
  FBC_isGrantFirstOk CHARACTER(1),
  FBC_FoC CHARACTER(1),
  FBC_BasicFeeWasCharged CHARACTER(1),
  FBC_BasicFeeEnabled INTEGER,
  FBC_ServiceId VARCHAR(50),
  FBC_timeBlockSize INTEGER,
  FBC_CategoryId INTEGER,
  DmiTickets_UsedVolumeQuota BIGINT,
  DmiTickets_UsedVolumeQuotaBeforeTS BIGINT,
  DmiTickets_UsedVolumeQuotaAfterTS BIGINT,
  DmiTickets_UsedTimeQuota BIGINT,
  DmiTickets_UsedTimeQuotaBeforeTS BIGINT,
  DmiTickets_UsedTimeQuotaAfterTS BIGINT,
  DmiTickets_QoSReleaseIndication VARCHAR(100),
  DmiTickets_QoSNegotiatedData VARCHAR(255),
  DmiTickets_QoSMaxRequestedBandwidthUL INTEGER,
  DmiTickets_QoSMaxRequestedBandwidthDL INTEGER,
  DmiTickets_QoSGuaranteedBitrateUL INTEGER,
  DmiTickets_QoSGuaranteedBitrateDL INTEGER,
  DmiTickets_QoSApnAggregateMaxBitrateUL INTEGER,
  DmiTickets_QoSApnAggregateMaxBitrateDL INTEGER,
  DmiTickets_QoSClassIdentifier INTEGER,
  DmiTickets_QoSBearerIdentifier VARCHAR(50),
  DmiTickets_QoSAllocationRetentionPriorityLevel INTEGER,
  DmiTickets_QoSAllocationRetentionPriorityPreemptionCapability INTEGER,
  DmiTickets_QoSAllocationRetentionPriorityPreemptionVulnerability INTEGER,
  DmiTickets_TicketSequence INTEGER,
  wmca_Event_QuotaDimensionSize INTEGER,
  wmca_Event_QuotaDimensionType1 INTEGER,
  wmca_Event_QuotaPrecision1 INTEGER,
  wmca_Event_QuotaScalingFactor1 BIGINT,
  wmca_Event_QuotaDimensionType2 INTEGER,
  wmca_Event_QuotaPrecision2 INTEGER,
  wmca_Event_QuotaScalingFactor2 BIGINT,
  wmca_Event_NumberOfSegments INTEGER,
  wmca_Event_UsedVolumeQuotaBeforeTS BIGINT,
  wmca_Event_UsedVolumeQuotaAfterTS BIGINT,
  wmca_Event_UsedTimeQuotaBeforeTS BIGINT,
  wmca_Event_UsedTimeQuotaAfterTS BIGINT,
  Customer_ROP_s_OfferId VARCHAR(20),
  OnTouchHandling_LogInfo VARCHAR(50),
  Event_FarmNodeId VARCHAR(50),
  Event_ProcessId VARCHAR(20),
  Event_ContextId VARCHAR(20),
  LogicProcessing_LogInfo VARCHAR(30),
  Customer_CDP_DateOfBirth DATE,
  Customer_CDP_Gender INTEGER,
  SS7_IMSI VARCHAR(15),
  Customer_User_ServiceProviderId VARCHAR(20),
  PRIMARY KEY ( USEID, EVENTID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGINPUT_10008
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

/* COMMENT ON RTCS.EVENTS_10008_RATINGINPUT ( 
	SS7_ISROAMING IS 'BOOLEAN TYPE'
	);

COMMIT; */

RUNSTATS ON TABLE RTCS.EVENTS_RATINGINPUT_10008
	ALLOW WRITE ACCESS;

COMMIT;

/* ## 46 ##################################################################################################################### 

    Table EVENTS_RATINGIRRELEVANTOUTPUT_10008_OK 
    Source Events.RATINGIRRELEVANTOUTPUT.OK
    
*/

CREATE TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_10008_OK (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  fus VARCHAR(20),
  ful VARCHAR(20),
  fui VARCHAR(32),
  fuc INTEGER,
  fuq INTEGER,
  fut TIMESTAMP(0),
  fud BIGINT,
  fss INTEGER,
  fst INTEGER,
  fsi VARCHAR(20),
  fso BIGINT,
  fsa BIGINT,
  fsr INTEGER,
  cuc INTEGER,
  PRIMARY KEY ( USEID, EVENTID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_10008_OK
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_10008_OK
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGIRRELEVANTOUTPUT_10008_ERR 
    Source Events.RATINGIRRELEVANTOUTPUT.ERR
    
*/

CREATE TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_10008_ERR (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  fci VARCHAR(32),
  fec INTEGER,
  fee INTEGER,
  fei VARCHAR(100),
  foc BIGINT,
  PRIMARY KEY ( USEID, EVENTID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_10008_ERR
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_10008_ERR
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGIRRELEVANTOUTPUT_10008_PCRF
    Source Events.RATINGIRRELEVANTOUTPUT.ERR
    
*/

CREATE TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_10008_PCRF (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  pcpKey INTEGER,
  packageId VARCHAR(20),
  pcpState VARCHAR(20),
  pcpStateInvalidFrom VARCHAR(20),
  pcpNextState VARCHAR(20)
  /* ,
  PRIMARY KEY ( USEID, EVENTID ) */
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_10008_PCRF
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_10008_PCRF
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_PROJECTADDON_10008 
    Source Events.PROJECTADDON
    
*/

CREATE TABLE RTCS.EVENTS_PROJECTADDON_10008 (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  DmtrUSTicket_SUB_0_Final INTEGER,
  DmtrUSTicket_SUB_0_Sum VARCHAR(20),
  Infos4DiaEvent_3GPP_GGSN_Address VARCHAR(20),
  Infos4DiaEvent_3GPP_Charging_ID VARCHAR(20),
  DMTR_SUBREQUEST_USED_SERVICE_UNIT_0_CC_TOTAL_OCTETS BIGINT,
  DMTR_SUBREQUEST_USED_SERVICE_UNIT_1_CC_TOTAL_OCTETS BIGINT,
  PRIMARY KEY ( USEID, EVENTID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_PROJECTADDON_10008
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_PROJECTADDON_10008
	ALLOW WRITE ACCESS;

COMMIT;