/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'RTCS';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema RTCS'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = RTCS;

/* ## x ##################################################################################################################### 

    Table EVENTS_RATINGINPUT_11009 
    Source Events.ratingInput

*/

CREATE TABLE RTCS.EVENTS_RATINGINPUT_11009 (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  RechargingParameters_RechargeAmount INTEGER,
  RechargingParameters_ExpiryDateExtensionMode INTEGER,
  RechargingParameters_ExpiryDateExtension INTEGER,
  RechargingParameters_RechargeCurrency VARCHAR(20),
  RechargingParameters_RechargeChannel INTEGER,
  RechargingParameters_ProviderIdVoucher VARCHAR(20),
  RechargingParameters_VoucherType INTEGER,
  RechargingParameters_VoucherExpiryDate INTEGER,
  RechargingParameters_VoucherSerialNo VARCHAR(20),
  RechargingParameters_VoucherID VARCHAR(20),
  RechargingParameters_TransID VARCHAR(20),
  RechargingParameters_preValidated CHARACTER(1),
  RechargingParameters_StornoId VARCHAR(20),
  RechargingParameters_IsStorno CHARACTER(1),
  RechargingParameters_StornoSubUsecase VARCHAR(50),
  RechargingParameters_DonatorId VARCHAR(15),
  URecharging_In_AmountOfUnits BIGINT,
  URecharging_In_RechargeChannel INTEGER,
  URecharging_In_ChargeCode INTEGER,
  URecharging_In_RatingInput_0 VARCHAR(50),
  URecharging_In_RatingInput_1 VARCHAR(50),
  URecharging_In_RatingInput_2 VARCHAR(50),
  URecharging_In_RatingInput_3 VARCHAR(50),
  URecharging_In_RatingInput_4 VARCHAR(50),
  URecharging_In_RatingInput_5 VARCHAR(50),
  URecharging_In_RatingInput_6 VARCHAR(50),
  URecharging_In_RatingInput_7 VARCHAR(50),
  URecharging_In_RatingInput_8 VARCHAR(50),
  URecharging_In_RatingInput_9 VARCHAR(50),
  URecharging_In_EventName VARCHAR(50),
  URecharging_In_EventInfo VARCHAR(50),
  URecharging_In_WriteEvent CHARACTER(1),
  URecharging_Out_RatingOutput_0 VARCHAR(50),
  URecharging_Out_RatingOutput_1 VARCHAR(50),
  URecharging_Out_RatingOutput_2 VARCHAR(50),
  URecharging_Out_RatingOutput_3 VARCHAR(50),
  URecharging_Out_RatingOutput_4 VARCHAR(50),
  URecharging_Out_RatingOutput_5 VARCHAR(50),
  URecharging_Out_RatingOutput_6 VARCHAR(50),
  URecharging_Out_RatingOutput_7 VARCHAR(50),
  URecharging_Out_RatingOutput_8 VARCHAR(50),
  URecharging_Out_RatingOutput_9 VARCHAR(50),
  URecharging_Out_RechargedBalances_0 VARCHAR(50),
  URecharging_Out_RechargedBalances_1 VARCHAR(50),
  URecharging_Out_RechargedBalances_2 VARCHAR(50),
  URecharging_Out_RechargedBalances_3 VARCHAR(50),
  URecharging_Out_RechargedBalances_4 VARCHAR(50),
  URecharging_Out_RechargedBalances_5 VARCHAR(50),
  URecharging_Out_RechargedBalances_6 VARCHAR(50),
  URecharging_Out_RechargedBalances_7 VARCHAR(50),
  URecharging_Out_RechargedBalances_8 VARCHAR(50),
  URecharging_Out_RechargedBalances_9 VARCHAR(50),
  URecharging_Out_ResponseResult VARCHAR(50),
  Customer_ROP_s_OfferId VARCHAR(20),
  OnTouchHandling_LogInfo VARCHAR(50),
  Event_FarmNodeId VARCHAR(50),
  Event_ProcessId VARCHAR(20),
  Event_ContextId VARCHAR(20),
  LogicProcessing_LogInfo VARCHAR(30),
  Customer_CDP_DateOfBirth DATE,
  Customer_CDP_Gender INTEGER,
  EventInfo VARCHAR(20),
  ExternalClientUserName VARCHAR(50),
  ExternalClientIpAddress VARCHAR(15),
  ExternalClientIfType VARCHAR(20),
  RechargingParameters_messageIdHistory_0 VARCHAR(20),
  RechargingParameters_numberOfIntermediateRecharges INTEGER,
  Entry_ss7DialedNumber VARCHAR(50),
  SS7_normalizedCalled VARCHAR(50),
  SS7_normalizedCaller VARCHAR(50),
  Event_Duration BIGINT,
  Event_CallSetupTime INTEGER,
  Event_Volume BIGINT,
  Event_StopTime BIGINT,
  Event_ReguidingInfo INTEGER,
  Event_isSecondary CHARACTER(1),
  wmca_EventOrigin INTEGER,
  Event_ReGuidingTargetOfferId INTEGER,
  Event_ReGuidingFallbackOfferId INTEGER,
  Event_OriginalEventId BIGINT,
  Customer_User_IMSI VARCHAR(20),
  CallReferenceNumber VARCHAR(20),
  Event_ReguidingSourceCustomerInfo INTEGER,
  Event_ReGuidingSourceCustomerTargetOfferId INTEGER,
  Event_ReGuidingSourceCustomerFallbackOfferId INTEGER,
  Event_ErrorInfo_FailedTableName VARCHAR(20),
  Event_ErrorInfo_FailedSearchKey VARCHAR(10),
  Event_ErrorInfo_FailedSearchKeyContent VARCHAR(20),
  Customer_User_ServiceProviderId VARCHAR(20),
  PRIMARY KEY ( USEID, EVENTID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGINPUT_11009
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMENT ON RTCS.EVENTS_RATINGINPUT_11009 ( 
	USEID IS '---',
	EVENTID IS '---'
	);

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGINPUT_11009
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGOUTPUT_ADDI1_11009
    Source Events.RATINGOUTPUT.ADDI1
    
*/

CREATE TABLE RTCS.EVENTS_RATINGOUTPUT_ADDI1_11009 (
  USEID INTEGER	NOT NULL,
  EVENTID BIGINT NOT NULL,
  red TIMESTAMP(0),
  rme TIMESTAMP(0),
  ede INTEGER,
  edm INTEGER,
  gde INTEGER,
  pgd TIMESTAMP(0),
  pge INTEGER,
  dad TIMESTAMP(0),
  dae INTEGER,
  rou0 VARCHAR(50),
  rou1 VARCHAR(50),
  rou2 VARCHAR(50),
  rou3 VARCHAR(50),
  rou4 VARCHAR(50),
  rou5 VARCHAR(50),
  rou6 VARCHAR(50),
  rou7 VARCHAR(50),
  rou8 VARCHAR(50),
  rou9 VARCHAR(50),
  evi VARCHAR(50),
  PRIMARY KEY ( USEID, EVENTID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGOUTPUT_ADDI1_11009
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGOUTPUT_ADDI1_11009
	ALLOW WRITE ACCESS;

COMMIT;

/* ## 13 ##################################################################################################################### 

    Table EVENTS_RATINGIRRELEVANTOUTPUT_11009
    Source Events.RATINGIRRELEVANTOUTPUT
    
*/

CREATE TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_11009 (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  ct INTEGER,
  ec INTEGER,
  addi1 VARCHAR(255),
  PRIMARY KEY ( USEID, EVENTID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_11009
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_11009
	ALLOW WRITE ACCESS;

COMMIT;

/* ## 14 ##################################################################################################################### 

    Table EVENTS_PROJECTADDON_11009 
    Source Events.PROJECTADDON
    
*/

CREATE TABLE RTCS.EVENTS_PROJECTADDON_11009 (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  CustRecharge_ErrorCode VARCHAR(20),
  PRIMARY KEY ( USEID, EVENTID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_PROJECTADDON_11009
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_PROJECTADDON_11009
	ALLOW WRITE ACCESS;

COMMIT;
