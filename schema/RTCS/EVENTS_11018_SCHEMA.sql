/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'RTCS';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema RTCS'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = RTCS;

/* ## 2 ##################################################################################################################### 

    Table EVENTS_RATINGINPUT_11018 
    Source Events.ratingInput

*/

CREATE TABLE RTCS.EVENTS_RATINGINPUT_11018 (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  Customer_ROP_s_OfferId VARCHAR(20),
  OnTouchHandling_LogInfo VARCHAR(50),
  Event_FarmNodeId VARCHAR(50),
  Event_ProcessId VARCHAR(20),
  Event_ContextId VARCHAR(20),
  LogicProcessing_LogInfo VARCHAR(30),
  Customer_CDP_DateOfBirth DATE,
  Customer_CDP_Gender INTEGER,
  Customer_User_ServiceProviderId VARCHAR(20),
  PRIMARY KEY ( USEID, EVENTID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGINPUT_11018
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMENT ON RTCS.EVENTS_RATINGINPUT_11018 ( 
	USEID IS '-----'
	);

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGINPUT_11018
	ALLOW WRITE ACCESS;

COMMIT;

/* ## 13 ##################################################################################################################### 

    Table EVENTS_RATINGIRRELEVANTOUTPUT_11018 
    Source Events.RATINGIRRELEVANTOUTPUT
    
*/

CREATE TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_11018 (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  OperationClassification INTEGER,
  tid VARCHAR(20),
  frontend VARCHAR(20),
  ErrorDetail VARCHAR(3),
  RecipientSubscriber VARCHAR(11),
  PackageID VARCHAR(10),
  PackageName VARCHAR(20),
  AmountFailedToReserve VARCHAR(20),
  TMAF_errorCode INTEGER,
  TMAF_errorText VARCHAR(20),
  PRIMARY KEY ( USEID, EVENTID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_11018
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_11018
	ALLOW WRITE ACCESS;

COMMIT;
