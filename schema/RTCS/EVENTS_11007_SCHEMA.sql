/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'RTCS';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema RTCS'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = RTCS;

/* ## 48 ##################################################################################################################### 

    Table EVENTS_RATINGINPUT_11007
    Source Events.ratingInput

*/

CREATE TABLE RTCS.EVENTS_RATINGINPUT_11007 (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  CA_BalanceAdjustment_BalanceId VARCHAR(20),
  CA_BalanceAdjustment_AmountOfUnits BIGINT,
  CA_BalanceAdjustment_ChargeCode INTEGER,
  CA_BalanceAdjustment_EventInfo VARCHAR(100),
  CA_BalanceAdjustment_TransactionId VARCHAR(20),
  Customer_ROP_s_OfferId VARCHAR(20),
  OnTouchHandling_LogInfo VARCHAR(50),
  Event_FarmNodeId VARCHAR(50),
  Event_ProcessId VARCHAR(20),
  Event_ContextId VARCHAR(20),
  LogicProcessing_LogInfo VARCHAR(30),
  Customer_CDP_DateOfBirth DATE,
  Customer_CDP_Gender INTEGER,
  EventInfo VARCHAR(20),
  ExternalClientUserName VARCHAR(50),
  ExternalClientIpAddress VARCHAR(15),
  ExternalClientIfType VARCHAR(20),
  Customer_User_ServiceProviderId VARCHAR(20),
  OperationalHistory_DuplicateToMessageId VARCHAR(50),
  PRIMARY KEY ( USEID, EVENTID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGINPUT_11007
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

/* COMMENT ON RTCS.EVENTS_10008_RATINGINPUT ( 
	SS7_ISROAMING IS 'BOOLEAN TYPE'
	);

COMMIT; */

RUNSTATS ON TABLE RTCS.EVENTS_RATINGINPUT_11007
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGIRRELEVANTOUTPUT_11007
    Source Events.RATINGIRRELEVANTOUTPUT
    
*/

CREATE TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_11007 (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  ct INTEGER,
  ec INTEGER,
  ee INTEGER,
  ei VARCHAR(100),
  UnitsBelowLimit BIGINT,
  UnitsDeducted BIGINT,
  PRIMARY KEY ( USEID, EVENTID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_11007
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_11007
	ALLOW WRITE ACCESS;

COMMIT;