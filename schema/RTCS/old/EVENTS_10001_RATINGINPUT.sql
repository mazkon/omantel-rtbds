SET CURRENT SCHEMA = TEST;

CREATE TABLE TEST.EVENTS_10001_RATINGINPUT (
  USEID	INTEGER	NOT NULL,
  EVENTID	BIGINT	NOT NULL,
  SS7_ISROAMING	CHARACTER(1)	NOT NULL,
  SS7_ISCF	CHARACTER(1),
  SS7_NORMALIZEDCALLEDWITHOUTMNPPREFIX	VARCHAR(20),
  SS7_NORMALIZEDVLRNUMBER	VARCHAR(20),
  SS7_NORMALIZEDSMSMESSAGECENTER	VARCHAR(20),
  SS7_EXPANDEDCALLKIND	INTEGER	NOT NULL,
  SS7_NORMALIZEDLOCATIONNUMBERA	VARCHAR(20),
  SS7_NORMALIZEDVISITEDMSCNUMBER	VARCHAR(20),
  SS7_IMSI	VARCHAR(20),
  SS7_ACCESSFLAG	INTEGER	NOT NULL,
  SS7_MSISDNOFSUBSCRIBER	VARCHAR(20),
  SS7_ISUCB	CHARACTER(1)	NOT NULL,
  SS7_LOGINTYPE	INTEGER,
  "rating.originatorLocationInfo"	VARCHAR(20),
  "Event.LocationInfoType"	INTEGER,
  "rating.originatorCellId"	VARCHAR(10),
  "rating.isRoaming"	CHARACTER(1),
  "rating.RoamingZone"	VARCHAR(100),
  "rating.RoamingTimeZone"	VARCHAR(6),
  "Event.is3G"	CHARACTER(1),
  "Network.CIR.CallStopTimeLocal"	INTEGER,
  "Network.CIR.CallStopTimeTimeZone"	INTEGER,
  "Network.CIR.CallAttemptElapsedTimeValue"	INTEGER,
  "Network.CIR.CallConnectedToDestinationTime"	INTEGER,
  "Network.CIR.ReleaseCause"	VARCHAR(20),
  "rating.normalizedDestinationWithoutMnpPrefix"	VARCHAR(20),
  "rating.normalizedVLR_Originator"	VARCHAR(20),
  "rating.normalizedDestination"	VARCHAR(20),
  "rating.isCF"	CHARACTER(1),
  ENTRY_SS7DIALEDNUMBER	VARCHAR(20),
  SS7_NORMALIZEDCALLED	VARCHAR(20),
  SS7_NORMALIZEDCALLER	VARCHAR(20),
  "Event.Duration"	BIGINT,
  "Event.CallSetupTime"	INTEGER,
  "Event.Volume"	BIGINT,
  "Event.StopTime"	BIGINT,
  "Event.ReguidingInfo"	INTEGER,
  "Event.isSecondary"	CHARACTER(10),
  "wmca.EventOrigin"	INTEGER,
  "Event.ReGuidingTargetOfferId"	INTEGER,
  "Event.ReGuidingFallbackOfferId"	INTEGER,
  "Event.OriginalEventId"	BIGINT,
  "Customer.User.IMSI"	VARCHAR(20),
  CALLREFERENCENUMBER	VARCHAR(20),
  "Event.ReguidingSourceCustomerInfo"	INTEGER,
  "Event.ReGuidingSourceCustomerTargetOfferId"	INTEGER,
  "Event.ReGuidingSourceCustomerFallbackOfferId"	INTEGER,
  "Event.ErrorInfo.FailedTableName"	VARCHAR(20),
  "Event.ErrorInfo.FailedSearchKey"	VARCHAR(20),
  "Event.ErrorInfo.FailedSearchKeyContent"	VARCHAR(20),
  "wmca.Event.QuotaDimensionSize"	INTEGER,
  "wmca.Event.QuotaDimensionType1"	INTEGER,
  "wmca.Event.QuotaPrecision1"	INTEGER,
  "wmca.Event.QuotaScalingFactor1"	BIGINT,
  "wmca.Event.QuotaDimensionType2"	INTEGER,
  "wmca.Event.QuotaPrecision2"	INTEGER,
  "wmca.Event.QuotaScalingFactor2"	BIGINT,
  "Customer.ROP.s_OfferId"	VARCHAR(20),
  "OnTouchHandling.LogInfo"	VARCHAR(10),
  "Event.FarmNodeId"	VARCHAR(20),
  "Event.ProcessId"	VARCHAR(20),
  "Event.ContextId"	VARCHAR(20),
  "LogicProcessing.LogInfo"	VARCHAR(30),
  "Customer.CDP.DateOfBirth"	DATE,
  "Customer.CDP.Gender"	INTEGER,
  "Customer.User.ServiceProviderId"	VARCHAR(20)
  ) 
  ORGANIZE BY ROW;

ALTER TABLE TEST.EVENTS_10001_RATINGINPUT
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMENT ON TEST.EVENTS_10001_RATINGINPUT ( 
	SS7_ISROAMING IS 'BOOLEAN TYPE',
	SS7_ISUCB IS 'BOOLEAN TYPE',
	SS7_LOGINTYPE IS 'deprecated',
	"rating.originatorCellId" IS 'deprecated',
	"rating.isRoaming" IS 'BOOLEAN TYPE',
	"Event.is3G" IS 'BOOLEAN TYPE',
	"rating.isCF" IS 'BOOLEAN TYPE',
	"Event.isSecondary" IS 'BOOLEAN TYPE - system internal usage only' );

COMMIT;

RUNSTATS ON TABLE TEST.EVENTS_10001_RATINGINPUT
	ALLOW WRITE ACCESS;

COMMIT;

CREATE UNIQUE INDEX TEST.EVENTS_10001_RATINGINPUT_PK
  ON TEST.EVENTS_10001_RATINGINPUT
    ( USEID ASC, EVENTID ASC )
  DISALLOW REVERSE SCANS
  COMPRESS NO
  EXCLUDE NULL KEYS;

COMMIT;

RUNSTATS ON TABLE "TEST"."EVENTS_10001_RATINGINPUT"
	FOR INDEX
	TEST.EVENTS_10001_RATINGINPUT_PK
	ALLOW READ ACCESS;

COMMIT;
