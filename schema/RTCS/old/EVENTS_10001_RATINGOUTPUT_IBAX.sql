SET CURRENT SCHEMA = TEST;

CREATE TABLE TEST.EVENTS_10001_RATINGOUTPUT_IBAX (
  USEID	INTEGER	NOT NULL,
  EVENTID	BIGINT	NOT NULL,
  BALANCENAME	VARCHAR(20),
  INCREMENTVALUE	BIGINT,
  NEWBALANCE	BIGINT,
  EXPIRYDATE	TIMESTAMP,
  CREDITLIMIT	BIGINT
  ) 
  ORGANIZE BY ROW;

ALTER TABLE TEST.EVENTS_10001_RATINGOUTPUT_IBAX
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE TEST.EVENTS_10001_RATINGOUTPUT_IBAX
	ALLOW WRITE ACCESS;

COMMIT;

CREATE UNIQUE INDEX TEST.EVENTS_10001_RATINGOUTPUT_IBAX_PK
  ON TEST.EVENTS_10001_RATINGOUTPUT_IBAX
    ( USEID ASC, EVENTID ASC )
  DISALLOW REVERSE SCANS
  COMPRESS NO
  EXCLUDE NULL KEYS;

COMMIT;

RUNSTATS ON TABLE "TEST"."EVENTS_10001_RATINGOUTPUT_IBAX"
	FOR INDEX
	TEST.EVENTS_10001_RATINGOUTPUT_IBAX_PK
	ALLOW READ ACCESS;

COMMIT;