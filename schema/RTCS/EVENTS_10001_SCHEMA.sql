/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'RTCS';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema RTCS'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = RTCS;

/* ## 2 ##################################################################################################################### 

    Table EVENTS_RATINGINPUT_10001 
    Source Events.ratingInput

*/

CREATE TABLE RTCS.EVENTS_RATINGINPUT_10001 (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  SS7_ISROAMING CHARACTER(1) NOT NULL,
  SS7_ISCF CHARACTER(1),
  SS7_NORMALIZEDCALLEDWITHOUTMNPPREFIX  VARCHAR(50),
  SS7_NORMALIZEDVLRNUMBER VARCHAR(20),
  SS7_NORMALIZEDSMSMESSAGECENTER VARCHAR(20),
  SS7_EXPANDEDCALLKIND INTEGER NOT NULL,
  SS7_NORMALIZEDLOCATIONNUMBERA VARCHAR(20),
  SS7_NORMALIZEDVISITEDMSCNUMBER VARCHAR(20),
  SS7_IMSI VARCHAR(20),
  SS7_ACCESSFLAG INTEGER NOT NULL,
  SS7_MSISDNOFSUBSCRIBER VARCHAR(20),
  SS7_ISUCB CHARACTER(1) NOT NULL,
  SS7_LOGINTYPE INTEGER,
  RATING_ORIGINATORLOCATIONINFO VARCHAR(50),
  EVENT_LOCATIONINFOTYPE INTEGER,
  RATING_ORIGINATORCELLID VARCHAR(50),
  RATING_ISROAMING CHARACTER(1),
  RATING_ROAMINGZONE VARCHAR(100),
  RATING_ROAMINGTIMEZONE VARCHAR(6),
  EVENT_IS3G CHARACTER(1),
  NETWORK_CIR_CALLSTOPTIMELOCAL INTEGER,
  NETWORK_CIR_CALLSTOPTIMETIMEZONE INTEGER,
  NETWORK_CIR_CALLATTEMPTELAPSEDTIMEVALUE INTEGER,
  NETWORK_CIR_CALLCONNECTEDTODESTINATIONTIME INTEGER,
  NETWORK_CIR_RELEASECAUSE VARCHAR(20),
  RATING_NORMALIZEDDESTINATIONWITHOUTMNPPREFIX VARCHAR(50),
  RATING_NORMALIZEDVLR_ORIGINATOR VARCHAR(20),
  RATING_NORMALIZEDDESTINATION VARCHAR(50),
  RATING_ISCF CHARACTER(1),
  ENTRY_SS7DIALEDNUMBER VARCHAR(50),
  SS7_NORMALIZEDCALLED VARCHAR(50),
  SS7_NORMALIZEDCALLER VARCHAR(50),
  EVENT_DURATION BIGINT,
  Event_CallSetupTime INTEGER,
  Event_Volume BIGINT,
  Event_StopTime BIGINT,
  Event_ReguidingInfo INTEGER,
  Event_isSecondary CHARACTER(1),
  wmca_EventOrigin INTEGER,
  Event_ReGuidingTargetOfferId INTEGER,
  Event_ReGuidingFallbackOfferId INTEGER,
  Event_OriginalEventId BIGINT,
  Customer_User_IMSI VARCHAR(20),
  CALLREFERENCENUMBER VARCHAR(20),
  Event_ReguidingSourceCustomerInfo INTEGER,
  Event_ReGuidingSourceCustomerTargetOfferId INTEGER,
  Event_ReGuidingSourceCustomerFallbackOfferId INTEGER,
  Event_ErrorInfo_FailedTableName VARCHAR(20),
  Event_ErrorInfo_FailedSearchKey VARCHAR(20),
  Event_ErrorInfo_FailedSearchKeyContent VARCHAR(20),
  wmca_Event_QuotaDimensionSize INTEGER,
  wmca_Event_QuotaDimensionType1 INTEGER,
  wmca_Event_QuotaPrecision1 INTEGER,
  wmca_Event_QuotaScalingFactor1 BIGINT,
  wmca_Event_QuotaDimensionType2 INTEGER,
  wmca_Event_QuotaPrecision2 INTEGER,
  wmca_Event_QuotaScalingFactor2 BIGINT,
  Customer_ROP_s_OfferId VARCHAR(20),
  OnTouchHandling_LogInfo VARCHAR(50),
  Event_FarmNodeId VARCHAR(50),
  Event_ProcessId VARCHAR(20),
  Event_ContextId VARCHAR(20),
  LogicProcessing_LogInfo VARCHAR(30),
  Customer_CDP_DateOfBirth DATE,
  Customer_CDP_Gender INTEGER,
  Customer_User_ServiceProviderId VARCHAR(20),
  CONSTRAINT PK_EVENTS_RATINGINPUT_10001 PRIMARY_KEY (USEID, EVENTID)
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGINPUT_10001
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

COMMENT ON RTCS.EVENTS_RATINGINPUT_10001 ( 
  USEID IS 'Technical field, implicitely set. An event is uniquely identified by the pair UseID x EventID. Extended event records refer to the original event record through USEID x EVENTID. ',
  EVENTID IS 'Technical field, implicitely set. An event is uniquely identified by the pair UseID x EventID. ',
  SS7_ISROAMING IS 'This parameter informs that the subscriber is roaming that is detected by the service.',
  SS7_ISCF IS 'This is the call forwarding indicator. Call forwarding is detected by the service.',
  SS7_NORMALIZEDCALLEDWITHOUTMNPPREFIX IS 'If the IN service supports mobile number portability, the called party numbers can contain special -operator specific- prefixes. This member provides the normalized called party number without the mnp prefix.',
  SS7_NORMALIZEDVLRNUMBER IS 'This parameter contains the normalized VLR number and is provided by the service.',
  SS7_NORMALIZEDSMSMESSAGECENTER IS 'This parameter contains the normalized SMSC address.',
  SS7_EXPANDEDCALLKIND IS 'This parameter provides the detected bearer service. It is the expanded trigger information call kind and should contain additional bearer service like UMTS or video telephonie etc',
  SS7_NORMALIZEDLOCATIONNUMBERA IS 'This parameter contains the normalized A-Location number and is provided by the service.',
  SS7_NORMALIZEDVISITEDMSCNUMBER IS 'Optional field that contains the V-MSC address, if it is provided by the triggering protocol. The V-MSC address will be used e.g. for sending PP Tracker SMS directly.',
  SS7_IMSI IS 'Optional field that contains the IMSI (International Mobile Subscriber Identity), if it is provided by the triggering protocol.',
  SS7_ACCESSFLAG IS 'This parameter indicates the type of access: 0 - MOC, 1 - MTC, 2 - Dial, 3 - USSD, 4 - E-mail, 5 - UCB, 6 - SMS-MO, 7 - GPRS, 8 - SMS-MT',
  SS7_MSISDNOFSUBSCRIBER IS 'This parameter stores the subscribers msisdn',
  SS7_ISUCB IS 'This parameter indicates whether the call was triggered via UCB or not.',
  SS7_LOGINTYPE IS 'Deprecated. An optional field! Identifies the customers login path. 0 = Default from another network element 7 = Static order; means: - DBA - Basic fee',
  RATING_ORIGINATORLOCATIONINFO IS 'Location information of originator.',
  EVENT_LOCATIONINFOTYPE IS 'Optional, only available if protocol provides the information (location/cellId/serviceAreaId).',
  RATING_ORIGINATORCELLID IS 'Deprecated ? ignore content. Replaced by rating.originatorLocationInfo.',
  RATING_ISROAMING IS 'This parameter informs about the roaming status of the subscriber.',
  RATING_ROAMINGZONE IS 'This parameter contains the textual representation of a roaming zone, e.g. NorthGermany.',
  RATING_ROAMINGTIMEZONE IS 'This parameter contains the time zone assigned to a roaming zone, e.g. GMT+1.',
  EVENT_IS3G IS 'Optional, only available if protocol provides the information (location/cellId/serviceAreaId). True when the customer is in 3G network.',
  NETWORK_CIR_CALLSTOPTIMELOCAL IS 'Optional (depends on MOC-GeneralSetting-CirDataSetting flag) parameter retrieved from CallInfoReport. Stores local call stop time. Format: Unixtime, unit: seconds.',
  NETWORK_CIR_CALLSTOPTIMETIMEZONE IS 'Optional (depends on MOC-GeneralSetting-CirDataSetting flag) parameter retrieved from CallInfoReport. Stores call stop time timezone.',
  NETWORK_CIR_CALLATTEMPTELAPSEDTIMEVALUE IS 'Optional (depends on MOC-GeneralSetting-CirDataSetting flag) parameter retrieved from CallInfoReport. Stores Call attempt elapsed time. Unit: seconds',
  NETWORK_CIR_CALLCONNECTEDTODESTINATIONTIME IS 'Optional (depends on MOC-GeneralSetting-CirDataSetting flag) parameter retrieved from CallInfoReport. Stores call connected to destination time. Unit: 1/10 seconds.',
  NETWORK_CIR_RELEASECAUSE IS 'Optional (depends on MOC-GeneralSetting-CirDataSetting flag) parameter retrieved from CallInfoReport. Stores release cause.',
  RATING_NORMALIZEDDESTINATIONWITHOUTMNPPREFIX IS 'If the IN service supports mobile number portability, the called party numbers can contain special -operator specific- prefixes. Provides the normalized called party number without the mnp prefix.',
  RATING_NORMALIZEDVLR_ORIGINATOR IS 'This parameter is only relevant if HRZ mapping library is not used. It contains the normalized VLR number of the caller or session originator and is provided by the service.',
  RATING_NORMALIZEDDESTINATION IS 'This parameter contains the normalized destination that is determined by the service (means normalized CdPA after rerouting and/or MNP etc.).',
  RATING_ISCF IS 'This is the call forwarding indicator. Call forwarding is detected by the service.',
  ENTRY_SS7DIALEDNUMBER IS 'Dialed number without overDialedDigits as received by IDP. Available for SS7 originating calls only.',
  SS7_NORMALIZEDCALLED IS 'This parameter contains the normalized destination that is determined by the service (means normalized CdPA after rerouting and/or MNP etc.). In case of SMS-MT content is CALLING party.',
  SS7_NORMALIZEDCALLER IS 'This parameter is provided by the service and contains the normalized calling party. The caller number is usable to determine origin to destination relation by terminating calls of roaming subscribers.',
  EVENT_DURATION IS 'Total call duration of event [sec]. Dedicated for supervision result and time-based session charging only. This field is optional and will not be available for all use cases. (set to 0)',
  Event_CallSetupTime IS 'Total call attempt setup time [sec]. Optional, present in case of time-based session charging.',
  Event_Volume IS 'Total volume of event (accumulated up+dn-link) [byte]. Relevant only for volume-based session charging. Please ignore for non-session charging use cases: MOC, MTC, SMS, Admin, Subscriptions, ...',
  Event_StopTime IS 'Stop time of event, or Stop/End Time of a session [sec since 1.1.1970]. Optional, not present in case of charge free use cases.',
  Event_ReguidingInfo IS 'Info about applied offer reguiding and fallback.',
  Event_isSecondary IS 'for system internal usage only',
  wmca_EventOrigin IS 'Identification if event was written during onTouch processing or not. Optional, it is only part of the event if value is not 0. 0 - default, 1 - onTouch running. ',
  Event_ReGuidingTargetOfferId IS 'Key of target offer used by offer reguiding. Written, if offer reguiding is activated in (default) offer configuration. In case of applied customer reguiding, this attribute refers to target customer (hierarchy).',
  Event_ReGuidingFallbackOfferId IS 'Key of default offer used by offer reguiding fallback. Written, if offer reguiding is actvated in (default) offer configuration.',
  Event_OriginalEventId IS 'Store the original eventId delivered by the mediation in case of OffCS events. Optional, written for offline charging use cases.',
  Customer_User_IMSI IS 'Customer.IMSI attribute of entry customer, if provisioned.',
  CALLREFERENCENUMBER IS 'CallReferenceNumber from SS7 Trigger. Optional, present if provided by network',
  Event_ReguidingSourceCustomerInfo IS 'see Event.ReguidingInfo. Indicates applied customer reguiding and logs offer reguiding on source customer (hierarchy).',
  Event_ReGuidingSourceCustomerTargetOfferId IS 'see Event.ReGuidingTargetOfferId. Indicates applied customer reguiding and logs offer reguiding on source customer (hierarchy).',
  Event_ReGuidingSourceCustomerFallbackOfferId IS 'see Event.ReGuidingFallbackOfferId. Indicates applied customer reguiding and logs offer reguiding on source customer (hierarchy).',
  Event_ErrorInfo_FailedTableName IS 'Table name where an error occurred, i.e. a searched key not found.',
  Event_ErrorInfo_FailedSearchKey IS 'Search key name of the table where an error occurred.',
  Event_ErrorInfo_FailedSearchKeyContent IS 'Search key content of the table where an error occurred.',
  wmca_Event_QuotaDimensionSize IS 'The quota dimension size of 0 indicates zero-dimensional quota and sizes greater 0 indicate quota with physical dimensions.',
  wmca_Event_QuotaDimensionType1 IS 'The type of quota one respectively Event.Duration. The type is one of the values 1=TIME_DIMENSION, 2=NONTIME_DIMENSION, 3=VOLUME_DIMENSION, 4=PRICE_DIMENSION, 5=BOOLEAN_DIMENSION. If not applicable 0 (unused) is set.',
  wmca_Event_QuotaPrecision1 IS 'Quota precision for quota one. If the quota holds time units in e.g. 1/10 seconds, the precision value is 10. Value 0 corresponds to unused.',
  wmca_Event_QuotaScalingFactor1 IS 'Scaling factor for quota one. If time unit in the system is seconds and a quota value comes in minutes, then the scaling factor is 60 since 1 [min] = 1 [60 s]. If not applicable value 0 (unused) is set.',
  wmca_Event_QuotaDimensionType2 IS 'The type of quota two respectively Event.Volume. The type is one of the values: 1=TIME_DIMENSION, 2=NONTIME_DIMENSION, 3=VOLUME_DIMENSION, 4=PRICE_DIMENSION, 5=BOOLEAN_DIMENSION. If not applicable value 0 (unused) is set.',
  wmca_Event_QuotaPrecision2 IS 'Quota precision for quota two. If the quota holds time units in e.g. 1/10 seconds, the precision value is 10. Value 0 corresponds to unused.',
  wmca_Event_QuotaScalingFactor2 IS 'Scaling factor for quota two. If time unit in the system is seconds and a quota value comes in minutes, then the scaling factor is 60 since 1 [min] = 1 [60 s]. If not applicable value 0 (unused) is set.',
  Customer_ROP_s_OfferId IS 'It denotes the contract or offer of the corresponding customer instances. Technically, the id of the offer is used to find within the blue data the corresponding instances of the blue data and flexible logics',
  OnTouchHandling_LogInfo IS 'Internal status of on touch handling is stored for analysis. At the external interface the following must be obeyed: value 0 = normal touch processing, value 1..9 = touch processing a little slow, all other values: potential problems in touch processing',
  Event_FarmNodeId IS 'ID of farm node, where the event is generated (concatenation of clustername and CE ID).',
  Event_ProcessId IS 'ID of process, which generated the event (visible in trace, e.g. CCC_USER_258).',
  Event_ContextId IS 'Context Id, which was active during event creation. Format is hex, e.g. a082000000020a69 (visible in trace).',
  LogicProcessing_LogInfo IS 'In this field the internal statistcs of flex logic execution is stored. Format is >step count<,>weighted step count<',
  Customer_CDP_DateOfBirth IS 'date of birth of the customer (as stored in the CDP profile)',
  Customer_CDP_Gender IS 'gender of the customer: 0 =unknown 1 = male, 2 = female (as stored in the CDP profile)',
  Customer_User_ServiceProviderId IS 'This value defines Customers ServiceProviderId which is used in MVNO scenarios.'
  );

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGINPUT_10001
	ALLOW WRITE ACCESS;

COMMIT;

/* ## 13 ##################################################################################################################### 

    Table EVENTS_RATINGIRRELEVANTOUTPUT_10001 
    Source Events.RATINGIRRELEVANTOUTPUT
    
*/

CREATE TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_10001 (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  CIF INTEGER,
  FCF INTEGER,
  CON INTEGER,
  CUF INTEGER,
  CTR VARCHAR(100),
  CLIM BIGINT,
  OCGPA VARCHAR(100),
  ENI INTEGER,
  IMEI VARCHAR(50),
  CUC INTEGER,
  UIS_firstAnnoId INTEGER,
  UIS_lastAnnoId INTEGER,
  FC INTEGER,
  CONSTRAINT PK_EVENTS_RATINGIRRELEVANTOUTPUT_10001 PRIMARY KEY ( USEID, EVENTID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_10001
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_10001
	ALLOW WRITE ACCESS;

COMMIT;

/* ## 14 ##################################################################################################################### 

    Table EVENTS_PROJECTADDON_10001 
    Source Events.PROJECTADDON
    
*/

CREATE TABLE RTCS.EVENTS_PROJECTADDON_10001 (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  MocCustPoint_timeIfNoTariffSwitch BIGINT,
  CONSTRAINT PK_EVENTS_PROJECTADDON_10001 PRIMARY KEY ( USEID, EVENTID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_PROJECTADDON_10001
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_PROJECTADDON_10001
	ALLOW WRITE ACCESS;

COMMIT;
