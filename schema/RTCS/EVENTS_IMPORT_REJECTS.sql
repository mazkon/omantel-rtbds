/*
    Project:     RTBDS
    Schema:      RTCS
    Description: Schema for events data
    
    Created:    2016-03-29
*/

/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'RTCS';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema RTCS'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = RTCS;

/* ## 1 ##################################################################################################################### 

    Table EVENTS_IMPORT_REJECTS

*/

CREATE TABLE RTCS.EVENTS_IMPORT_REJECTS (
  CREATED TIMESTAMP NOT NULL,
  SEQUENCE_ID BIGINT NOT NULL,
  JOB_NAME VARCHAR(50) NOT NULL,
  STAGE VARCHAR(50) NOT NULL,
  SOURCE_FILE VARCHAR(255),
  REJECT_REASON VARCHAR(255),
  SOURCE_ROW_NO BIGINT,
  RAW_DATA VARCHAR(32000)
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_IMPORT_REJECTS
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMENT ON RTCS.EVENTS_IMPORT_REJECTS ( 
  CREATED IS 'Reject create timestamp',
  SEQUENCE_ID IS 'Sequence where reject occured',
  JOB_NAME IS 'Name of the job where reject occured',
  STAGE IS 'Stage in the job where reject occured',
  SOURCE_FILE IS 'Name of the source data file',
  REJECT_REASON IS 'Reason of reject',
  SOURCE_ROW_NO IS 'Source data file row number (if available)',
  RAW_DATA IS 'raw data of rejected record'
  );

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_IMPORT_REJECTS
 ALLOW WRITE ACCESS;

COMMIT;