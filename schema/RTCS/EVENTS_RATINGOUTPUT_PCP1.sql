/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'RTCS';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema RTCS'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = RTCS;

/* ## 9 ##################################################################################################################### 

    Table EVENTS_RATINGOUTPUT_PCP1 
    Source Events.RATINGOUTPUT.PCP1
    
*/

CREATE TABLE RTCS.EVENTS_RATINGOUTPUT_PCP1 (
  USEID INTEGER	NOT NULL,
  EVENTID BIGINT NOT NULL,
  PerFactor VARCHAR(10),
  PerActivity INTEGER,
  PerState INTEGER
  /* ,
  PRIMARY KEY ( USEID, EVENTID ) */
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGOUTPUT_PCP1
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGOUTPUT_PCP1
	ALLOW WRITE ACCESS;

COMMIT;