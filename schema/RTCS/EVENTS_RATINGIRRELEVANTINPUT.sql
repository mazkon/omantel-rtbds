/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'RTCS';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema RTCS'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = RTCS;


/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGIRRELEVANTINPUT 
    Source Events.RatingIrrelevantInput
    
*/
CREATE TABLE RTCS.EVENTS_RATINGIRRELEVANTINPUT (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  CAINPUT VARCHAR(255),
  PRIMARY KEY ( USEID, EVENTID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGIRRELEVANTINPUT
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGIRRELEVANTINPUT
	ALLOW WRITE ACCESS;

COMMIT;
