/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'RTCS';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema RTCS'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = RTCS;

/* ## x ##################################################################################################################### 

    Table EVENTS_RATINGINPUT_11015
    Source Events.ratingInput

*/

CREATE TABLE RTCS.EVENTS_RATINGINPUT_11015 (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  Customer_ROP_s_OfferId VARCHAR(20),
  OnTouchHandling_LogInfo VARCHAR(50),
  Event_FarmNodeId VARCHAR(50),
  Event_ProcessId VARCHAR(20),
  Event_ContextId VARCHAR(20),
  LogicProcessing_LogInfo VARCHAR(30),
  Customer_CDP_DateOfBirth DATE,
  Customer_CDP_Gender INTEGER,
  EventInfo VARCHAR(20),
  ExternalClientUserName VARCHAR(50),
  ExternalClientIpAddress VARCHAR(15),
  ExternalClientIfType VARCHAR(20),
  Customer_User_ServiceProviderId VARCHAR(20),
  CA_Subscribe_Feat_TempCashLoan_UserNotification INTEGER,
  PRIMARY KEY ( USEID, EVENTID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGINPUT_11015
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMENT ON RTCS.EVENTS_RATINGINPUT_11015 ( 
	USEID IS '---',
	EVENTID IS '---'
	);

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGINPUT_11015
	ALLOW WRITE ACCESS;

COMMIT;

/* ## xx ##################################################################################################################### 

    Table EVENTS_RATINGIRRELEVANTOUTPUT_11015
    Source Events.RATINGIRRELEVANTOUTPUT
    
*/

CREATE TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_11015 (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  ServiceFee BIGINT,
  DailyFee BIGINT,
  BarringDays BIGINT,
  TCLLoanAmount BIGINT,
  TCLCounter INTEGER,
  TCLStatus INTEGER,
  RepaymentDate VARCHAR(19),
  RepaymentDueDate VARCHAR(19),
  BlockedTill VARCHAR(19),
  PreviousOutstandingAmount BIGINT,
  CurrentsOutstandingAmount BIGINT,
  TotalOfDailyFees BIGINT,
  TotalUsableBalance BIGINT,
  DaysLate BIGINT,
  RechargeAmount BIGINT,
  PRIMARY KEY ( USEID, EVENTID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_11015
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_11015
	ALLOW WRITE ACCESS;

COMMIT;
