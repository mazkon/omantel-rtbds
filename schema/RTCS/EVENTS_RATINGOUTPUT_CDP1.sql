/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'RTCS';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema RTCS'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = RTCS;

/* ## 12 ##################################################################################################################### 

    Table EVENTS_RATINGOUTPUT_CDP1 
    Source Events.RATINGOUTPUT.CDP1
    
*/

CREATE TABLE RTCS.EVENTS_RATINGOUTPUT_CDP1 (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  DISCOUNTCODE INTEGER NOT NULL,
  DISCOUNTPRICEUNITTYPE INTEGER	NOT NULL,
  DISCOUNTPRICE BIGINT NOT NULL
  /* ,
  PRIMARY KEY ( USEID, EVENTID ) */  -- TODO - PK disabled - we need to analyse data first and then define it
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGOUTPUT_CDP1
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGOUTPUT_CDP1
	ALLOW WRITE ACCESS;

COMMIT;