/*
    Project:     RTBDS
    Schema:      RTCS
    Description: Schema for events data
    
    Created:    2016-03-29
*/

/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'RTCS';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema RTCS'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = RTCS;

/* ## 1 ##################################################################################################################### 

    Table EVENTS

*/

CREATE TABLE RTCS.EVENTS (
  USEID	INTEGER	NOT NULL,
  EVENTID BIGINT NOT NULL,
  ACCESSKEY VARCHAR(20),
  ACCESSKEYTYPE INTEGER NOT NULL,
  OWNINGCUSTOMERID VARCHAR(20),
  ROOTCUSTOMERID VARCHAR(20),
  COMPOSEDCUSTOMERID VARCHAR(20),
  EVENTTYPE BIGINT NOT NULL,
  ORIGEVENTTIME TIMESTAMP NOT NULL,
  CREATIONEVENTTIME TIMESTAMP NOT NULL,
  EFFECTIVEEVENTTIME TIMESTAMP NOT NULL,
  BILLCYCLEID INTEGER,
  BILLPERIODID INTEGER,
  ERRORCODE INTEGER,
  RATEEVENTTYPE VARCHAR(20),
  EXTERNALCORRELATIONID VARCHAR(255),
  UNIVERSALATTRIBUTE1 VARCHAR(20),
  UNIVERSALATTRIBUTE2 VARCHAR(20),
  UNIVERSALATTRIBUTE3 VARCHAR(20),
  UNIVERSALATTRIBUTE4 VARCHAR(20),
  UNIVERSALATTRIBUTE5 VARCHAR(20),
  CONSTRAINT PK_EVENTS PRIMARY KEY ( USEID, EVENTID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMENT ON RTCS.EVENTS ( 
  USEID IS 'Technical field, implicitely set. An event is uniquely identified by the pair UseID x EventID. Extended event records refer to the original event record through USEID x EVENTID. ',
  EVENTID IS 'Technical field, implicitely set. An event is uniquely identified by the pair UseID x EventID. ',
  ACCESSKEY	IS 'Key to customer data, used in e.g. call processing use cases, here MSISDN of SS7 messages. Not available in case access was done via CustomerId (e.g. CA operation).',
  ACCESSKEYTYPE	IS 'Defines the type of access key, -1 = not available (CA operation), 0 = MSISDN, 1 = IMSI, 2 = SIP_URI. Note: With new use cases new values may be introduced.',
  OWNINGCUSTOMERID IS 'Default Value: N/A. ID of entry customer (that has access key (MSISDN, IMSI etc.) attached). Is equal to ROOTCUSTOMERID, if no hierarchy scenarios are applied.',
  ROOTCUSTOMERID IS 'Default Value: N/A. Unique ID of the customer that receives the bill. With single customers (not a member of family or a company hierarchy) ROOTCUSTOMERID == OWNINGCUSTOMERID',
  COMPOSEDCUSTOMERID IS 'Technical field. Currently not set',
  EVENTTYPE IS 'Specifies the type (sort of) event record. Per event type there may be 0..1 environment interfaces and 0..1 event profiles specifying content (and layout) of event records of the type.',
  ORIGEVENTTIME IS 'A.k.a. OriginalEventTime in EHAF operations. Date and time used by rating/charging for tariff selection. OrigEventTime is related to the time, the service has been provided to the end user.',
  CREATIONEVENTTIME IS 'System time of farm node when logging charged event to event record. Generally, EffectiveEventTime = ( EventCreationTime + some ms).',
  EFFECTIVEEVENTTIME IS 'Offline charging and billing of hybrid customers is based on "historized" customer data, with buckets aligned to bill periods and versions for administrative customer data changes.',
  BILLCYCLEID IS 'Identifies subscriber''s bill cycle. BillCycleId is administered on subscriber''s BOP. It comes from bill calendar (administered e.g via Customer Care).',
  BILLPERIODID IS 'Each bill cycle has multiple, numbered bill periods.Identifies period within referenced bill cycle, e.g. mothly 15th. - id=1 starts 15.jan.2012, id=2 - starts 15.feb.2012 etc.',
  ERRORCODE IS 'Event creating applications may here set an error code to indicate this event record not to be valid.',
  RATEEVENTTYPE IS 'Here the type of the event to be rated can be set by the application for later evaluation. Typical values would e.g. be ''MOC'' for a complete list and explanation check the event interface spec, table rate event types.',
  EXTERNALCORRELATIONID IS 'ID filled by applications to correlate events.  In TIS admin use cases the correlation ID may be set by the external system that initiated the administration via generic @MessageId control parameter (see TMAF IFSpec).'
  );

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS
	ALLOW WRITE ACCESS;

COMMIT;