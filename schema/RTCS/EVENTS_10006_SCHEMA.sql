/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'RTCS';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema RTCS'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = RTCS;

/* ## 35 ##################################################################################################################### 

    Table EVENTS_RATINGINPUT_10006 
    Source Events.ratingInput

*/

CREATE TABLE RTCS.EVENTS_RATINGINPUT_10006 (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  "CommonParameter.MCC" VARCHAR(3),
  "CommonParameter.MNC" VARCHAR(3),
  "DMI.REQUEST.CC-Request-Type" INTEGER,
  "DMI.REQUEST.Requested-Action" INTEGER,
  "DMI.REQUEST.Service-Information.MMS-Information.Message-Size" INTEGER,
  "DMI.REQUEST.Service-Information.MMS-Information.Message-Type" INTEGER,
  "DMI.REQUEST.Service-Information.MMS-Information.Originator-Address.Address-Domain.3GPP-IMSI-MCC-MNC" VARCHAR(50),
  "DMI.REQUEST.Service-Information.MMS-Information.Originator-Address.Address-Domain.Domain-Name" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Originator-Address.Address-Data" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Originator-Address.Address-Type" INTEGER,  
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:0.Address-Domain.3GPP-IMSI-MCC-MNC" VARCHAR(20),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:0.Address-Domain.Domain-Name" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:0.Address-Data" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:0.Address-Type" INTEGER,  
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:1.Address-Domain.3GPP-IMSI-MCC-MNC" VARCHAR(20),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:1.Address-Domain.Domain-Name" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:1.Address-Data" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:1.Address-Type" INTEGER,
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:2.Address-Domain.3GPP-IMSI-MCC-MNC" VARCHAR(20),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:2.Address-Domain.Domain-Name" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:2.Address-Data" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:2.Address-Type" INTEGER,
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:3.Address-Domain.3GPP-IMSI-MCC-MNC" VARCHAR(20),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:3.Address-Domain.Domain-Name" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:3.Address-Data" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:3.Address-Type" INTEGER,
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:4.Address-Domain.3GPP-IMSI-MCC-MNC" VARCHAR(20),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:4.Address-Domain.Domain-Name" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:4.Address-Data" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:4.Address-Type" INTEGER,
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:5.Address-Domain.3GPP-IMSI-MCC-MNC" VARCHAR(20),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:5.Address-Domain.Domain-Name" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:5.Address-Data" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:5.Address-Type" INTEGER,
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:6.Address-Domain.3GPP-IMSI-MCC-MNC" VARCHAR(20),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:6.Address-Domain.Domain-Name" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:6.Address-Data" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:6.Address-Type" INTEGER,
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:7.Address-Domain.3GPP-IMSI-MCC-MNC" VARCHAR(20),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:7.Address-Domain.Domain-Name" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:7.Address-Data" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:7.Address-Type" INTEGER,
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:8.Address-Domain.3GPP-IMSI-MCC-MNC" VARCHAR(20),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:8.Address-Domain.Domain-Name" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:8.Address-Data" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:8.Address-Type" INTEGER,
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:9.Address-Domain.3GPP-IMSI-MCC-MNC" VARCHAR(20),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:9.Address-Domain.Domain-Name" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:9.Address-Data" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:9.Address-Type" INTEGER,
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:10.Address-Domain.3GPP-IMSI-MCC-MNC" VARCHAR(20),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:10.Address-Domain.Domain-Name" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:10.Address-Data" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:10.Address-Type" INTEGER,
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:11.Address-Domain.3GPP-IMSI-MCC-MNC" VARCHAR(20),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:11.Address-Domain.Domain-Name" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:11.Address-Data" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:11.Address-Type" INTEGER,
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:12.Address-Domain.3GPP-IMSI-MCC-MNC" VARCHAR(20),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:12.Address-Domain.Domain-Name" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:12.Address-Data" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:12.Address-Type" INTEGER,
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:13.Address-Domain.3GPP-IMSI-MCC-MNC" VARCHAR(20),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:13.Address-Domain.Domain-Name" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:13.Address-Data" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:13.Address-Type" INTEGER,
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:14.Address-Domain.3GPP-IMSI-MCC-MNC" VARCHAR(20),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:14.Address-Domain.Domain-Name" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:14.Address-Data" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:14.Address-Type" INTEGER,
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:15.Address-Domain.3GPP-IMSI-MCC-MNC" VARCHAR(20),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:15.Address-Domain.Domain-Name" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:15.Address-Data" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:15.Address-Type" INTEGER,
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:16.Address-Domain.3GPP-IMSI-MCC-MNC" VARCHAR(20),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:16.Address-Domain.Domain-Name" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:16.Address-Data" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:16.Address-Type" INTEGER,
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:17.Address-Domain.3GPP-IMSI-MCC-MNC" VARCHAR(20),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:17.Address-Domain.Domain-Name" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:17.Address-Data" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:17.Address-Type" INTEGER,
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:18.Address-Domain.3GPP-IMSI-MCC-MNC" VARCHAR(20),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:18.Address-Domain.Domain-Name" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:18.Address-Data" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:18.Address-Type" INTEGER,
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:19.Address-Domain.3GPP-IMSI-MCC-MNC" VARCHAR(20),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:19.Address-Domain.Domain-Name" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:19.Address-Data" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:19.Address-Type" INTEGER,
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:20.Address-Domain.3GPP-IMSI-MCC-MNC" VARCHAR(20),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:20.Address-Domain.Domain-Name" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:20.Address-Data" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:20.Address-Type" INTEGER,
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:21.Address-Domain.3GPP-IMSI-MCC-MNC" VARCHAR(20),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:21.Address-Domain.Domain-Name" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:21.Address-Data" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:21.Address-Type" INTEGER,
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:22.Address-Domain.3GPP-IMSI-MCC-MNC" VARCHAR(20),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:22.Address-Domain.Domain-Name" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:22.Address-Data" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:22.Address-Type" INTEGER,
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:23.Address-Domain.3GPP-IMSI-MCC-MNC" VARCHAR(20),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:23.Address-Domain.Domain-Name" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:23.Address-Data" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:23.Address-Type" INTEGER,
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:24.Address-Domain.3GPP-IMSI-MCC-MNC" VARCHAR(20),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:24.Address-Domain.Domain-Name" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:24.Address-Data" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:24.Address-Type" INTEGER,
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:25.Address-Domain.3GPP-IMSI-MCC-MNC" VARCHAR(20),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:25.Address-Domain.Domain-Name" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:25.Address-Data" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:25.Address-Type" INTEGER,
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:26.Address-Domain.3GPP-IMSI-MCC-MNC" VARCHAR(20),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:26.Address-Domain.Domain-Name" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:26.Address-Data" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:26.Address-Type" INTEGER,
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:27.Address-Domain.3GPP-IMSI-MCC-MNC" VARCHAR(20),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:27.Address-Domain.Domain-Name" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:27.Address-Data" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:27.Address-Type" INTEGER,
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:28.Address-Domain.3GPP-IMSI-MCC-MNC" VARCHAR(20),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:28.Address-Domain.Domain-Name" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:28.Address-Data" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:28.Address-Type" INTEGER,
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:29.Address-Domain.3GPP-IMSI-MCC-MNC" VARCHAR(20),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:29.Address-Domain.Domain-Name" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:29.Address-Data" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:29.Address-Type" INTEGER,
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:30.Address-Domain.3GPP-IMSI-MCC-MNC" VARCHAR(20),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:30.Address-Domain.Domain-Name" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:30.Address-Data" VARCHAR(150),
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:30.Address-Type" INTEGER,
  "DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:L" INTEGER,
  "DMI.REQUEST.Service-Information.MMS-Information.VAS-Id" VARCHAR(50),
  "DMI.REQUEST.Service-Information.MMS-Information.VASP-Id" VARCHAR(50),
  DmiTickets_SubServiceId INTEGER,
  Event_is3G CHARACTER(1),
  "DMI.REQUEST.Multiple-Services-Credit-Control:0.Used-Service-Unit:0.CC-Service-Specific-Units" BIGINT,
  "DMI.REQUEST.Used-Service-Unit:0.CC-Service-Specific-Units" BIGINT,
  "DMTR_SUBREQUEST.Used-Service-Unit:0.CC-Service-Specific-Units" BIGINT,
  "DMTR_SUBREQUEST.Used-Service-Unit:0.CC-Total-Octets" BIGINT,
  "DMTR_SUBREQUEST.Used-Service-Unit:1.CC-Service-Specific-Units" BIGINT,
  "DMTR_SUBREQUEST.Used-Service-Unit:1.CC-Total-Octets" BIGINT,
  rating_SubServiceId INTEGER,
  rating_CC_Request_Type INTEGER,
  rating_originatorLocationInfo VARCHAR(50),
  Event_LocationInfoType INTEGER,
  RATING_ISROAMING CHARACTER(1),
  RATING_ROAMINGZONE VARCHAR(100),
  RATING_ROAMINGTIMEZONE VARCHAR(6),
  rating_Originator_3GPP_IMSI_MCC_MNC VARCHAR(6),
  rating_Recipient_L INTEGER,
  rating_Message_Size INTEGER,
  rating_Message_Type INTEGER,
  rating_VAS_Id VARCHAR(50),
  rating_VASP_Id VARCHAR(50),
  ENTRY_SS7DIALEDNUMBER VARCHAR(50),
  SS7_NORMALIZEDCALLED VARCHAR(50),
  SS7_NORMALIZEDCALLER VARCHAR(50),
  EVENT_DURATION BIGINT,
  Event_CallSetupTime INTEGER,
  Event_Volume BIGINT,
  Event_StopTime BIGINT,
  Event_ReguidingInfo INTEGER,
  Event_isSecondary CHARACTER(10),
  wmca_EventOrigin INTEGER,
  Event_ReGuidingTargetOfferId INTEGER,
  Event_ReGuidingFallbackOfferId INTEGER,
  Event_OriginalEventId BIGINT,
  Customer_User_IMSI VARCHAR(20),
  CALLREFERENCENUMBER VARCHAR(20),
  Event_ReguidingSourceCustomerInfo INTEGER,
  Event_ReGuidingSourceCustomerTargetOfferId INTEGER,
  Event_ReGuidingSourceCustomerFallbackOfferId INTEGER,
  Event_ErrorInfo_FailedTableName VARCHAR(20),
  Event_ErrorInfo_FailedSearchKey VARCHAR(20),
  Event_ErrorInfo_FailedSearchKeyContent VARCHAR(20),
  convChargingGA_chargeControl_ChargeMethod INTEGER,
  FBC_Event_defaultNumberMMS INTEGER,
  FBC_Event_DefaultSize INTEGER,
  FBC_SvcQuotaType INTEGER,
  FBC_SvcTypeEvent INTEGER,
  wmca_Event_QuotaDimensionSize INTEGER,
  wmca_Event_QuotaDimensionType1 INTEGER,
  wmca_Event_QuotaPrecision1 INTEGER,
  wmca_Event_QuotaScalingFactor1 BIGINT,
  wmca_Event_QuotaDimensionType2 INTEGER,
  wmca_Event_QuotaPrecision2 INTEGER,
  wmca_Event_QuotaScalingFactor2 BIGINT,
  Customer_ROP_s_OfferId VARCHAR(20),
  OnTouchHandling_LogInfo VARCHAR(50),
  Event_FarmNodeId VARCHAR(50),
  Event_ProcessId VARCHAR(20),
  Event_ContextId VARCHAR(20),
  LogicProcessing_LogInfo VARCHAR(30),
  Customer_CDP_DateOfBirth DATE,
  Customer_CDP_Gender INTEGER,
  SS7_IMSI VARCHAR(20),
  Customer_User_ServiceProviderId VARCHAR(20),
  PRIMARY KEY ( USEID, EVENTID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGINPUT_10006
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGINPUT_10006
	ALLOW WRITE ACCESS;

COMMIT;

/* ## 36 ##################################################################################################################### 

    Table EVENTS_RATINGINPUT_DMI_REQUEST_USER_LOCATION_INFO_10006
    Source Events.RATINGINPUT."DMI.REQUEST.Service-Information.PS-Information.3GPP-User-Location-Info"
    
*/

CREATE TABLE RTCS.EVENTS_RATINGINPUT_DMI_REQUEST_USER_LOCATION_INFO_10006 (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  UserLocationinfo INTEGER
  /* ,
  PRIMARY KEY ( USEID, EVENTID ) */
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGINPUT_DMI_REQUEST_USER_LOCATION_INFO_10006
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGINPUT_DMI_REQUEST_USER_LOCATION_INFO_10006
	ALLOW WRITE ACCESS;

COMMIT;

/* ## 37 ##################################################################################################################### 

    Table EVENTS_RATINGINPUT_RATINGX_NORMALIZEDDESTINATIONS_10006
    Source Events.RATINGINPUT."ratingX.normalizedDestinations"
    
*/

CREATE TABLE RTCS.EVENTS_RATINGINPUT_RATINGX_NORMALIZEDDESTINATIONS_10006 (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  ratingX_normalizedDestinations VARCHAR(100)
  /* ,
  PRIMARY KEY ( USEID, EVENTID ) */
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGINPUT_RATINGX_NORMALIZEDDESTINATIONS_10006
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGINPUT_RATINGX_NORMALIZEDDESTINATIONS_10006
	ALLOW WRITE ACCESS;

COMMIT;

/* ## 38 ##################################################################################################################### 

    Table EVENTS_RATINGINPUT_RATINGX_TYPEOFDESTINATIONS_10006
    Source Events.RATINGINPUT."ratingX.typeOfDestinations"
    
*/

CREATE TABLE RTCS.EVENTS_RATINGINPUT_RATINGX_TYPEOFDESTINATIONS_10006 (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  ratingX_typeOfDestinations INTEGER
  /* ,
  PRIMARY KEY ( USEID, EVENTID ) */
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGINPUT_RATINGX_TYPEOFDESTINATIONS_10006
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGINPUT_RATINGX_TYPEOFDESTINATIONS_10006
	ALLOW WRITE ACCESS;

COMMIT;

/* ## 46 ##################################################################################################################### 

    Table EVENTS_RATINGIRRELEVANTOUTPUT_10006_OK 
    Source Events.RATINGIRRELEVANTOUTPUT.OK
    
*/

CREATE TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_10006_OK (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  mseid VARCHAR(50),
  musrlo VARCHAR(50),
  msubt TIMESTAMP(0),
  cuc INTEGER,
  mfoc VARCHAR(200),
  munkn VARCHAR(200),
  PRIMARY KEY ( USEID, EVENTID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_10006_OK
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_10006_OK
	ALLOW WRITE ACCESS;

COMMIT;

/* ## 47 ##################################################################################################################### 

    Table EVENTS_RATINGIRRELEVANTOUTPUT_10006_ERR 
    Source Events.RATINGIRRELEVANTOUTPUT.ERR
    
*/

CREATE TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_10006_ERR (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  mseid VARCHAR(50),
  mevcau INTEGER,
  meveff INTEGER,
  merri VARCHAR(50),
  mfoc VARCHAR(200),
  mforb VARCHAR(200),
  munkn VARCHAR(200),
  PRIMARY KEY ( USEID, EVENTID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_10006_ERR
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGIRRELEVANTOUTPUT_10006_ERR
	ALLOW WRITE ACCESS;

COMMIT;