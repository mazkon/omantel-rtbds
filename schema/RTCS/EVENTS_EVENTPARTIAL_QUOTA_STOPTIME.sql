/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'RTCS';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema RTCS'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = RTCS;

/* ## 5 ##################################################################################################################### 

    Table EVENTS_EVENTPARTIAL_QUOTA_STOPTIME 
    Source Events.EventPartial_Quota_StopTime
    
*/

CREATE TABLE RTCS.EVENTS_EVENTPARTIAL_QUOTA_STOPTIME (
  USEID INTEGER NOT NULL,
  EVENTID BIGINT NOT NULL,
  EventPartial_Quota_StopTime BIGINT
  /* ,
  PRIMARY KEY ( USEID, EVENTID ) */  -- TODO - PK disabled - we need to analyse data first and then define it
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_EVENTPARTIAL_QUOTA_STOPTIME
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_EVENTPARTIAL_QUOTA_STOPTIME
	ALLOW WRITE ACCESS;

COMMIT;