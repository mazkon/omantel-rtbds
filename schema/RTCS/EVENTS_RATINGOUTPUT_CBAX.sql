/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'RTCS';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema RTCS'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = RTCS;

/* ## 9 ##################################################################################################################### 

    Table EVENTS_RATINGOUTPUT_CBAX 
    Source Events.RATINGOUTPUT.CBAX
    
*/

CREATE TABLE RTCS.EVENTS_RATINGOUTPUT_CBAX (
  USEID INTEGER	NOT NULL,
  EVENTID BIGINT NOT NULL,
  BALANCENAME VARCHAR(100),
  CHARGEDVALUE BIGINT,
  NEWBALANCE BIGINT,
  EXPIRYDATE TIMESTAMP,
  CREDITLIMIT INTEGER
  /* ,
  PRIMARY KEY ( USEID, EVENTID ) */  -- TODO - PK disabled - we need to analyse data first and then define it
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTCS.EVENTS_RATINGOUTPUT_CBAX
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE RTCS.EVENTS_RATINGOUTPUT_CBAX
	ALLOW WRITE ACCESS;

COMMIT;