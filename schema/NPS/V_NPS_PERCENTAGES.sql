/*
    Project:     RTBDS
    Schema:      NPS
    Table:       V_percentages
    Description: 
    
    Created:    2016-05-03
*/

/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'REPORTING_DATA';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema REPORTING_DATA'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = REPORTING_DATA;

Create table REPORTING_DATA.V_percentages as
( 
	select 
		month, 
		year_no, 
		month_no, 
		quarter_no,
		region_name as Answer_type,
		NPS as percentage
	from 
		REPORTING_DATA.V_NPS_MTH_REGION_OUTLET_LEVEL ) 
 with no data;

COMMIT;