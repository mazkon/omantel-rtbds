/*
    Project:     RTBDS
    Schema:      NPS
    Description: Schema for events data
    
    Created:    2016-03-29
*/

/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'NPS';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema NPS'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = NPS;

/* ## 1 ##################################################################################################################### 

    Table IMPORT_REJECTS

*/

CREATE TABLE NPS.NPS_IMPORT_REJECTS (
  CREATED TIMESTAMP NOT NULL,
  SEQUENCE_ID BIGINT NOT NULL,
  JOB_NAME VARCHAR(50) NOT NULL,
  STAGE VARCHAR(50) NOT NULL,
  SOURCE_FILE VARCHAR(255),
  REJECT_REASON VARCHAR(255),
  SOURCE_ROW_NO BIGINT,
  RAW_DATA VARCHAR(32000)
  ) 
  ORGANIZE BY ROW;

ALTER TABLE NPS.NPS_IMPORT_REJECTS
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMENT ON NPS.NPS_IMPORT_REJECTS ( 
  CREATED IS 'Reject create timestamp',
  SEQUENCE_ID IS 'Sequence where reject occured',
  JOB_NAME IS 'Name of the job where reject occured',
  STAGE IS 'Stage in the job where reject occured',
  SOURCE_FILE IS 'Name of the source data file',
  REJECT_REASON IS 'Reason of reject',
  SOURCE_ROW_NO IS 'Source data file row number (if available)',
  RAW_DATA IS 'raw data of rejected record'
  );

COMMIT;

RUNSTATS ON TABLE NPS.NPS_IMPORT_REJECTS
 ALLOW WRITE ACCESS;

COMMIT;
