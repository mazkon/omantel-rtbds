/*
    Project:     RTBDS
    Schema:      REPORTING_DATA
    View:        V_NPS_REGION_MAP2
    Description:  
    
    Created:    2016-05-02
*/

/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'REPORTING_DATA';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema REPORTING_DATA'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = REPORTING_DATA;

CREATE VIEW REPORTING_DATA.V_NPS_REGION_MAP2
AS
SELECT nps1.*, nps1.region AS region_name
  FROM reporting_data.v_nps_region_map nps1
WITH NO ROW MOVEMENT;

COMMIT;