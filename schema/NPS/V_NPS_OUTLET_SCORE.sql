/*
    Project:     RTBDS
    Schema:      NPS
    Table:       v_nps_outlet_score
    Description: 
    
    Created:    2016-05-03
*/

/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'REPORTING_DATA';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema REPORTING_DATA'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = REPORTING_DATA;

create table  reporting_data.v_nps_outlet_score as
(
	select 
		sum(case when rating_score='A' then 1 else 0 end) as promoter,
		sum(case when rating_score='B' then 1 else 0 end) as passive,
		sum(case when rating_score='C' then 1 else 0 end) as detractor,
	  	substr(cast(cast(QUESTION_SENT_TIME as date) as char(10)),1,7) as month,                                                                                             
	  	year(cast(QUESTION_SENT_TIME as date)) as year_no,                                                                                                                   
	  	quarter(cast(QUESTION_SENT_TIME as date)) as quarter_no,                                                                                                             
	  	month(cast(QUESTION_SENT_TIME as date)) as month_no,
	  	1 as region_id, 
	  	region as region_name,
	  	outlet_name
	from reporting_data.v_nps_answered 
	group by 
		substr(cast(cast(QUESTION_SENT_TIME as date) as char(10)),1,7),                                                                                             
		year(cast(QUESTION_SENT_TIME as date)),                                                                                                                   
		quarter(cast(QUESTION_SENT_TIME as date)),                                                                                                             
		month(cast(QUESTION_SENT_TIME as date)),
		region,
		outlet_name
) with no data;




COMMIT