/*
    Project:     RTBDS
    Schema:      NPS
    Table:       v_nps_answerrate_calc_total
    Description: 
    
    Created:    2016-04-28
*/

/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'REPORTING_DATA';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema REPORTING_DATA'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = REPORTING_DATA;


create table REPORTING_DATA.v_nps_answerrate_calc_total as
(
	select 
		sum(questions_answered) as questions_answered,
		sum(questions_asked) as questions_asked,
		cast((cast(sum(questions_answered) as decimal(8,2))*100)/sum(questions_asked) as decimal(8,2)) as answer_rate,
		month, 
		year_no, 
		quarter_no, 
		month_no
	from 
		reporting_data.v_nps_answerrate
	group by 
		month, 
		year_no, 
		quarter_no, 
		month_no
) with no data;



COMMIT