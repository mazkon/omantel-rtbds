/*
    Project:     RTBDS
    Schema:      NPS and REPORTING_SCHEMA
    Procedure:   REFRESH_NPS    
    Description: All SQL instructions to refresh data in views and tables 
    
    Created:    2016-04-28
*/

/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'NPS';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema NPS'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = NPS;

CREATE TABLE NPS.NPS_REFRESH_STATUS (
  DATE	TIMESTAMP	NOT NULL	DEFAULT ,
  TABLE	VARCHAR(30)	NOT NULL,
  REC_COUNT	INTEGER	NOT NULL,
  ACTION	CHARACTER(1)	NOT NULL	DEFAULT 'I'
  ) 
  IN USERSPACE1
  ORGANIZE BY ROW;

ALTER TABLE NPS.NPS_REFRESH_STATUS
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMENT ON NPS.NPS_REFRESH_STATUS ( 
	DATE IS 'When the action occured',
	TABLE IS 'Name of the refreshed table',
	REC_COUNT IS 'Number of preceeded records',
	ACTION IS 'Synonim for the action D (delete) , I (insert),   U  (update)' );

COMMIT;

RUNSTATS ON TABLE NPS.NPS_REFRESH_STATUS
	ALLOW WRITE ACCESS;

COMMIT;