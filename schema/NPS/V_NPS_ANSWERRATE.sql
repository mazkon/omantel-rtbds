/*
    Project:     RTBDS
    Schema:      REPORTING_DATA
    View:        V_NPS_ANSWERRATE
    Description:  
    
    Created:    2016-05-02
*/

/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'REPORTING_DATA';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema REPORTING_DATA'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = REPORTING_DATA;

CREATE VIEW REPORTING_DATA.V_NPS_ANSWERRATE
AS
SELECT region,
       count (*) AS questions_asked,
       sum (CASE WHEN (status = 'COMPLETED') THEN 1 ELSE 0 END) AS questions_answered,
	     TO_CHAR(QUESTION_SENT_TIME,'YYYY-MM') as month,  
       year (cast (QUESTION_SENT_TIME AS DATE)) AS year_no,
       quarter (cast (QUESTION_SENT_TIME AS DATE)) AS quarter_no,
       month (cast (QUESTION_SENT_TIME AS DATE)) AS month_no
  FROM reporting_data.v_nps_region_map
GROUP BY region,
		     TO_CHAR(QUESTION_SENT_TIME,'YYYY-MM'), 
         year (cast (QUESTION_SENT_TIME AS DATE)),
         quarter (cast (QUESTION_SENT_TIME AS DATE)),
         month (cast (QUESTION_SENT_TIME AS DATE))
WITH NO ROW MOVEMENT;

COMMIT;