/*
    Project:     RTBDS
    Schema:      MNP
    Table:       MNP_CALENDAR
    Description: Storage for Calendar 
    
    Created:    2016-04-28
*/

/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'REPORTING_DATA';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema REPORTING_DATA'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = REPORTING_DATA;


CREATE TABLE REPORTING_DATA.v_nps_answerrate_calc as
(
	select 
		1 as region_id,
		region as region_name,
		questions_answered,
		questions_asked,
		cast((cast(questions_answered as decimal(8,2))*100)/questions_asked as decimal(8,2)) as answer_rate,
		month, 
		year_no, 
		quarter_no, 
		month_no
	from reporting_data.v_nps_answerrate
) with no data;



COMMIT;