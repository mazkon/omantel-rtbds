/*
    Project:     RTBDS
    Schema:      NPS
    Table:       
    Description: 
    
    Created:    2016-05-03
*/

/* Create schema if not exists */

SET CURRENT SCHEMA = NPS;


CREATE PROCEDURE NPS.REFRESH_NPS ( )
  LANGUAGE SQL
  CALLED ON NULL INPUT
  INHERIT SPECIAL REGISTERS
BEGIN
  DECLARE NUM INT DEFAULT 0;
  DECLARE NUM1 INT DEFAULT 0;
  
  update NPS.NPS_DATA set region='D&D' where outlet_name='Adam';
  update NPS.NPS_DATA set region='MUSCAT' where outlet_name='Airport';
  update NPS.NPS_DATA set region='ALBATINAH' where outlet_name='Al Buraimi';
  update NPS.NPS_DATA set region='DHOFAR' where outlet_name='Al Hosn';
  update NPS.NPS_DATA set region='ALSHARQIYAH' where outlet_name='Al kamil';
  update NPS.NPS_DATA set region='MUSCAT' where outlet_name='Al Amerat';
  update NPS.NPS_DATA set region='MUSCAT' where outlet_name='Lulu Bousher';
  update NPS.NPS_DATA set region='DHOFAR' where outlet_name='Salalah Aqod';
  update NPS.NPS_DATA set region='DHOFAR' where outlet_name='Aqod Oman Mobile';
  update NPS.NPS_DATA set region='MUSCAT' where outlet_name='Al Bahja';
  update NPS.NPS_DATA set region='D&D' where outlet_name='Bahla';
  update NPS.NPS_DATA set region='MUSCAT' where outlet_name='Bank Muscat - HQ';
  update NPS.NPS_DATA set region='MUSCAT' where outlet_name='Al Khuwair-Sarooj';
  update NPS.NPS_DATA set region='ALBATINAH' where outlet_name='Barka';
  update NPS.NPS_DATA set region='ALBATINAH' where outlet_name='Lulu Barka';

  update NPS.NPS_DATA set region='ALBATINAH' where outlet_name='Daba';
  update NPS.NPS_DATA set region='ALSHARQIYAH' where outlet_name='Al Duqum Counter1';
  update NPS.NPS_DATA set region='ALSHARQIYAH' where outlet_name='Duqom ';
  update NPS.NPS_DATA set region='MUSCAT' where outlet_name='Ghala';
  update NPS.NPS_DATA set region='ALSHARQIYAH' where outlet_name='Ibra - New Counter';
  update NPS.NPS_DATA set region='D&D' where outlet_name='Ibri New Outlet';
  update NPS.NPS_DATA set region='ALSHARQIYAH' where outlet_name='Jalan BBA';
  update NPS.NPS_DATA set region='ALSHARQIYAH' where outlet_name='Jalan BBH';
  update NPS.NPS_DATA set region='MUSCAT' where outlet_name='Al Khoud';
  update NPS.NPS_DATA set region='MUSCAT' where outlet_name='KOM';

  update NPS.NPS_DATA set region='MUSCAT' where outlet_name='Lulu Darsit';
  update NPS.NPS_DATA set region='D&D' where outlet_name='Lulu Ibri';
  update NPS.NPS_DATA set region='ALBATINAH' where outlet_name='Lulu Khabura';
  update NPS.NPS_DATA set region='ALBATINAH' where outlet_name='Lulu Khasab';
  update NPS.NPS_DATA set region='D&D' where outlet_name='Lulu Nizwa';
  update NPS.NPS_DATA set region='DHOFAR' where outlet_name='Lulu Salalah';
  update NPS.NPS_DATA set region='MUSCAT' where outlet_name='Lulu Seeb';
  update NPS.NPS_DATA set region='MUSCAT' where outlet_name='Lulu - Wadikabeer';

  update NPS.NPS_DATA set region='MUSCAT' where outlet_name='AL GHUBRA MARS';
  update NPS.NPS_DATA set region='ALSHARQIYAH' where outlet_name='Masirah';
  update NPS.NPS_DATA set region='MUSCAT' where outlet_name='HQ';
  update NPS.NPS_DATA set region='MUSCAT' where outlet_name='Muscat City Center';
  update NPS.NPS_DATA set region='MUSCAT' where outlet_name='Grand Mall';
  update NPS.NPS_DATA set region='DHOFAR' where outlet_name='Mirbaat';
  update NPS.NPS_DATA set region='ALSHARQIYAH' where outlet_name='Al-Mudhaibi';
  update NPS.NPS_DATA set region='ALBATINAH' where outlet_name='Musanah';
  update NPS.NPS_DATA set region='ALBATINAH' where outlet_name='Nakhal';
  update NPS.NPS_DATA set region='D&D' where outlet_name='Nizwa';
  update NPS.NPS_DATA set region='MUSCAT' where outlet_name='Qurum City Center';
  update NPS.NPS_DATA set region='MUSCAT' where outlet_name='Quriyat';
  update NPS.NPS_DATA set region='MUSCAT' where outlet_name='Al Qurum';
  update NPS.NPS_DATA set region='ALBATINAH' where outlet_name='Rustaq';
  update NPS.NPS_DATA set region='MUSCAT' where outlet_name='Souq Ruwi';
  update NPS.NPS_DATA set region='DHOFAR' where outlet_name='Saadah (Salalah)';
  update NPS.NPS_DATA set region='ALBATINAH' where outlet_name='Safer Mall';
  update NPS.NPS_DATA set region='ALBATINAH' where outlet_name='Saham';
  update NPS.NPS_DATA set region='DHOFAR' where outlet_name='Salalah new';
  update NPS.NPS_DATA set region='DHOFAR' where outlet_name='Salalah';
  update NPS.NPS_DATA set region='DHOFAR' where outlet_name='Salalah(Airport)';
  update NPS.NPS_DATA set region='D&D' where outlet_name='Samayl';
  update NPS.NPS_DATA set region='DHOFAR' where outlet_name='Salalah Gardens Mall';
  update NPS.NPS_DATA set region='ALBATINAH' where outlet_name='Shanas';
  update NPS.NPS_DATA set region='ALSHARQIYAH' where outlet_name='Sinaw';
  update NPS.NPS_DATA set region='ALBATINAH' where outlet_name='Sohar New Counter';
  update NPS.NPS_DATA set region='ALBATINAH' where outlet_name='Sohar Industrial Area';
  update NPS.NPS_DATA set region='MUSCAT' where outlet_name='SQ University';
  update NPS.NPS_DATA set region='ALSHARQIYAH' where outlet_name='Sur OM';
  update NPS.NPS_DATA set region='ALSHARQIYAH' where outlet_name='SUR';
  update NPS.NPS_DATA set region='ALBATINAH' where outlet_name='Suwaiq';
  update NPS.NPS_DATA set region='DHOFAR' where outlet_name='Salalah Taqa';
  update NPS.NPS_DATA set region='DHOFAR' where outlet_name='Thumrait';
  update NPS.NPS_DATA set region='MUSCAT' where outlet_name='The Wave Muscat';
  update NPS.NPS_DATA set region='D&D' where outlet_name='Yanqual';
  update NPS.NPS_DATA set region='MUSCAT' where outlet_name='Zakher';
  update NPS.NPS_DATA set region='MUSCAT' where outlet_name='Muscat Festival - Amirat Park';
  update NPS.NPS_DATA set region='MUSCAT' where outlet_name='Muscat Festival - Naseem Park';
  update NPS.NPS_DATA set region='DHOFAR' where outlet_name='Salalah - Khareef Festival';
  
  
  DELETE FROM REPORTING_DATA.v_nps_answerrate_calc;
  GET DIAGNOSTICS NUM = ROW_COUNT;
  INSERT INTO NPS_REFRESH_STATUS (TABLE,REC_COUNT,ACTION)  VALUES ('v_nps_answerrate_calc',NUM,'D');
  insert into REPORTING_DATA.v_nps_answerrate_calc
    select 
   	  1 as region_id,
	    region as region_name,
    	questions_answered,
    	questions_asked,
    	cast((cast(questions_answered as decimal(8,2))*100)/questions_asked as decimal(8,2)) as answer_rate,
    	month, 
    	year_no, 
    	quarter_no, 
    	month_no
  from reporting_data.v_nps_answerrate; 
  GET DIAGNOSTICS NUM = ROW_COUNT;
  INSERT INTO NPS_REFRESH_STATUS (TABLE,REC_COUNT,ACTION)  VALUES ('v_nps_answerrate_calc',NUM,'I');

  DELETE FROM REPORTING_DATA.v_nps_answerrate_calc_total;
  GET DIAGNOSTICS NUM = ROW_COUNT;
  INSERT INTO NPS_REFRESH_STATUS (TABLE,REC_COUNT,ACTION)  VALUES ('v_nps_answerrate_calc_total',NUM,'D');
  insert into REPORTING_DATA.v_nps_answerrate_calc_total
  select 
  	sum(questions_answered) as questions_answered,
  	sum(questions_asked) as questions_asked,
  	cast((cast(sum(questions_answered) as decimal(8,2))*100)/sum(questions_asked) as decimal(8,2)) as answer_rate,
  	month, 
  	year_no, 
  	quarter_no, 
  	month_no
  from 
  	reporting_data.v_nps_answerrate
  group by 
  	month, 
  	year_no, 
  	quarter_no, 
  	month_no;
	GET DIAGNOSTICS NUM = ROW_COUNT;
  INSERT INTO NPS_REFRESH_STATUS (TABLE,REC_COUNT,ACTION)  VALUES ('v_nps_answerrate_calc_total',NUM,'I');
	
  DELETE FROM reporting_data.v_nps_aggregated ;
  GET DIAGNOSTICS NUM = ROW_COUNT;
  INSERT INTO NPS_REFRESH_STATUS (TABLE,REC_COUNT,ACTION)  VALUES ('v_nps_aggregated',NUM,'D');
  insert into reporting_data.v_nps_aggregated 
  select 
  	sum(case when rating_score='A' then 1 else 0 end) as promoter,
  	sum(case when rating_score='B' then 1 else 0 end) as passive,
  	sum(case when rating_score='C' then 1 else 0 end) as detractor,    
    TO_CHAR(QUESTION_SENT_TIME,'YYYY-MM') as month,	
  	year(cast(QUESTION_SENT_TIME as date)) as year_no,                                                                                                                   
  	quarter(cast(QUESTION_SENT_TIME as date)) as quarter_no,                                                                                                             
  	month(cast(QUESTION_SENT_TIME as date)) as month_no
  from 
  	reporting_data.v_nps_answered
  group by  
	  TO_CHAR(QUESTION_SENT_TIME,'YYYY-MM'),
  	year(cast(QUESTION_SENT_TIME as date)),                                                                                                                   
  	quarter(cast(QUESTION_SENT_TIME as date)),                                                                                                             
  	month(cast(QUESTION_SENT_TIME as date));
  GET DIAGNOSTICS NUM = ROW_COUNT;
  INSERT INTO NPS_REFRESH_STATUS (TABLE,REC_COUNT,ACTION)  VALUES ('v_nps_aggregated',NUM,'I');
  
  DELETE FROM reporting_data.v_nps_outlet_score ;
  GET DIAGNOSTICS NUM = ROW_COUNT;
  INSERT INTO NPS_REFRESH_STATUS (TABLE,REC_COUNT,ACTION)  VALUES ('v_nps_outlet_score',NUM,'D');
  insert into reporting_data.v_nps_outlet_score 
  select 
  	sum(case when rating_score='A' then 1 else 0 end) as promoter,
  	sum(case when rating_score='B' then 1 else 0 end) as passive,
  	sum(case when rating_score='C' then 1 else 0 end) as detractor,     
    TO_CHAR(QUESTION_SENT_TIME,'YYYY-MM') as month,		
  	year(cast(QUESTION_SENT_TIME as date)) as year_no,                                                                                                                   
  	quarter(cast(QUESTION_SENT_TIME as date)) as quarter_no,                                                                                                             
  	month(cast(QUESTION_SENT_TIME as date)) as month_no,
  	1 as region_id, 
  	region as region_name,
  	outlet_name
  from 
  	reporting_data.v_nps_answered 
  group by 
    TO_CHAR(QUESTION_SENT_TIME,'YYYY-MM') ,		
  	year(cast(QUESTION_SENT_TIME as date)),                                                                                                                   
  	quarter(cast(QUESTION_SENT_TIME as date)),                                                                                                             
  	month(cast(QUESTION_SENT_TIME as date)),
  	region,
     outlet_name;
  GET DIAGNOSTICS NUM = ROW_COUNT;
  INSERT INTO NPS_REFRESH_STATUS (TABLE,REC_COUNT,ACTION)  VALUES ('v_nps_outlet_score',NUM,'I');

  DELETE FROM reporting_data.v_nps_mth_region_outlet_level ;
  GET DIAGNOSTICS NUM = ROW_COUNT;
  INSERT INTO NPS_REFRESH_STATUS (TABLE,REC_COUNT,ACTION)  VALUES ('v_nps_mth_region_outlet_level',NUM,'D');
  insert into reporting_data.v_nps_mth_region_outlet_level 
  select 
  	month,
  	year_no, 
  	quarter_no, 
  	month_no,
  	case when (sum(promoter+detractor+passive)>10) then 1 else 0 end as region_id,
  	region_name, outlet_name, 
  	sum(promoter-detractor)*100 as positive,
  	sum(promoter+detractor+passive) as answer_sum , 
  	cast(sum(promoter-detractor)*10000*0.1/0.1/sum(promoter+detractor+passive)  as int)*0.1/10 as nps
  from
  	reporting_data.v_nps_outlet_score 
  group by month, year_no, quarter_no, month_no, region_id, region_name, outlet_name;
  GET DIAGNOSTICS NUM = ROW_COUNT;
  INSERT INTO NPS_REFRESH_STATUS (TABLE,REC_COUNT,ACTION)  VALUES ('v_nps_mth_region_outlet_level',NUM,'I');

  DELETE FROM reporting_data.v_nps_mth_region_level ;
  GET DIAGNOSTICS NUM = ROW_COUNT;
  INSERT INTO NPS_REFRESH_STATUS (TABLE,REC_COUNT,ACTION)  VALUES ('v_nps_mth_region_level',NUM,'D');
  insert into reporting_data.v_nps_mth_region_level 
  select 
  	month, 
  	year_no, 
  	quarter_no, 
  	month_no,
  	region_id,
  	region_name,
  	sum(promoter-detractor)*100 as positive,
  	sum(promoter+detractor+passive) as answer_sum, 
  	cast(sum(promoter-detractor)*10000*0.1/0.1/sum(promoter+detractor+passive)  as int)*0.1/10 as nps
  from 
  	reporting_data.v_nps_outlet_score 
  group by month, year_no, quarter_no, month_no, region_id, region_name;
  GET DIAGNOSTICS NUM = ROW_COUNT;
  INSERT INTO NPS_REFRESH_STATUS (TABLE,REC_COUNT,ACTION)  VALUES ('v_nps_mth_region_level',NUM,'I');

  DELETE FROM reporting_data.v_nps_mth_level;
  GET DIAGNOSTICS NUM = ROW_COUNT;
  INSERT INTO NPS_REFRESH_STATUS (TABLE,REC_COUNT,ACTION)  VALUES ('v_nps_mth_level',NUM,'D');
  insert into reporting_data.v_nps_mth_level 
  	select 
  		month,
  		year_no, 
  		quarter_no, 
  		month_no,
  		sum(promoter-detractor)*100 as positive,
  		sum(promoter+detractor+passive) as answer_sum, 
  		cast(sum(promoter-detractor)*10000*0.1/0.1/sum(promoter+detractor+passive)  as int)*0.1/10 as nps
  	from 
  		reporting_data.v_nps_outlet_score
  	group by month, year_no, quarter_no, month_no;
  GET DIAGNOSTICS NUM = ROW_COUNT;
  INSERT INTO NPS_REFRESH_STATUS (TABLE,REC_COUNT,ACTION)  VALUES ('v_nps_mth_level',NUM,'I');
  DELETE FROM REPORTING_DATA.V_percentages;
  GET DIAGNOSTICS NUM = ROW_COUNT;
  INSERT INTO NPS_REFRESH_STATUS (TABLE,REC_COUNT,ACTION)  VALUES ('V_percentages',NUM,'D');
  insert into REPORTING_DATA.V_percentages
  select 
  	month, 
  	year_no, 
  	month_no, 
  	quarter_no,
  	'PASSIVE',
  	cast((cast(sum(passive) as decimal(8,2))*100)/sum(DETRACTOR+promoter+ passive) as decimal(8,2))
  from 
  	REPORTING_DATA.V_NPS_aggregated
  group by 
  	month, 
  	year_no, 
  	month_no, 
  	quarter_no ;
  GET DIAGNOSTICS NUM = ROW_COUNT;
  SET NUM1 = NUM;
  
  insert into REPORTING_DATA.V_percentages
  select 
  	month, 
  	year_no, 
  	month_no, 
  	quarter_no,
  	'DETRACTOR',
  	cast((cast(sum(DETRACTOR) as decimal(8,2))*100)/sum(DETRACTOR+promoter+ passive) as decimal(8,2))
  from 
  	REPORTING_DATA.V_NPS_aggregated
  group by 
  	month, 
  	year_no, 
  	month_no, 
  	quarter_no ;
  GET DIAGNOSTICS NUM = ROW_COUNT;
  SET NUM1 = NUM + NUM1;
  
  insert into REPORTING_DATA.V_percentages
  select 
  	month, 
  	year_no, 
  	month_no, 
  	quarter_no,
  	'PROMOTER',
  	cast((cast(sum(promoter) as decimal(8,2))*100)/sum(DETRACTOR+promoter+ passive) as decimal(8,2))
  from 
  	REPORTING_DATA.V_NPS_aggregated
  group by 
  	month, 
  	year_no, 
  	month_no, 
  	quarter_no;
  GET DIAGNOSTICS NUM = ROW_COUNT;
  SET NUM1 = NUM + NUM1;  
  INSERT INTO NPS_REFRESH_STATUS (TABLE,REC_COUNT,ACTION)  VALUES ('V_percentages',NUM1,'I');

COMMIT;

END;
