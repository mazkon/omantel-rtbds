/*
    Project:     RTBDS
    Schema:      NPS
    Table:       v_nps_mth_region_level
    Description: 
    
    Created:    2016-05-03
*/

/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'REPORTING_DATA';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema REPORTING_DATA'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = REPORTING_DATA;

create table reporting_data.v_nps_mth_region_level as 
(
	select 
		month, 
		year_no, 
		quarter_no, 
		month_no,
		region_id,
		region_name,
		sum(promoter-detractor)*100 as positive,
		sum(promoter+detractor+passive) as answer_sum, 
		cast(sum(promoter-detractor)*10000*0.1/0.1/sum(promoter+detractor+passive)  as int)*0.1/10 as nps
	from 
		reporting_data.v_nps_outlet_score 
	group by month, year_no, quarter_no, month_no, region_id, region_name
) with no data;

COMMIT