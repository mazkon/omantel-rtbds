/*
    Project:     RTBDS
    Schema:      POS
    Description: Schema for POS data
    
    Created:    2016-04-11
*/

/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'POS';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema POS'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = POS;

CREATE TABLE POS.TRANSACTION_TYPES (
  TRANSACTION_TYPE	VARCHAR(10)	NOT NULL,
  TRANSACTION_DESC	VARCHAR(50),
  CONSTRAINT PK_TRANSACTION_TYPES PRIMARY KEY (TRANSACTION_TYPE)
  ) 
  ORGANIZE BY ROW;

ALTER TABLE POS.TRANSACTION_TYPES
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMENT ON POS.TRANSACTION_TYPES ( 
	TRANSACTION_TYPE IS 'Transaction type',
	TRANSACTION_DESC IS 'transaction description' );

COMMIT;

RUNSTATS ON TABLE POS.TRANSACTION_TYPES
	ALLOW WRITE ACCESS;

COMMIT;

/*
    Initial dictionary values
*/
INSERT INTO POS.TRANSACTION_TYPES VALUES ('D', 'Deposit Posting');
INSERT INTO POS.TRANSACTION_TYPES VALUES ('IS', 'Inventory Sold');
INSERT INTO POS.TRANSACTION_TYPES VALUES ('P', 'Receipt Posting');
INSERT INTO POS.TRANSACTION_TYPES VALUES ('R', 'Receipt Reversal');
INSERT INTO POS.TRANSACTION_TYPES VALUES ('S', 'Refund Reversal');
INSERT INTO POS.TRANSACTION_TYPES VALUES ('T', 'Deposit Refund');

COMMIT;

