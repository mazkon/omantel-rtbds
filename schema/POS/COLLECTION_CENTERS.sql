/*
    Project:     RTBDS
    Schema:      POS
    Description: Schema for POS data
    
    Created:    2016-04-11
*/

/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'POS';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema POS'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = POS;

/* ## 1 ##################################################################################################################### 

    Table COLLECTION_CENTERS

*/

CREATE TABLE POS.COLLECTION_CENTERS (
  REGION_ID VARCHAR(5) NOT NULL,
  COLLECTION_CENTER_ID VARCHAR(5) NOT NULL,
  COLLECTION_CENTER_DESC VARCHAR(50),
  SYS_CREATION_DATE TIMESTAMP(0) NOT NULL,
  SYS_UPDATE_DATE TIMESTAMP(0),
  PAYMENT_AGENCY_ID VARCHAR(6),
  COUNTER_TYPE VARCHAR(10),
  PRIMARY KEY ( REGION_ID, COLLECTION_CENTER_ID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE POS.COLLECTION_CENTERS
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMENT ON POS.COLLECTION_CENTERS ( 
  REGION_ID IS 'Region',
  COLLECTION_CENTER_ID IS 'Collection center ID',
  COLLECTION_CENTER_DESC IS 'Collection center description',
  SYS_CREATION_DATE	IS 'System creation date',
  SYS_UPDATE_DATE IS 'System update date',
  PAYMENT_AGENCY_ID IS 'Payment agency ID',
  COUNTER_TYPE IS 'Counter type'
  ); 

COMMIT;

RUNSTATS ON TABLE POS.COLLECTION_CENTERS
	ALLOW WRITE ACCESS;

COMMIT;