/*
    Project:     RTBDS
    Schema:      POS
    Description: Schema for POS data
    
    Created:    2016-04-11
*/

/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'POS';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema POS'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = POS;

/* ## 1 ##################################################################################################################### 

    Table REGIONS

*/

CREATE TABLE POS.REGIONS (
  REGION_ID VARCHAR(5) NOT NULL,
  REGION_DESC VARCHAR(50),
  SYS_CREATION_DATE TIMESTAMP(0) NOT NULL,
  SYS_UPDATE_DATE TIMESTAMP(0),
  PRIMARY KEY ( REGION_ID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE POS.REGIONS
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMENT ON POS.REGIONS ( 
  REGION_ID IS 'Region ID',
  REGION_DESC IS 'region description',
  SYS_CREATION_DATE IS 'System creation date',
  SYS_UPDATE_DATE IS 'System update date'
  );

COMMIT;

RUNSTATS ON TABLE POS.REGIONS
	ALLOW WRITE ACCESS;

COMMIT;