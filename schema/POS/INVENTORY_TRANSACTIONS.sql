/*
    Project:     RTBDS
    Schema:      POS
    Description: Schema for POS data
    
    Created:    2016-04-11
*/

/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'POS';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema POS'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = POS;

/* ## 1 ##################################################################################################################### 

    Table INVENTORY_TRANSACTIONS
    
    Caution:
        PRIMARY KEY definition - ( TRANSACTION_ID, ERP_TRANSACTION_ID ) - it looks ok, but
            TRANSACTION_ID at source is not declared as NOT NULL.
            ERP_TRANSACTION_ID at source is declared as NOT NULL, but contain duplicate values.

*/

CREATE TABLE POS.INVENTORY_TRANSACTIONS (
  TRANSACTION_ID BIGINT NOT NULL,
  ERP_TRANSACTION_ID BIGINT NOT NULL,
  ERP_INVENTORY_ITEM_NAME VARCHAR(100) NOT NULL,
  ERP_SUBINVENTORY_NAME VARCHAR(5) NOT NULL,
  DMA_SERVICE_CODE VARCHAR(20),
  TRANSACTION_TYPE VARCHAR(3) NOT NULL,
  ACTUAL_TRANSACTION_DATE TIMESTAMP(0),
  TRANSFER_QTY INTEGER,
  TRANSACTION_COST DECIMAL(12,3),
  AGENCY_ID VARCHAR(6),
  USER_ID VARCHAR(20) NOT NULL,
  SYS_CREATION_DATE TIMESTAMP(0),
  DEST_COUNTER_NAME VARCHAR(10),
  DEST_AGENCY_ID VARCHAR(3),
  ACCOUNTED VARCHAR(1),
  PRIMARY KEY ( TRANSACTION_ID, ERP_TRANSACTION_ID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE POS.INVENTORY_TRANSACTIONS
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMENT ON POS.INVENTORY_TRANSACTIONS ( 
  TRANSACTION_ID IS 'Transaction ID',
  ERP_TRANSACTION_ID IS 'ERP Transaction ID',
  ERP_INVENTORY_ITEM_NAME IS 'ERP inventory item name',
  ERP_SUBINVENTORY_NAME IS 'ERP subinventory name',
  DMA_SERVICE_CODE IS 'DMA service code',
  TRANSACTION_TYPE IS 'Transaction type',
  ACTUAL_TRANSACTION_DATE IS 'Actual transaction date',
  TRANSFER_QTY IS 'transfer quantity',
  TRANSACTION_COST IS 'Transaction cost',
  AGENCY_ID IS 'Agency ID',
  USER_ID IS 'User ID',
  SYS_CREATION_DATE IS 'System creation date',
  DEST_COUNTER_NAME IS 'Destination counter name',
  DEST_AGENCY_ID IS 'Destination agency ID',
  ACCOUNTED IS 'Accounted flag'
  );

COMMIT;

RUNSTATS ON TABLE POS.INVENTORY_TRANSACTIONS
	ALLOW WRITE ACCESS;

COMMIT;