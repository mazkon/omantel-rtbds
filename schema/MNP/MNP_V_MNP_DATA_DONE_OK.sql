/*
    Project:     RTBDS
    Schema:      MNP
    Table:       MNP_CALENDAR
    Description: Storage for Calendar 
    
    Created:    2016-04-28
*/

/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'MNP';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema MNP'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = MNP;
SET CURRENT SCHEMA = MNP;

CREATE OR REPLACE VIEW MNP.V_MNP_DATA_DONE_OK
AS
SELECT DISTINCT reqid
  FROM mnp.v_mnp_data_ok
 WHERE porting_main_state = 'DONE' AND porting_sub_state = 'COMPLETED'
WITH NO ROW MOVEMENT;

SET CURRENT SCHEMA = ISADMIN;

COMMIT;



COMMIT;