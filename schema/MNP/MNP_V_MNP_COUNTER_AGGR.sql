/*
    Project:     RTBDS
    Schema:      MNP
    Table:       MNP_CALENDAR
    Description: Storage for Calendar 
    
    Created:    2016-04-28
*/

/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'MNP';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema MNP'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = MNP;

CREATE VIEW MNP."V_MNP_COUNTER_AGGR"
AS
SELECT mnp.v_mnp_data_ok.donor,
       mnp.v_mnp_data_ok.recepient AS recipient,
       mnp.v_mnp_data_ok.number_type,
       count (DISTINCT mnp.v_mnp_data_ok.reqid) AS mnp_no,
       CASE
          WHEN (mnp.v_mnp_DATA_OK.DONOR = 'OMOB') THEN 'OUT'
          WHEN (mnp.v_mnp_DATA_OK.recepient = 'OMOB') THEN 'IN'
          ELSE 'OTHER'
       END
          AS port_ind,
          TO_CHAR(mnp.v_mnp_DATA_OK.DATE_TIME,'YYYY-MM') AS month, 
          year (cast (mnp.v_mnp_DATA_OK.DATE_TIME AS DATE)) AS year_no,
          quarter (cast (mnp.v_mnp_DATA_OK.DATE_TIME AS DATE)) AS quarter_no,
          month (cast (mnp.v_mnp_DATA_OK.DATE_TIME AS DATE)) AS month_no
  FROM mnp.v_mnp_data_ok
 WHERE porting_main_state = 'DONE' AND porting_sub_state = 'COMPLETED'
GROUP BY mnp.v_mnp_data_ok.donor,
         mnp.v_mnp_data_ok.recepient,
         mnp.v_mnp_data_ok.number_type,
         TO_CHAR(mnp.v_mnp_DATA_OK.DATE_TIME,'YYYY-MM'),
         year (cast (mnp.v_mnp_DATA_OK.DATE_TIME AS DATE)),
         quarter (cast (mnp.v_mnp_DATA_OK.DATE_TIME AS DATE)),
         month (cast (mnp.v_mnp_DATA_OK.DATE_TIME AS DATE))
WITH NO ROW MOVEMENT;

COMMIT;