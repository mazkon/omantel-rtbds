-- SET CURRENT SCHEMA = MNP;
-- SET CURRENT PATH = SYSIBM,SYSFUN,SYSPROC,SYSIBMADM,ISADMIN;

-- DROP PROCEDURE MNP.REFRESH_MNP;
CREATE OR REPLACE PROCEDURE MNP.REFRESH_MNP () 
  SPECIFIC SQL160428155622212
  LANGUAGE SQL
  NOT DETERMINISTIC
  EXTERNAL ACTION
  MODIFIES SQL DATA
  CALLED ON NULL INPUT
  INHERIT SPECIAL REGISTERS
  OLD SAVEPOINT LEVEL
BEGIN
  DECLARE DNUM INT DEFAULT 0;
  DECLARE NUM1 INT DEFAULT 0;
  DECLARE NUM2 INT DEFAULT 0;
  
  
  DELETE FROM MNP.V_MNP_COUNTER_AGGR_NEW;
  GET DIAGNOSTICS DNUM = ROW_COUNT;
  INSERT INTO MNP.MNP_REFRESH_STATUS (TABLE,REC_COUNT,ACTION)  VALUES ('v_mnp_counter_aggr_new',DNUM,'D');
  INSERT INTO MNP.V_MNP_COUNTER_AGGR_NEW

  SELECT 
       to_char(mnp.v_mnp_DATA_OK.DATE_TIME,'YYYY-MM') as month, 
       year (cast (mnp.v_mnp_DATA_OK.DATE_TIME AS DATE)) AS year_no,
       quarter (cast (mnp.v_mnp_DATA_OK.DATE_TIME AS DATE)) AS quarter_no,
       month (cast (mnp.v_mnp_DATA_OK.DATE_TIME AS DATE)) AS month_no,
       week (cast (mnp.v_mnp_DATA_OK.DATE_TIME AS DATE)) AS week_no,
       day (cast (mnp.v_mnp_DATA_OK.DATE_TIME AS DATE)) AS day_no,
       customer_type,
       cast (mnp.v_mnp_DATA_OK.DATE_TIME AS DATE) AS date,
       number_type,
       mnp.v_mnp_DATA_OK.DONOR,
       mnp.v_mnp_DATA_OK.recepient,
       CASE
          WHEN (mnp.v_mnp_DATA_OK.DONOR = 'OMOB')
          THEN
             mnp.v_mnp_DATA_OK.recepient
          ELSE
             mnp.v_mnp_DATA_OK.DONOR
       END
          AS Operator,
       sum (CASE WHEN (mnp.v_mnp_DATA_OK.DONOR = 'OMOB') THEN 1 ELSE 0 END)
          AS Port_out,
       sum (
          CASE WHEN (mnp.v_mnp_DATA_OK.RECePIENT = 'OMOB') THEN 1 ELSE 0 END)
          AS Port_in,
       sum (CASE WHEN (mnp.v_mnp_DATA_OK.DONOR = 'OMOB') THEN -1 ELSE 1 END)
          AS Port_net_value
  FROM mnp.v_mnp_data_ok
 WHERE     porting_main_state = 'DONE'
       AND porting_sub_state = 'COMPLETED'
       AND (   mnp.v_mnp_DATA_OK.DONOR = 'OMOB'
            OR mnp.v_mnp_DATA_OK.recepient = 'OMOB')
GROUP BY to_char(mnp.v_mnp_DATA_OK.DATE_TIME,'YYYY-MM'),
         year (cast (mnp.v_mnp_DATA_OK.DATE_TIME AS DATE)),
         quarter (cast (mnp.v_mnp_DATA_OK.DATE_TIME AS DATE)),
         month (cast (mnp.v_mnp_DATA_OK.DATE_TIME AS DATE)),
         week (cast (mnp.v_mnp_DATA_OK.DATE_TIME AS DATE)),
         day (cast (mnp.v_mnp_DATA_OK.DATE_TIME AS DATE)),
         cast (mnp.v_mnp_DATA_OK.DATE_TIME AS DATE),
         mnp.v_mnp_DATA_OK.DONOR,
         mnp.v_mnp_DATA_OK.recepient,
         customer_type,
         number_type; 
  
  GET DIAGNOSTICS NUM1 = ROW_COUNT;
  INSERT INTO MNP_REFRESH_STATUS (TABLE,REC_COUNT,ACTION)  VALUES ('v_mnp_counter_aggr_new',NUM1,'I');
  
  DELETE FROM mnp.v_mnp_duration_f1; 
  GET DIAGNOSTICS DNUM = ROW_COUNT;
  INSERT INTO MNP_REFRESH_STATUS (TABLE,REC_COUNT,ACTION)  VALUES ('v_mnp_duration_f1',DNUM,'D');
  insert into mnp.v_mnp_duration_f1
	select
		mnp1.reqid,
		mnp1.msisdn,
		donor,
		recipient,
		number_type,
		customer_type,
		port_ind,
		month,
		year_no,
		quarter_no,
		month_no,
		INT(
		((DOUBLE(DAYS(mnp2.dt)) * 86400) + MIDNIGHT_SECONDS(mnp2.dt)) -
		((DOUBLE(DAYS(mnp1.dt)) * 86400) + MIDNIGHT_SECONDS(mnp1.dt))
		)/3600 - hf1.dates_num*24 as hours_no
		,
		MOD(INT(
		((DOUBLE(DAYS(mnp2.dt)) * 86400) + MIDNIGHT_SECONDS(mnp2.dt)) -
		((DOUBLE(DAYS(mnp1.dt)) * 86400) + MIDNIGHT_SECONDS(mnp1.dt))
		),3600)/60 as minutes_no,
		
		cast((INT(
		((DOUBLE(DAYS(mnp2.dt)) * 86400) + MIDNIGHT_SECONDS(mnp2.dt)) -
		((DOUBLE(DAYS(mnp1.dt)) * 86400) + MIDNIGHT_SECONDS(mnp1.dt))
		)/3600*0.1/0.1 +
		MOD(INT(
		((DOUBLE(DAYS(mnp2.dt)) * 86400) + MIDNIGHT_SECONDS(mnp2.dt)) -
		((DOUBLE(DAYS(mnp1.dt)) * 86400) + MIDNIGHT_SECONDS(mnp1.dt))
		),3600)/60*0.1/0.1/60)*100 as integer)*0.01 - hf1.dates_num*24 as hours
	
	from
		(
			select 
				reqid, 
				phone_numbers as msisdn, 
				date_time as dt 
			from 
				MNP.V_MNP_DATA_OK 
			where 
				mnp.v_mnp_data_ok.porting_main_state='REQUESTED' 
				and mnp.v_mnp_data_ok.PORTING_SUB_STATE='UNDER PROCESS'
		) mnp1,
		(
			select 
				reqid,
	  			mnp.v_mnp_data_ok.donor,
	  			mnp.v_mnp_data_ok.recepient as recipient,
	  			mnp.v_mnp_data_ok.number_type,
	  			mnp.v_mnp_data_ok.customer_type,
	  			case
	  				when (mnp.V_MNP_DATA_OK.DONOR='OMOB') then 'OUT'
	  				when (mnp.V_MNP_DATA_OK.recepient='OMOB') then 'IN'
	  				else 'OTHER' 
	  			end as port_ind,
	  			-- substr(cast(cast(mnp.V_MNP_DATA_OK.DATE_TIME as date) as char(10)),1,7) as month,
				TO_CHAR(MNP.V_MNP_DATA_OK.DATE_TIME,'YYYY-MM') as month,
	  			year(cast(mnp.V_MNP_DATA_OK.DATE_TIME as date)) as year_no,
	  			quarter(cast(mnp.V_MNP_DATA_OK.DATE_TIME as date)) as quarter_no,
	  			month(cast(mnp.V_MNP_DATA_OK.DATE_TIME as date)) as month_no,
	  			date_time as dt 
	  		from 
	  			MNP.V_MNP_DATA_OK 
	  		where 
	  			mnp.v_mnp_data_ok.porting_main_state='ACCEPTED' 
	  			and mnp.v_mnp_data_ok.PORTING_SUB_STATE='COMPLETED'
	  	) mnp2,
	  	mnp.v_holi_dur_f1 hf1
	where 
		mnp1.reqid=mnp2.reqid
		and mnp1.reqid=hf1.reqid ;

  GET DIAGNOSTICS NUM1 = ROW_COUNT;
  INSERT INTO MNP_REFRESH_STATUS (TABLE,REC_COUNT,ACTION)  VALUES ('v_mnp_duration_f1',NUM1,'I');
  
  
  -------------------------------------------------------------------------------------------------
  -- mnp.v_mnp_duration_f2 
  
  DELETE FROM mnp.v_mnp_duration_f2; 
  GET DIAGNOSTICS DNUM = ROW_COUNT;
  INSERT INTO MNP_REFRESH_STATUS (TABLE,REC_COUNT,ACTION)  VALUES ('v_mnp_duration_f2',DNUM,'D');
  insert into mnp.v_mnp_duration_f2
	select
		mnp1.reqid,
		mnp1.msisdn,
		donor,
		recipient,
		number_type,
		customer_type,
		port_ind,
		month,
		year_no,
		quarter_no,
		month_no,
		INT(
		((DOUBLE(DAYS(mnp2.dt)) * 86400) + MIDNIGHT_SECONDS(mnp2.dt)) -
		((DOUBLE(DAYS(mnp1.dt)) * 86400) + MIDNIGHT_SECONDS(mnp1.dt))
		)/3600 - hf2.dates_num*24 as hours_no
		,
		MOD(INT(
		((DOUBLE(DAYS(mnp2.dt)) * 86400) + MIDNIGHT_SECONDS(mnp2.dt)) -
		((DOUBLE(DAYS(mnp1.dt)) * 86400) + MIDNIGHT_SECONDS(mnp1.dt))
		),3600)/60 as minutes_no,
		
		cast((INT(
		((DOUBLE(DAYS(mnp2.dt)) * 86400) + MIDNIGHT_SECONDS(mnp2.dt)) -
		((DOUBLE(DAYS(mnp1.dt)) * 86400) + MIDNIGHT_SECONDS(mnp1.dt))
		)/3600*0.1/0.1 +
		MOD(INT(
		((DOUBLE(DAYS(mnp2.dt)) * 86400) + MIDNIGHT_SECONDS(mnp2.dt)) -
		((DOUBLE(DAYS(mnp1.dt)) * 86400) + MIDNIGHT_SECONDS(mnp1.dt))
		),3600)/60*0.1/0.1/60)*100 as integer)*0.01 - hf2.dates_num*24 as hours
	from
		(
			select 
				reqid, 
				phone_numbers as msisdn, 
				date_time as dt 
			from 
				MNP.V_MNP_DATA_OK 
			where 
				mnp.v_mnp_data_ok.porting_main_state='ACCEPTED' 
				and mnp.v_mnp_data_ok.PORTING_SUB_STATE='COMPLETED'
		) mnp1,
		(
			select 
				reqid,
	  			mnp.v_mnp_data_ok.donor,
	  			mnp.v_mnp_data_ok.recepient as recipient,
	  			mnp.v_mnp_data_ok.number_type,
	  			mnp.v_mnp_data_ok.customer_type,
	  			case
	  				when (mnp.V_MNP_DATA_OK.DONOR='OMOB') then 'OUT'
	  				when (mnp.V_MNP_DATA_OK.recepient='OMOB') then 'IN'
	  				else 'OTHER' 
	  			end as port_ind,
	  			-- substr(cast(cast(mnp.V_MNP_DATA_OK.DATE_TIME as date) as char(10)),1,7) as month,
				TO_CHAR(MNP.V_MNP_DATA_OK.DATE_TIME,'YYYY-MM') as month,
	  			year(cast(mnp.V_MNP_DATA_OK.DATE_TIME as date)) as year_no,
	  			quarter(cast(mnp.V_MNP_DATA_OK.DATE_TIME as date)) as quarter_no,
	  			month(cast(mnp.V_MNP_DATA_OK.DATE_TIME as date)) as month_no,
	  			date_time as dt 
	  		from 
	  			MNP.V_MNP_DATA_OK 
	  		where 
	  			mnp.v_mnp_data_ok.porting_main_state='DONE' 
	  			and mnp.v_mnp_data_ok.PORTING_SUB_STATE='COMPLETED'
	  	) mnp2,
	  	mnp.v_holi_dur_f2 hf2
	where 
		mnp1.reqid=mnp2.reqid
		and mnp1.reqid=hf2.reqid;
  GET DIAGNOSTICS NUM1 = ROW_COUNT;
  INSERT INTO MNP_REFRESH_STATUS (TABLE,REC_COUNT,ACTION)  VALUES ('v_mnp_duration_f2',NUM1,'I');
  
  -------------------------------------------------------------------------------------------------- 
  -- v_mnp_process_error
  
  DELETE FROM v_mnp_process_error ;
  GET DIAGNOSTICS DNUM = ROW_COUNT;
  INSERT INTO MNP_REFRESH_STATUS (TABLE,REC_COUNT,ACTION)  VALUES ('v_mnp_process_error',DNUM,'D');
   INSERT INTO MNP.v_mnp_process_error SELECT  * FROM MNP.v_mnp_process_error_cancel;
  GET DIAGNOSTICS NUM1 = ROW_COUNT;
  INSERT INTO MNP.v_mnp_process_error SELECT  * FROM MNP.v_mnp_process_error_error;
  GET DIAGNOSTICS NUM2 = ROW_COUNT;
  INSERT INTO MNP_REFRESH_STATUS (TABLE,REC_COUNT,ACTION)  VALUES ('v_mnp_process_error',NUM1+NUM2,'I'); 
END;
