/*
    Project:     RTBDS
    Schema:      MNP
    Table:       MNP_CALENDAR
    Description: Storage for Calendar 
    
    Created:    2016-04-28
*/

/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'MNP';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema MNP'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = MNP;
CREATE VIEW MNP.V_HOLI_DUR_F2
AS
SELECT mnp1.reqid,
       sum (
          CASE
             WHEN (    cal.calendar_date <> mnp1.dt
                   AND cal.calendar_date <> mnp2.dt
                   AND cal.workday_ind = 0)
             THEN
                1
             ELSE
                0
          END)
          AS dates_num
  FROM (SELECT reqid, date (date_time) AS dt
          FROM MNP.V_MNP_DATA_OK
         WHERE     mnp.v_mnp_data_ok.porting_main_state = 'ACCEPTED'
               AND mnp.v_mnp_data_ok.PORTING_SUB_STATE = 'COMPLETED') mnp1,
       (SELECT reqid, date (date_time) AS dt
          FROM MNP.V_MNP_DATA_OK
         WHERE     mnp.v_mnp_data_ok.porting_main_state = 'DONE'
               AND mnp.v_mnp_data_ok.PORTING_SUB_STATE = 'COMPLETED') mnp2,
       mnp.calendar cal
 WHERE     mnp1.reqid = mnp2.reqid
       AND cal.calendar_date BETWEEN mnp1.dt AND mnp2.dt
GROUP BY mnp1.reqid
WITH NO ROW MOVEMENT;

COMMIT;



COMMIT;