/*
    Project:     RTBDS
    Schema:      MNP
    Description: Schema for MNP data
    
    Created:    2016-04-11
*/

/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'MNP';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema MNP'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = MNP;

/* ## 1 ##################################################################################################################### 

    Table SERVICES

*/

CREATE TABLE MNP.MNP_ERRMSG (
  ERROR_ID BIGINT NOT NULL,
  TYPE_ID INTEGER NOT NULL,
  DESCRIPTION VARCHAR(255),
  MSISDN VARCHAR(20),
  CONSTRAINT PK_MNP_ERRMSG PRIMARY KEY ( ERROR_ID, TYPE_ID )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE MNP.MNP_ERRMSG
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMENT ON MNP.MNP_ERRMSG ( 
  ERROR_ID IS 'Error ID',
  TYPE_ID IS 'Error type ID',
  DESCRIPTION IS 'Error description',
  MSISDN IS 'MSISDN'
  );

COMMIT;

RUNSTATS ON TABLE MNP.MNP_ERRMSG
	ALLOW WRITE ACCESS;

COMMIT;