/*
    Project:     RTBDS
    Schema:      MNP
    Table:       MNP_CALENDAR
    Description: Storage for Calendar 
    
    Created:    2016-04-28
*/

/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'MNP';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema MNP'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = MNP;

CREATE OR REPLACE VIEW MNP.V_MNP_DATA_OK
AS
SELECT *
  FROM mnp.mnp_data mnp1
  -- WHERE NOT EXISTS
  --        (SELECT 1
  --           FROM MNP.MNP_DATA AS mnp2
  --          WHERE mnp2.reqid = mnp1.reqid AND mnp2.REJECT_CODE > 0)
WITH NO ROW MOVEMENT;

COMMIT;