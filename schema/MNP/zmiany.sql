
-- Usunięcie kolumn, które już nie są w źródle 
-- 
alter table MNP.MNP_DATA drop column PRM_ID;
alter table MNP.MNP_DATA drop column REJECT_CODE;
alter table MNP.MNP_DATA drop column MESSAGE;

-- ponowne zalożenie widoków, które korzystały z usuniętych kolumn 
SET CURRENT SCHEMA = MNP;

SET CURRENT PATH = SYSIBM,SYSFUN,SYSPROC,SYSIBMADM,ISADMIN;
-- Warning: View create script may contain dropped or renamed columns and may require user modification
CREATE OR REPLACE VIEW MNP.V_MNP_DATA_OK
AS
select *  from mnp.mnp_data mnp1 
    -- where not exists (   select 1     from MNP.MNP_DATA as mnp2     where mnp2.reqid=mnp1.reqid      -- REJECT CODE WAS DELETED 
    -- and mnp2.REJECT_CODE >0
    -- )
WITH NO ROW MOVEMENT;

SET CURRENT SCHEMA = MNP;

SET CURRENT PATH = SYSIBM,SYSFUN,SYSPROC,SYSIBMADM,ISADMIN;

CREATE OR REPLACE VIEW MNP.V_MNP_PROCESS_ERROR_CANCEL
AS
SELECT reqid,
       donor,
       recepient AS recipient,
       number_type,
       CASE
          WHEN (DONOR = 'OMOB') THEN 'OUT'
          WHEN (recepient = 'OMOB') THEN 'IN'
          ELSE 'OTHER'
       END
          AS port_ind,
       substr (cast (cast (DATE_TIME AS DATE) AS CHAR (10)), 1, 7) AS month,
       year (cast (DATE_TIME AS DATE)) AS year_no,
       quarter (cast (DATE_TIME AS DATE)) AS quarter_no,
       month (cast (DATE_TIME AS DATE)) AS month_no,
       '' as reject_code,
       '' as reject_message
  FROM mnp.mnp_data
 WHERE porting_main_state = 'CANCELED' AND porting_sub_state = 'COMPLETED'
WITH NO ROW MOVEMENT;


-- Wywołanie procedury, która robi refresh na tabelach 
-- UWAGA ROBI SIĘ BARDZO DŁUGO 
CALL MNP.REFRESH_MNP(); 

