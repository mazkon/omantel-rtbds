/*
    Project:     RTBDS
    Schema:      MNP
    Table:       MNP_CALENDAR
    Description: Storage for Calendar 
    
    Created:    2016-04-28
*/

/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'MNP';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema MNP'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = MNP;

CREATE OR REPLACE VIEW MNP.V_MNP_PROCESS_ERROR_CANCEL
AS
SELECT reqid,
       donor,
       recepient AS recipient,
       number_type,
       CASE
          WHEN (DONOR = 'OMOB') THEN 'OUT'
          WHEN (recepient = 'OMOB') THEN 'IN'
          ELSE 'OTHER'
       END
          AS port_ind,
       -- substr (cast (cast (DATE_TIME AS DATE) AS CHAR (10)), 1, 7) AS month,
       TO_CHAR(DATE_TIME,'YYYY-MM') as month, 
       year (cast (DATE_TIME AS DATE)) AS year_no,
       quarter (cast (DATE_TIME AS DATE)) AS quarter_no,
       month (cast (DATE_TIME AS DATE)) AS month_no,
	     -1 AS reject_code,
       'Canceled' AS reject_message
  FROM mnp.mnp_data
 WHERE porting_main_state = 'CANCELED' AND porting_sub_state = 'COMPLETED'
WITH NO ROW MOVEMENT;

COMMIT;