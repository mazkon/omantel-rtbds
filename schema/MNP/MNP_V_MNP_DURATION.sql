/*
    Project:     RTBDS
    Schema:      MNP
    Table:       MNP_CALENDAR
    Description: Storage for Calendar 
    
    Created:    2016-04-28
*/

/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'MNP';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema MNP'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = MNP;

CREATE VIEW MNP.V_MNP_DURATION
AS
	SELECT mnp1.reqid,
	       donor,
	       recipient,
	       number_type,
	       port_ind,
	       month,
	       year_no,
	       quarter_no,
	       month_no,
	         INT (
	              (  (DOUBLE (DAYS (mnp2.dt)) * 86400)
	               + MIDNIGHT_SECONDS (mnp2.dt))
	            - (  (DOUBLE (DAYS (mnp1.dt)) * 86400)
	               + MIDNIGHT_SECONDS (mnp1.dt)))
	       / 3600
	          AS hours,
	         MOD (
	            INT (
	                 (  (DOUBLE (DAYS (mnp2.dt)) * 86400)
	                  + MIDNIGHT_SECONDS (mnp2.dt))
	               - (  (DOUBLE (DAYS (mnp1.dt)) * 86400)
	                  + MIDNIGHT_SECONDS (mnp1.dt))),
	            3600)
	       / 60
	          AS minutes,
	         cast (
	              (    INT (
	                        (  (DOUBLE (DAYS (mnp2.dt)) * 86400)
	                         + MIDNIGHT_SECONDS (mnp2.dt))
	                      - (  (DOUBLE (DAYS (mnp1.dt)) * 86400)
	                         + MIDNIGHT_SECONDS (mnp1.dt)))
	                 / 3600
	                 * 0.1
	                 / 0.1
	               +   MOD (
	                      INT (
	                           (  (DOUBLE (DAYS (mnp2.dt)) * 86400)
	                            + MIDNIGHT_SECONDS (mnp2.dt))
	                         - (  (DOUBLE (DAYS (mnp1.dt)) * 86400)
	                            + MIDNIGHT_SECONDS (mnp1.dt))),
	                      3600)
	                 / 60
	                 * 0.1
	                 / 0.1
	                 / 60)
	            * 100 AS INTEGER)
	       * 0.01
	          AS time_diff_dec
	  FROM (SELECT reqid, date_time AS dt
	          FROM MNP.V_MNP_DATA_OK
	         WHERE     mnp.v_mnp_data_ok.porting_main_state = 'REQUESTED'
	               AND mnp.v_mnp_data_ok.PORTING_SUB_STATE = 'UNDER PROCESS')
	       mnp1,
	       (SELECT reqid,
	               mnp.v_mnp_data_ok.donor,
	               mnp.v_mnp_data_ok.recepient AS recipient,
	               mnp.v_mnp_data_ok.number_type,
	               CASE
	                  WHEN (mnp.V_MNP_DATA_OK.DONOR = 'OMOB') THEN 'OUT'
	                  WHEN (mnp.V_MNP_DATA_OK.recepient = 'OMOB') THEN 'IN'
	                  ELSE 'OTHER'
	               END
	                  AS port_ind,
	                     TO_CHAR(mnp.V_MNP_DATA_OK.DATE_TIME,'YYYY-MM') AS month,
	               year (cast (mnp.V_MNP_DATA_OK.DATE_TIME AS DATE)) AS year_no,
	               quarter (cast (mnp.V_MNP_DATA_OK.DATE_TIME AS DATE))
	                  AS quarter_no,
	               month (cast (mnp.V_MNP_DATA_OK.DATE_TIME AS DATE)) AS month_no,
	               date_time AS dt
	          FROM MNP.V_MNP_DATA_OK
	         WHERE     mnp.v_mnp_data_ok.porting_main_state = 'DONE'
	               AND mnp.v_mnp_data_ok.PORTING_SUB_STATE = 'COMPLETED'
	               AND (   mnp.V_MNP_DATA_OK.DONOR = 'OMOB'
	                    OR mnp.V_MNP_DATA_OK.recepient = 'OMOB')) mnp2
	 WHERE mnp1.reqid = mnp2.reqid
WITH NO ROW MOVEMENT;

COMMIT;