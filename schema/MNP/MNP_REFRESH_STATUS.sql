/*
    Project:     RTBDS
    Schema:      MNP
    Table:       MNP_CALENDAR
    Description: Storage for Calendar 
    
    Created:    2016-04-28
*/

/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'MNP';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema MNP'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = MNP;

CREATE TABLE MNP.MNP_REFRESH_STATUS (
  DATE	TIMESTAMP	NOT NULL	DEFAULT ,
  TABLE	VARCHAR(30)	NOT NULL,
  REC_COUNT	INTEGER	NOT NULL,
  ACTION	CHARACTER(1)	NOT NULL	DEFAULT 'I'
  ) 
  IN USERSPACE1
  ORGANIZE BY ROW;

ALTER TABLE MNP.MNP_REFRESH_STATUS
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMENT ON MNP.MNP_REFRESH_STATUS ( 
	DATE IS 'When the action occured',
	TABLE IS 'Name of the refreshed table',
	REC_COUNT IS 'Number of preceeded records',
	ACTION IS 'Synonim for the action D (delete) , I (insert),   U  (update)' );

COMMIT;

RUNSTATS ON TABLE MNP.MNP_REFRESH_STATUS
	ALLOW WRITE ACCESS;

COMMIT;