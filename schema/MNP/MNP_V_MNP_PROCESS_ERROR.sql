/*
    Project:     RTBDS
    Schema:      MNP
    Table:       V_MNP_PROCESS_ERROR
    Description:  
    
    Created:    2016-04-28
*/

/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'MNP';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema MNP'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = MNP;


CREATE TABLE MNP.V_MNP_PROCESS_ERROR (
  REQID				BIGINT	NOT NULL,
  DONOR				VARCHAR(20),
  RECIPIENT			VARCHAR(20),
  NUMBER_TYPE		VARCHAR(20),
  PORT_IND			VARCHAR(5)	NOT NULL,
  MONTH				CHARACTER(7),
  YEAR_NO			INTEGER,
  QUARTER_NO		INTEGER,
  MONTH_NO			INTEGER,
  REJECT_CODE		INTEGER,
  REJECT_MESSAGE	VARCHAR(255)
  ) 
  IN USERSPACE1
  ORGANIZE BY ROW;

ALTER TABLE MNP.V_MNP_PROCESS_ERROR
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

RUNSTATS ON TABLE MNP.V_MNP_PROCESS_ERROR
	ALLOW WRITE ACCESS;

