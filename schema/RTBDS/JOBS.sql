/*
    Project:     RTBDS
    Schema:      RTBDS
    Description: 
    
    Created:    2016-04-29
*/

/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'RTBDS';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema RTBDS'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = RTBDS;

/* ## 1 ##################################################################################################################### 

    Table JOBS_MONITOR

*/

CREATE TABLE RTBDS.JOBS (
  SEQ_ID	BIGINT	NOT NULL,
  JOB_NAME	VARCHAR(255) NOT NULL,
  JOB_STATUS INTEGER,
  MESSAGE	VARCHAR(1024),
  CREATED_AT	TIMESTAMP	NOT NULL DEFAULT,
  CONSTRAINT PK_JOBS PRIMARY KEY ( SEQ_ID, JOB_NAME, CREATED_AT )
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTBDS.JOBS
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

COMMENT ON RTBDS.JOBS ( 
  SEQ_ID IS 'Sequence unique ID',
  JOB_NAME IS 'Job name',
  JOB_STATUS IS 'Job execution status',
  MESSAGE IS 'Message from job',
  CREATED_AT IS 'Register timestamp'
  );
  
COMMIT;

RUNSTATS ON TABLE RTBDS.JOBS
	ALLOW WRITE ACCESS;

COMMIT;
