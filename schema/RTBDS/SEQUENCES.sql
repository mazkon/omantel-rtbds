/*
    Project:     RTBDS
    Schema:      RTBDS
    Description: Schema for events data
    
    Created:    2016-03-29
*/

/* Create schema if not exists */
BEGIN
  DECLARE
    v_schema_cnt INTEGER; 
  BEGIN
    select count(1) into v_schema_cnt 
    from sysibm.sysschemata
    where name = 'RTBDS';

    IF (v_schema_cnt = 0) THEN -- schema not exists
      execute immediate 'create schema RTBDS'; --create schema
    END IF;
  END;
END;

SET CURRENT SCHEMA = RTBDS;

/* ## 1 ##################################################################################################################### 

    Table EVENTS_SEQUENCES

*/

CREATE TABLE RTBDS.SEQUENCES (
  SEQ_ID BIGINT NOT NULL,
  STARTED_AT TIMESTAMP NOT NULL DEFAULT ,
  FINISHED_AT TIMESTAMP,
  SEQ_NAME VARCHAR(100) NOT NULL,
  PROJECT VARCHAR(10),
  MESSAGE VARCHAR(1024),
  CONSTRAINT PK_SEQUENCES PRIMARY KEY (SEQ_ID)
  ) 
  ORGANIZE BY ROW;

ALTER TABLE RTBDS.SEQUENCES
  DATA CAPTURE NONE
  PCTFREE 0
  LOCKSIZE ROW
  APPEND OFF
  NOT VOLATILE;

COMMIT;

COMMENT ON RTBDS.SEQUENCES ( 
  SEQ_ID IS 'Sequence unique ID',
  STARTED_AT IS 'Sequence create timestamp',
  FINISHED_AT IS 'Sequence finish timestamp',
  SEQ_NAME IS 'Sequence name',
  PROJECT IS 'Name of RTBDS project',
  MESSAGE IS 'Sequence message'
  );

COMMIT;

RUNSTATS ON TABLE RTBDS.SEQUENCES
	ALLOW WRITE ACCESS;

COMMIT;
