#!/bin/sh
# Project: RTBDS
# Description:
# Move source data files from upload area to process area
#
# PARAMETERS
#   type - project specific string e.g. RTCS or NPS or MNP
#   file_mask - mask for processed files e.g. NPS_DATA_*.dat
#
# USAGE
#   import_prepare_source_data.sh NPS NPS_DATA_*.DAT
#
# RETURN CODES
#      POSITIVE
# 64 - Source folder is empty
#
#      NEGATIVE
# 74 - source folder not found
# 75 - destination folder cant be created
# 76 - cant move files to destination
#

SOURCE="/data/input/$1"
DESTINATION="/data/$1/PROCESSING"
FILES="$2"

# check source directory
if ! [ -d "$SOURCE" ] ; then
    exit 74
fi

# check destination directory
if ! [ -d "$DESTINATION" ] ; then

  mkdir "$DESTINATION"

  # break if can't create destination directory
  if ! [ $? -eq 0 ] ; then
    exit 75
  fi

fi

# check source dir content
if ! [ "$(ls -A $SOURCE/${FILES})" ]; then
     # Source dir is empty, positive break process
    exit 64
fi

mv ${SOURCE}/${FILES} ${DESTINATION}

# check files was moved
if ! [ $? -eq 0 ] ; then
    exit 76
fi
