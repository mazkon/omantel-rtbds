#!/bin/perl

##
## Definition of record for event 10006
##


our %record;

%record = ();

@header = ('UseID',
           'EventID',
           'AccessKey',
           'AccessKeyType',
           'OwningCustomerID',
           'RootCustomerID',
           'ComposedCustomerID',
           'EventType',
           'OrigEventTime',
           'CreationEventTime',
           'EffectiveEventTime',
           'BillCycleID',
           'BillPeriodID',
           'ErrorCode',
           'RateEventType',
           'ExternalCorrelationID',
           'RatingInput{}',
           'RatingIrrelevantInput{}',
           'RatingOutput{}',
           'RatingIrrelevantOutput{}',
           'UniversalAttribute1',
           'UniversalAttribute2',
           'UniversalAttribute3',
           'UniversalAttribute4',
           'UniversalAttribute5',
           'ProjectAddOn{}');

@rating_input = (
		'CommonParameter.MCC',
		'CommonParameter.MNC',
		'DMI.REQUEST.CC-Request-Type',
		'DMI.REQUEST.Requested-Action',
		'DMI.REQUEST.Service-Information.MMS-Information.Message-Size',
		'DMI.REQUEST.Service-Information.MMS-Information.Message-Type',
		'DMI.REQUEST.Service-Information.MMS-Information.Originator-Address.Address-Domain.3GPP-IMSI-MCC-MNC',
		'DMI.REQUEST.Service-Information.MMS-Information.Originator-Address.Address-Domain.Domain-Name',
		'DMI.REQUEST.Service-Information.MMS-Information.Originator-Address.Address-Data',
		'DMI.REQUEST.Service-Information.MMS-Information.Originator-Address.Address-Type',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:0.Address-Domain.3GPP-IMSI-MCC-MNC',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:0.Address-Domain.Domain-Name',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:0.Address-Data',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:0.Address-Type',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:1.Address-Domain.3GPP-IMSI-MCC-MNC',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:1.Address-Domain.Domain-Name',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:1.Address-Data',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:1.Address-Type',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:2.Address-Domain.3GPP-IMSI-MCC-MNC',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:2.Address-Domain.Domain-Name',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:2.Address-Data',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:2.Address-Type',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:3.Address-Domain.3GPP-IMSI-MCC-MNC',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:3.Address-Domain.Domain-Name',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:3.Address-Data',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:3.Address-Type',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:4.Address-Domain.3GPP-IMSI-MCC-MNC',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:4.Address-Domain.Domain-Name',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:4.Address-Data',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:4.Address-Type',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:5.Address-Domain.3GPP-IMSI-MCC-MNC',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:5.Address-Domain.Domain-Name',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:5.Address-Data',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:5.Address-Type',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:6.Address-Domain.3GPP-IMSI-MCC-MNC',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:6.Address-Domain.Domain-Name',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:6.Address-Data',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:6.Address-Type',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:7.Address-Domain.3GPP-IMSI-MCC-MNC',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:7.Address-Domain.Domain-Name',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:7.Address-Data',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:7.Address-Type',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:8.Address-Domain.3GPP-IMSI-MCC-MNC',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:8.Address-Domain.Domain-Name',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:8.Address-Data',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:8.Address-Type',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:9.Address-Domain.3GPP-IMSI-MCC-MNC',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:9.Address-Domain.Domain-Name',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:9.Address-Data',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:9.Address-Type',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:10.Address-Domain.3GPP-IMSI-MCC-MNC',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:10.Address-Domain.Domain-Name',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:10.Address-Data',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:10.Address-Type',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:11.Address-Domain.3GPP-IMSI-MCC-MNC',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:11.Address-Domain.Domain-Name',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:11.Address-Data',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:11.Address-Type',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:12.Address-Domain.3GPP-IMSI-MCC-MNC',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:12.Address-Domain.Domain-Name',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:12.Address-Data',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:12.Address-Type',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:13.Address-Domain.3GPP-IMSI-MCC-MNC',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:13.Address-Domain.Domain-Name',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:13.Address-Data',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:13.Address-Type',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:14.Address-Domain.3GPP-IMSI-MCC-MNC',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:14.Address-Domain.Domain-Name',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:14.Address-Data',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:14.Address-Type',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:15.Address-Domain.3GPP-IMSI-MCC-MNC',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:15.Address-Domain.Domain-Name',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:15.Address-Data',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:15.Address-Type',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:16.Address-Domain.3GPP-IMSI-MCC-MNC',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:16.Address-Domain.Domain-Name',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:16.Address-Data',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:16.Address-Type',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:17.Address-Domain.3GPP-IMSI-MCC-MNC',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:17.Address-Domain.Domain-Name',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:17.Address-Data',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:17.Address-Type',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:18.Address-Domain.3GPP-IMSI-MCC-MNC',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:18.Address-Domain.Domain-Name',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:18.Address-Data',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:18.Address-Type',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:19.Address-Domain.3GPP-IMSI-MCC-MNC',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:19.Address-Domain.Domain-Name',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:19.Address-Data',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:19.Address-Type',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:20.Address-Domain.3GPP-IMSI-MCC-MNC',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:20.Address-Domain.Domain-Name',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:20.Address-Data',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:20.Address-Type',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:21.Address-Domain.3GPP-IMSI-MCC-MNC',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:21.Address-Domain.Domain-Name',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:21.Address-Data',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:21.Address-Type',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:22.Address-Domain.3GPP-IMSI-MCC-MNC',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:22.Address-Domain.Domain-Name',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:22.Address-Data',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:22.Address-Type',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:23.Address-Domain.3GPP-IMSI-MCC-MNC',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:23.Address-Domain.Domain-Name',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:23.Address-Data',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:23.Address-Type',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:24.Address-Domain.3GPP-IMSI-MCC-MNC',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:24.Address-Domain.Domain-Name',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:24.Address-Data',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:24.Address-Type',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:25.Address-Domain.3GPP-IMSI-MCC-MNC',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:25.Address-Domain.Domain-Name',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:25.Address-Data',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:25.Address-Type',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:26.Address-Domain.3GPP-IMSI-MCC-MNC',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:26.Address-Domain.Domain-Name',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:26.Address-Data',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:26.Address-Type',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:27.Address-Domain.3GPP-IMSI-MCC-MNC',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:27.Address-Domain.Domain-Name',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:27.Address-Data',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:27.Address-Type',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:28.Address-Domain.3GPP-IMSI-MCC-MNC',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:28.Address-Domain.Domain-Name',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:28.Address-Data',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:28.Address-Type',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:29.Address-Domain.3GPP-IMSI-MCC-MNC',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:29.Address-Domain.Domain-Name',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:29.Address-Data',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:29.Address-Type',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:30.Address-Domain.3GPP-IMSI-MCC-MNC',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:30.Address-Domain.Domain-Name',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:30.Address-Data',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:30.Address-Type',
		'DMI.REQUEST.Service-Information.MMS-Information.Recipient-Address:L',
		'DMI.REQUEST.Service-Information.MMS-Information.VAS-Id',
		'DMI.REQUEST.Service-Information.MMS-Information.VASP-Id',
		'DMI.REQUEST.Service-Information.PS-Information.3GPP-User-Location-Info#',
		'DmiTickets.SubServiceId',
		'Event.is3G',
		'DMI.REQUEST.Multiple-Services-Credit-Control:0.Used-Service-Unit:0.CC-Service-Specific-Units',
		'DMI.REQUEST.Used-Service-Unit:0.CC-Service-Specific-Units',
		'DMTR_SUBREQUEST.Used-Service-Unit:0.CC-Service-Specific-Units',
		'DMTR_SUBREQUEST.Used-Service-Unit:0.CC-Total-Octets',
		'DMTR_SUBREQUEST.Used-Service-Unit:1.CC-Service-Specific-Units',
		'DMTR_SUBREQUEST.Used-Service-Unit:1.CC-Total-Octets',
		'rating.SubServiceId',
		'rating.CC_Request_Type',
		'rating.originatorLocationInfo',
		'Event.LocationInfoType',
		'rating.isRoaming',
		'rating.RoamingZone',
		'rating.RoamingTimeZone',
		'rating.Originator.3GPP_IMSI_MCC_MNC',
		'rating.Recipient_L',
		'rating.Message_Size',
		'rating.Message_Type',
		'rating.VAS_Id',
		'rating.VASP_Id',
		'ratingX.normalizedDestinations#',
		'ratingX.typeOfDestinations#',
		'Entry_ss7DialedNumber',
		'SS7_normalizedCalled',
		'SS7_normalizedCaller',
		'Event.Duration',
		'Event.CallSetupTime',
		'Event.Volume',
		'Event.StopTime',
		'Event.ReguidingInfo',
		'Event.isSecondary',
		'wmca.EventOrigin',
		'Event.ReGuidingTargetOfferId',
		'Event.ReGuidingFallbackOfferId',
		'Event.OriginalEventId',
		'Customer.User.IMSI',
		'CallReferenceNumber',
		'Event.ReguidingSourceCustomerInfo',
		'Event.ReGuidingSourceCustomerTargetOfferId',
		'Event.ReGuidingSourceCustomerFallbackOfferId',
		'Event.ErrorInfo.FailedTableName',
		'Event.ErrorInfo.FailedSearchKey',
		'Event.ErrorInfo.FailedSearchKeyContent',
		'convChargingGA.chargeControl_ChargeMethod',
		'FBC.Event.defaultNumberMMS',
		'FBC.Event.DefaultSize',
		'FBC.SvcQuotaType',
		'FBC.SvcTypeEvent',
		'EventPartial.Quota.Duration#',
		'EventPartial.Quota.Volume#',
		'EventPartial.Quota.StopTime#',
		'wmca.Event.QuotaDimensionSize',
		'wmca.Event.QuotaDimensionType1',
		'wmca.Event.QuotaPrecision1',
		'wmca.Event.QuotaScalingFactor1',
		'wmca.Event.QuotaDimensionType2',
		'wmca.Event.QuotaPrecision2',
		'wmca.Event.QuotaScalingFactor2',
		'Customer.ROP.s_OfferId',
		'OnTouchHandling.LogInfo',
		'Event.FarmNodeId',
		'Event.ProcessId',
		'Event.ContextId',
		'LogicProcessing.LogInfo',
		'Customer.CDP.DateOfBirth',
		'Customer.CDP.Gender',
		'SS7_IMSI',
		'Customer.User.ServiceProviderId'
				);
@rating_irr_in = ();
@rating_output = ('bopk','ResponsibleAccount','ropk','orig','htz','rerid','BillingType','OfferSubscriptionID','cco1{}','cbax[]','ibax[]','ccp1[]','cdp1[]');
		 
@cco1 =  ('GrantResult','ChargeResult','ReasonInfo','CallStopTime','CallDuration','TotalUsedVolume','TotalUsedQuota');
@cbax = ('BalanceName','ChargedValue','NewBalance','ExpiryDate','CreditLimit');
@ibax = ('BalanceName','IncrementValue','NewBalance','ExpiryDate','CreditLimit');
@ccp1 = ('ChargeCode','ChargeBeginTime','ChargedCallDuration','UsedCallDuration','ChargedVolume','UsedVolume','ChargeType',
	  'ChargedPriceUnitType','ChargedPrice','TaxInfo','TaxOfChargedPrice');
@cdp1 = ('DiscountCode','DiscountPriceUnitType','DiscountPrice');

@rating_irr_out = ('ok{}','err{}');
@ok  = ('msied','musrio','msubt','cuc','mfoc','munkn');
@err = ('mseid','mevcau','meveff','merri','mfoc','mforb','munkn');


@add_on = ('');
$record{event_headers} = \@header; 
$record{RatingInput}   = \@rating_input;
$record{RatingIrrelevantInput} = \@rating_irr_in;
$record{RatingOutput}  = \@rating_output;
$record{RatingIrrelevantOutput} = \@rating_irr_out;
$record{cco1} = \@cco1;
$record{cbax} = \@cbax;
$record{ibax} = \@ibax;
$record{ccp1} = \@ccp1;
$record{cdp1} = \@cdp1;
$record{ok} = \@ok;
$record{err}= \@err;
$record{ProjectAddOn} = \@add_on;


