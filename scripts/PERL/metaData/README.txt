## Przechowywanie informacji o wersji pliku z opisem struktury 
## Służy do porównania informacji o strukurze która jest w pliku oraz tej dla której mamy przygotowane transformacje


metaDataXXXXX.xml     - opis stryktury wyciągnięty z pliku RunXXXX_XXXX01of04/EventType_XXXXX/metaData.xml. Na podstawie tego pliku jest przygotowywany plik z podsumowaniem
metaDataForEvent.pl   - skrypt czytający plik xml przetwarzający go 
summaryEventXXXXX.xml - plik wyjściowy, wygenerowany na podstawie pliku wejściowego  

UWAGA!!! Attension !!!

Co zrobić gdy podczas czytania plików z INCOMING stage pojawią siękomunikaty o niezgodności struktury ???
1. Ustalić na czym polegają różnice, 
2. Zmodyfkować transformacje, joby itp. 
3. Przetestować 
4. Wgrać nowy plik metaData.xml i uruchomić polecenie  perl metaDataForEvent.pl metaData.xml 
5. Przegrać do katalogu DATA plik z katalogu ....../INCOMING/SUSPECTED/STRUCTURE/