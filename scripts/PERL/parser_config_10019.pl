#!/bin/perl

##
## Definition of record for event 10019 
##


our %record;

%record = ();

@header = ('UseID',
           'EventID',
           'AccessKey',
           'AccessKeyType',
           'OwningCustomerID',
           'RootCustomerID',
           'ComposedCustomerID',
           'EventType',
           'OrigEventTime',
           'CreationEventTime',
           'EffectiveEventTime',
           'BillCycleID',
           'BillPeriodID',
           'ErrorCode',
           'RateEventType',
           'ExternalCorrelationID',
           'RatingInput{}',
           'RatingIrrelevantInput{}',
           'RatingOutput{}',
           'RatingIrrelevantOutput{}',
           'UniversalAttribute1',
           'UniversalAttribute2',
           'UniversalAttribute3',
           'UniversalAttribute4',
           'UniversalAttribute5',
           'ProjectAddOn{}');

@rating_input = (
		'GEX.UsedUnits',
		'GEX.RequestedUnits',
		'GEX.RatingAmount',
		'GEX.Currency',
		'GEX.MerchantId',
		'GEX.ProductId',
		'GEX.Purpose',
		'Event.is3G',
		'rating.CC_Request_Type',
		'rating.isRoaming',
		'rating.RoamingZone',
		'rating.RoamingTimeZone',
		'rating.Message_Size',
		'rating.Message_Type',
		'rating.normalizedDestination',
		'rating.normalizedSmsMessageCenter',
		'rating.normalizedVisitedMscNumber',
		'rating.normalizedVLR_Destination',
		'rating.normalizedVLR_Originator',
		'rating.Originator.3GPP_IMSI_MCC_MNC',
		'rating.Originator.Address_Type',
		'rating.Originator.Domain_Name',
		'rating.Recipient.3GPP_IMSI_MCC_MNC_0',
		'rating.Recipient.Recipient.Domain_Name_0',
		'rating.Recipient.Recipient_L',
		'rating.Recipient.RecipientAddress_Type_0',
		'rating.Recipient.typeOfDestination',
		'rating.VAS_Id',
		'rating.VASP_Id',
		'ratingX.normalizedDestinations#',
		'ratingX.typeOfDestinations#',
		'Entry_ss7DialedNumber',
		'SS7_normalizedCalled',
		'SS7_normalizedCaller',
		'Event.Duration',
		'Event.CallSetupTime',
		'Event.Volume',
		'Event.StopTime',
		'Event.ReguidingInfo',
		'Event.isSecondary',
		'wmca.EventOrigin',
		'Event.ReGuidingTargetOfferId',
		'Event.ReGuidingFallbackOfferId',
		'Event.OriginalEventId',
		'Customer.User.IMSI',
		'CallReferenceNumber',
		'Event.ReguidingSourceCustomerInfo',
		'Event.ReGuidingSourceCustomerTargetOfferId',
		'Event.ReGuidingSourceCustomerFallbackOfferId',
		'Event.ErrorInfo.FailedTableName',
		'Event.ErrorInfo.FailedSearchKey',
		'Event.ErrorInfo.FailedSearchKeyContent',
		'EventPartial.Quota.Duration#',
		'EventPartial.Quota.Volume#',
		'EventPartial.Quota.StopTime#',
		'wmca.Event.QuotaDimensionSize',
		'wmca.Event.QuotaDimensionType1',
		'wmca.Event.QuotaPrecision1',
		'wmca.Event.QuotaScalingFactor1',
		'wmca.Event.QuotaDimensionType2',
		'wmca.Event.QuotaPrecision2',
		'wmca.Event.QuotaScalingFactor2',
		'Customer.ROP.s_OfferId',
		'OnTouchHandling.LogInfo',
		'Event.FarmNodeId',
		'Event.ProcessId',
		'Event.ContextId',
		'LogicProcessing.LogInfo',
		'Customer.CDP.DateOfBirth',
		'Customer.CDP.Gender',
		'EventInfo',
		'ExternalClientUserName',
		'ExternalClientIpAddress',
		'ExternalClientIfType',
		'SS7_IMSI',
		'Customer.User.ServiceProviderId'
	);

@rating_irr_in = ();
@rating_output = ('bopk','ResponsibleAccount','ropk','orig','htz','rerid','BillingType','OfferSubscriptionID','cco1{}','cbax[]','ibax[]','ccp1[]','cdp1[]','cpp1[]');
 
@cco1 =  ('GrantResult','ChargeResult','ReasonInfo','CallStopTime','CallDuration','TotalUsedVolume','TotalUsedQuota');
@cbax = ('BalanceName','ChargedValue','NewBalance','ExpiryDate','CreditLimit');

@ibax = ('BalanceName','IncrementValue','NewBalance','ExpiryDate','CreditLimit');

@ccp1 = ('ChargeCode','ChargeBeginTime','ChargedCallDuration','UsedCallDuration','ChargedVolume','UsedVolume','ChargeType',
                  'ChargedPriceUnitType','ChargedPrice','TaxInfo','TaxOfChargedPrice');
@cdp1 = ('DiscountCode','DiscountPriceUnitType','DiscountPrice');
@cpp1 = ('ChargeCode','ChargeType','ChargedPriceUnitType','ChargedPrice','ChargedSplitPrices','TaxInfo','TaxOfChargedPrice');

@rating_irr_out = ('ok{}','err{}');
@ok  = (
	'DonatorAccessKey',
	'CurrentStatus',
	'PreviousStatus',
	'gseid',
	'gscid',
	'gnsnmn',
	'gnsnai',
	'gnsnai1',
	'gnsnai2',
	'gnsnai3',
	'gnsnai4',
	'gnsnai5',
	'gnsnai6',
	'gnsnai7',
	'gnsnai8',
	'gnsnai9',
	'gnsnaf',
	'gexpi',
	'goexpi',
	'gtast',
	'gtaid',
	'cuc',
	'gexse',
	'OldActiveEndDate',
	'NewActiveEndDate',
	'OldDeactiveEndDate',
	'NewDeactiveEndDate',
	'OldGraceEndDate',
	'NewGraceEndDate',
	'OldPostGraceEndDate',
	'NewPostGraceEndDate',
       );
@err = (
	'gseid',
	'gscid',
	'DonatorAccessKey',
	'gnsnmn',
	'gnsnai',
	'gnsnps',
	'gtaid',
	'gevcau',
	'geveff',
	'gerri',
	'gexse'
      );


@add_on = ( 'GEX.RecipientFee',
            'ccsExtension.recipientFee',
            'ccsExtension.PPITransactionID' 
          );
$record{event_headers} = \@header; 
$record{RatingInput}   = \@rating_input;
$record{RatingIrrelevantInput} = \@rating_irr_in;
$record{RatingOutput}  = \@rating_output;
$record{RatingIrrelevantOutput} = \@rating_irr_out;
$record{cco1} = \@cco1;
$record{cbax} = \@cbax;
$record{ibax} = \@ibax;
$record{ccp1} = \@ccp1;
$record{cdp1} = \@cdp1;
$record{cpp1} = \@cpp1;

$record{ok}  = \@ok;
$record{err} = \@err;

$record{ProjectAddOn} = \@add_on;


