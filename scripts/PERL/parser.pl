#!/usr/bin/perl 

#use Data::Printer;
use Data::Dumper;
use IO::Uncompress::Gunzip qw(gunzip $GunzipError);

# subroutines, common for all event type
require "./parser_config.pl";
my $call = 0 ;

# check number of arguments 
if ( @ARGV != 2 ) {
  print "\n***\n Two arguments are required: \n1. path to file \n2. event type \n***\n";
  exit -1;   
}
# check if config file for event is present or not  
if ( grep /^$ARGV[1]$/, @sup_events  ) {
   local $required_file = "parser_config_$ARGV[1].pl";
   if (-e $required_file )  {
      require "$required_file";
   } else {
     print  "\n***\n Event configuration file for event \"$ARGV[1]\"  not found\n***\n";
     exit -2 ;
   }
} else {
  print "\n***\n Event type $Argv[1] not in list of supported events \n***\n";
  exit -3;
}

# config zone ----------------------------
# definition of structure 
our %record;

# prepare file name  
@part1 = (); 
@part1 = split('_',$ARGV[0]);
@part2 = ();
@part2 = split('\.',$part1[4]);
$record_count = $part1[1];
$file_name = $part2[0]; 

# array for each character in line 
my @carray  = ();  
# separator position in line 
my @sep_pos  = 0;

# end config zone ------------------------

## control line number
my $line_number = 0;

## array for all  data 
my @data= ();

## common values for all structures in record (primary and foreign key) 
$useid = '';
$eventid = '';
$event_type = '';

# counters for each file
%control = ();

my $z = new IO::Uncompress::Gunzip $ARGV[0] or die "gunzip failed: $GunzipError\n";


# for each read line 
while ( $line = readline($z)) {
   my $first_char = substr $line, 0, 1 ;
   if ( $first_char eq '(' ){ 
       # substring from ( to ) 
       $line = substr($line,1,length($line)-3);
       $line =~ s/ ,/,/g;
       $line =~ s/, /,/g;
       ## print "Linie do obróbki ".$line."\n";
       @carray = split //, $line;
       $sep_pos = 0;
       $cur_pos = 0; 
       # browse main structure 
       foreach my $head ( @{$record{event_headers}} ){
          # check last char in column name 
          if ($head  =~ /}$/) { 
            # structre can be NULL 
            if ($carray[$sep_pos] eq 'N' && $carray[$sep_pos+1] eq 'U' && $carray[$sep_pos+2] eq 'L' && $carray[$sep_pos+3] eq 'L' ) {
               # odczyt do następnego przecinka po to aby przesunąć wskaźnik   
               local $val_tmp = getRecordPartNEW(',');
            } else {
              local $col_val =  getRecordPartNEW('{');
              # move 
              $sep_pos++;
              ## print $head ."->". $col_val." \n";
	      $h = substr $head, 0, length($head)-2;
	      processStructure($h,$record{$h},$col_val,$useid,$eventid);
            }   
          } else {
             # to  next comma or enclosed by " " 
             ##print $head."\n"; 
             $col_val = getRecordPartNEW(',');
             ##print $head ."->". $col_val."\n";
             ## store first and second value from line 
             if ($head eq 'UseID') {
               $useid = $col_val;
             }
             if  ($head eq 'EventID') {
               $eventid = $col_val;
             }
             if  ($head eq 'EventType') {
               $event_type = $col_val;
             }
             push @{$data[$line_number]{head}},trim($col_val);
          }
      } 
      
      WriteToFile('Event',\@{$data[$line_number]{head}});
      $line_number++;
      if ( $line_number > 10000) { 
          last;
      }
      $useid = '';
      $eventid = '';
   } else {
      # non data line 
   }
}
   ## fill summary file  
   my $filename = $event_data_dir.$event_type."_".$file_name."_INFO.txt";
   open(my $fh, '>', $filename) or die "Could not open file '$filename' $!";
   foreach $key ( keys %control ) {
       print $fh $event_data_dir.$event_type."_".$file_name."_".$key.";".$control{$key}."\n";
   }
   close $fh;


## --------------------------------------------------------------------------------------------
## FUNCTIONS

sub getRecordPartNEW{
 ## print " getRecordPartNEW in $sep_pos\n";
  local $lchar = $_[0];
  local $rchar = '';
  local $str = '';
  local $added = 0;
  local $enclosed = 0;

  if ($lchar eq "(" ) {
       $rchar = ")";
  } elsif ( $lchar eq "[" ) {
       $rchar = "]";
  } elsif ( $lchar eq "{" ) {
       $rchar = "}";
  } else {
        $rchar = ","
  }

  local $found = 0;
  local $enc   = 0;  # counter for enclosed char 
  local $esc   = 0;  # counter for escaped char

  # comma separated fields
  # " enclosed character or \" character 
  if ( $lchar eq ',' ) {
    while ( $found < 1 ) {
       if ($carray[$sep_pos] eq $lchar && $enc == 0 and $esc == 0 ) {
         $found = 1;
       } else {
         if ( $carray[$sep_pos] eq '"' && $carray[$sep_pos-1] eq '\\' )  {
            if ( $esc == 0 ) {
               $esc++;
            } else {
               $esc--;
            }
         }
         if ($carray[$sep_pos] eq '"' ) {
            if ($enc == 0) {
               $enc++;
            } else {
               $enc--;
            }

         }
         $str = $str.$carray[$sep_pos];
       }
       $sep_pos++;
    }
    return $str;
  }

  local $br = 0;
  # looking  for char other than comma 
  while ($found < 1 ) {
    if ($carray[$sep_pos] eq $lchar ) {
        $br++;
        $added = 1;
    }
    if ($carray[$sep_pos] eq $rchar) {
        $br--;
    }
    if ($br == 0 && $added == 1 ) {
       # found if added lchar before 
          $found = 1;
    }
    $str = $str.$carray[$sep_pos];
    $sep_pos++;
  }

  #print " getRecordPartNEW out $sep_pos\n";
  #print $str; 
  return $str;
}


sub getRecordPart2{
  #print " getRecordPart2 in \n";
  #print Dumper( @_); 
  local $lchar = $_[0];
  local $rchar = '';
  local $str = '';
  local $added = 0;
  local $enclosed = 0;
  
  local @carray2 = ();
  local $sep_pos2 = 0;
  local $len = length($_[1]); 
  @carray2 = split  //, $_[1];  
  $sep_pos2 = $_[2];
  ##print Dumper (@carray2);
  
  if ($lchar eq "(" ) {
       $rchar = ")";
  } elsif ( $lchar eq "[" ) {
       $rchar = "]";
  } elsif ( $lchar eq "{" ) {
       $rchar = "}";
  } else {
        $rchar = ","
  }

  local $found = 0;
  local $enc   = 0;  # counter for enclosed char 
  local $esc   = 0;  # counter for escaped char

  # comma separated fields
  # " enclosed character or \" character 
  if ( $lchar eq ',' ) {
    while ( $found < 1 ) {
       if ($carray2[$sep_pos2] eq $lchar && $enc == 0 and $esc == 0 ) {
         $found = 1;
       } else {
         if ( $carray2[$sep_pos2] eq '"' && $carray2[$sep_pos2-1] eq '\\' )  {
            if ( $esc == 0 ) {
               $esc++;
            } else {
               $esc--;
            }
         }
         if ($carray2[$sep_pos2] eq '"' ) {
            if ($enc == 0) {
               $enc++;
            } else {
               $enc--;
            }

         }
         $str = $str.$carray2[$sep_pos2];
       }
       $sep_pos2++;
       if ($sep_pos2 > $len ) {
          $found =1;
       } 
    }
    return $str;
  }

  local $br = 0;
  # looking  for char other than comma 
  while ($found < 1 ) {
    if ($carray2[$sep_pos2] eq $lchar ) {
        $br++;
        $added = 1;
    }
    if ($carray2[$sep_pos2] eq $rchar) {
        $br--;
    }
    if ($br == 0 && $added == 1 ) {
       # found if added lchar before 
          $found = 1;
    }
    $str = $str.$carray2[$sep_pos2];
    $sep_pos2++;
    if ($sep_pos2 > $len) {
       $found = 1;
    } 
  }

  #print " getRecordPart2 out $sep_pos2\n";
  #print $str; 
  return $str;
}




sub WriteToFile{
   our $event_data_dir;
   my @arr = ();
   @arr =  $_[1];
   local $stru_name  = $_[0];
   $stru_name =~ s/\./_/g;
   my $filename = $event_data_dir.$event_type."_".$file_name."_".$stru_name.".csv";
   open(my $fh, '>>', $filename) or die "Could not open file '$filename' $!";
   for $k ( 0 .. $#arr ) {
       print $fh join(';',@{$arr[$k]}) ;
       print $fh "\n";
       $control{$_[0]} = scalar $control{$_[0]} +1;
   }
   close $fh;
2}

sub trim{
# return string without leading and ending
  $str = @_[0];
  $str =~ s/^\s+|\s+$//g;
  return $str;
}

sub clearString{ 
# return string without unnecesary characters like (,),'i "  etc 
   $str = @_[0];
   $str =~ s/{//g;
   $str =~ s/}//g;
   return $str;
}

sub processListOfStructures{
#  
   ##print Dumper ( @_);   
   local @ldata = ();
   local $lstr = $_[2];
   local $size = @{@_[1]}; 

   # jeśli przyszedł tylko NULL lub {NULL} to oznacza że że ten element jest pusty.
   # dla tablicy jednoelemntowej oznacza to że jest wartość null, a dla tablicy wieloelementowej brak wartości. 
   
   $lstr =~ s/\[//g;
   $lstr =~ s/\]//g;
   $lstr =~ s/$_[0]//;
   ## print "rozmiar tablicy $size  \n";
   ## print "wyczyszczona    $lstr  \n";
 
   if ( ($size > 1 ) && (( $lstr eq '{NULL}' ) || ($lstr eq 'NULL') ) )  {
     ## structure is empty 
     return;
   }   
 
   $lstr =~ s/, {/,{/g;
   local @tab = split('},{',$lstr);
   for $n ( 0 .. $#tab ) {
       push @ldata,  $_[3];
       push @ldata,  $_[4];
       @ldata2 = split(',',$tab[$n]);
       for $x ( 0 .. $#ldata2) {
          push @ldata, trim(clearString($ldata2[$x]));
       }
       WriteToFile(@_[0],\@ldata);
       @ldata = ();
       @ldata2 = ();
   }
}

sub processListOfLongs{
# 
   local @ldata = () ;
   local @ldata2 = ();
   @ldata2 = split(',',@_[1]);
   for $n ( 0 .. $#ldata2 ) {
      push @ldata, $_[2];
      push @ldata, $_[3];
      push @ldata, $ldata2[$n];
      WriteToFile(@_[0],\@ldata);
      @ldata = ();
   }
}


sub processStructure {
  ## print "Poczatek----\n";
  ## print Dumper(@_);
  ## print "Koniec  ----\n";

  $call++;          

  # print "\n NUMER WYOŁANIA $call \n";
   

  local $pos = 0;              # current position in string 
  local $val = '';             # substring for current structure or value
  local $pos_eq = 0;           # position of '=' char          
  local @struct_val =();       # values for current structure
  local $counter = 1;          # control current column 
  local $lname = @_[0];        # structure_name and file name 
  local $cname;                # name without {} # = 
  # remove first and last char
  local $struct = substr $_[2], 1, length($_[2])-2;
  push @struct_val, scalar $_[3];
  push @struct_val, scalar $_[4];
  $size = @{@_[1]};
  ## print "\n\n NAZWA STRUKTURY  $_[0] o rozmiarze $size\n\n";
  for my $name( @{@_[1]} ) {
    if ($name  =~ /}$/) {                      # } -> structure 
        ##print " $name ma rozmiar $size \n";
        ##print " Wejscie w przeglądanie strukturyy $name z pos = $pos \n";
        ##print "Licznik $counter, z $size  \n"; 
        if ($counter == $size)  {
            ## print "1. \n";
            $val = substr $struct, $pos, length($struct)-$pos;
         } else {
            ## print "2. \n"; 
            $val  = getRecordPart2(',',$struct,$pos,$name);
        }
        ## print " \t odczytana wartosc  $val\n";
        if ( trim($val) ne 'NULL' )  {
           ## print "1.\n";
           if ($counter == $size)  {              # get rest of string with values
              $val = substr $struct, $pos, length($struct)-$pos;
           } else {
              $val =  getRecordPart2('{',$struct,$pos);
              ##print " \t druga odczytana wartość $val \n";
           }
           ## print "2.\n";
           $cname = substr $name, 0, length($name)-2;
           $pos  = $pos + length($val)+1;
           ## print "pozycja do następnego startu czytania -> $pos\n"; 
           ## wyciecie nazwy struktury w z początku 
           if (substr($val,0,length($cname)) eq $cname  ) {
              $val = substr($val,length($cname));  
           }
           processStructure($cname,$record{$cname},$val,@_[3],@_[4]);
        } else {
           $pos  = $pos + length($val)+1;
        }
    } elsif ( $name =~ /]$/ ) {                # ] -> list of structure 
        # sprawdz czy nie jest NULL 
        ## print " Wejscie w przeglądanie tablicy $name z pos  $pos \n ";
        local $comma_tmp = getRecordPart2(',',$struct,$pos);
        ## print " według przecinka ".$comma_tmp."\n"; 
        local $val_tmp = getRecordPart2('[',$struct,$pos);
        ## print " według nawiasu ".$val_tmp."\n"; 

        if (trim($comma_tmp) eq 'NULL' ) {
           ##print "NULL zamiast tablicy";
           $pos = $pos + length($comma_tmp) + 1;
        } elsif ( index($comma_tmp,'[') > -1 ) {
           ##print "Jest tablica\n";
           # wycinam na wszelki wypadek nazwe sprzed tablicy 
           $cname = substr $name, 0, length($name)-2;
           $name_pos = index($struct,$cname,$pos);
           $val = $val_tmp;
           ##print "LISTA STRUKTUR -> ". $val. "\n";
           # move position on line to the next element 
           $pos = $pos + length($cname) + length($val)+ 1;
           processListOfStructures($cname,$record{$cname},$val,@_[3],@_[4]);
        }
    } elsif ( $name =~ /=$/ ) {                # = -> key = value  
         ## print " Wejcie w przeglądanie klucz = wartosc $name \n";
         if ($counter == $size)  {
            $val = substr $struct, $pos, length($struct)-$pos;
         } else {
            $val =  getRecordPart2(',',$struct,$pos);
         }
         $pos  = $pos + length($val)+1;
         # get pos of '=' char and substr from index to the end of string
         $pos_eq = index($val,'=',0);
         if ($pos_eq == -1 ) {# no char '='
           push @struct_val, trim($val) ;
         }
         else {
            my $tmp_val =  substr $val, $pos_eq+1,length($val)-$pos_eq;
            $tmp_val = trim($tmp_val);
            push @struct_val,$tmp_val;
         }
    } elsif ( $name =~ /#$/ )  {               # * -> list of long eg. [1111,2222,333] 
         ##print " Wejcie w przeglądanie listy wartosci $name\n";
         # first look for text to the next comma 
         if ($counter == $size)  {
            $val = substr $struct, $pos, length($struct)-$pos;
         } else {
            $val =  getRecordPart2(',',$struct,$pos);
         }
         if (index($val,'[') >=0 ){
            if ($counter == $size)  {
               $val = substr $struct, $pos, length($struct)-$pos;
            } else {
              $val =  getRecordPart2('[',$struct,$pos);
            }
            # get list of long values
            $pos = $pos + length($val)+3;
            $cname = substr $name, 0, length($name)-1;
            processListOfLongs($cname,$val,@_[3],@_[4],$pos);

         } else {
            $pos = $pos + length($val)+1;
         }
    } else {                                   # -> comma separated,
         # first look for text to the next comma 
         ##print " Wejcie w przeglądanie pomiedzy przecinkami $name\n";
         if ($counter == $size)  {
            $val = substr $struct, $pos, length($struct)-$pos;
         } else {
            $val =  getRecordPart2(',',$struct,$pos);
         }
         $pos = $pos + length($val)+1;
         ##print "$name  ---> $val \n";
         ## contains = (eg.orig=1 )  but no "( eg.  aa bc = XXX") 
         if (index($val,'"') < 0 && ( index($val,'=') > 0 )) {
            $pos_eq = index($val,'=');
            $val = substr($val,$pos_eq+1);
         }
         push @struct_val,trim($val);
    }
    $counter++;
  } # end loop for  
  WriteToFile($lname,\@struct_val);
  @struct_val = ();
  $call--;
}

