#!/bin/perl
 
our $event_source_dir; 
our $event_proces_dir;
our $event_data_dir;
our $event_structure_suspected;
our $event_structure_checksums;

our @sup_events;

# path to the files  
$event_source_dir = '/data/RTCS/INCOMING';

# where to write gzip files 
$event_proces_dir = '/data/RTCS/PARSING/';

# where to write flat cvs files    
$event_data_dir   = '/data/RTCS/PROCESSING/';

# where are checksums for dtructure 
$event_structure_checksums = '/data/scripts/PERL/metaData/';

# where to write files witch mismatched structure 
$event_structure_suspected = '/data/RTCS/INCOMING/SUSPECTED/STRUCTURE/';

# List of supported events. File with events outside this list goin to ... /dev/null
@sup_events = (10001,10002,10003,10008,40000); 
 
1;
