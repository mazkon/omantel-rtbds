use XML::Simple;    # parse xml
use Data::Dumper;   # dump data to screen 
use File::Copy;     # copy file to destination directory


require "/data/scripts/PERL/parser_config.pl";


#begin config zone  ------------------------------------------------

# where to write files
our $event_proces_dir;
# where to write suspected files 
our $event_structure_suspected;
# List of supported events. File with events outside this list goin to ... /dev/null
our @sup_events; 

#end config zone ------------------------------------------

# name of untar directory  
$dirname = $ARGV[0];

# read the Manifest file with summary of tar file 
my $xml = new XML::Simple;
my $man = XMLin($dirname."/cdsManifest.xml");
#print Dumper($man);

# get info about date and hour of export 
@run = split(/_/,$man->{run}[0]);
$run_date = @run[0];
$run_id = $man->{run}[1]{ID}; 

foreach my $info (keys %{$man->{ContentFile}}) {  
   #get counter for file, and remove thousand separator
   $record_count =  $man->{ContentFile}->{$info}->{total};
   $record_count =~ s/,//;
 
   # create new filename with additional_information 
   @filename = split(/\//,$info);
   $event_type = substr $info, 10, 5;   
    
  
   if ( grep( /^$event_type$/, @sup_events ) ) {        # found event on supported list 
     # make destination directory 
     system("mkdir -p $event_proces_dir/$run_date/$event_type/");
     
     # prepare destination file name 
     $dest =  $event_proces_dir."/$run_date/$event_type/".$run_id.'_'.$record_count."_@filename[1]"; 
     
     # if checksum not agreeded then copy file to suspected directory with info file filled with message 
     if ( checkStructure($dirname.'/'.$info,$event_type) < 0 ) {
        # copy file to suspected directory and save a message about it  
        copy($dirname."/".$info,$event_structure_suspected);
        open ($fh,'>>',$event_structure_suspected.'/log.txt');
        print $fh "Mismatch structure of $dirname/$info with stored summary of structure \n ";
        close $fh ;  
        print "Poszlo do logu";
     } else { 
        # copy file to destination directory 
        copy($dirname."/".$info,$dest);
     }
   } else {
     print "event $event_type is going to trash ...\n";
   } 
}

sub checkStructure{
## compare structure information
   local $source = $dirname."/EventType_".$_[1]."/metaData.xml" ;
   local $summary = $event_structure_checksums."summaryEvent$_[1].xml";
   $xml_source  = XMLin($source);
   $xml_summary = XMLin($summary);
   if ( $xml_source->{checksum} ne $xml_summary->{checksum} ) { 
      return -1 ;
   } 
   if ( $xml_source->{ratingInputChecksum} ne $xml_summary->{ratingInputChecksum}  ) {
      return -1;
   }
   
   if ( $xml_source->{projectAddOnChecksum} ne $xml_summary->{projectAddOnChecksum} ) {
      return -1;
   }
   return 1;
}

exit 0;
