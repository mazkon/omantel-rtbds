#!/bin/perl

##
## Definition of record for event 11007
##


our %record;

%record = ();

@header = ('UseID',
           'EventID',
           'AccessKey',
           'AccessKeyType',
           'OwningCustomerID',
           'RootCustomerID',
           'ComposedCustomerID',
           'EventType',
           'OrigEventTime',
           'CreationEventTime',
           'EffectiveEventTime',
           'BillCycleID',
           'BillPeriodID',
           'ErrorCode',
           'RateEventType',
           'ExternalCorrelationID',
           'RatingInput{}',
           'RatingIrrelevantInput{}',
           'RatingOutput{}',
           'RatingIrrelevantOutput{}',
           'UniversalAttribute1',
           'UniversalAttribute2',
           'UniversalAttribute3',
           'UniversalAttribute4',
           'UniversalAttribute5',
           'ProjectAddOn{}');

@rating_input = (
		'CA.BalanceAdjustment.BalanceId',
		'CA.BalanceAdjustment.AmountOfUnits',
		'CA.BalanceAdjustment.ChargeCode',
		'CA.BalanceAdjustment.EventInfo',
		'CA.BalanceAdjustment.TransactionId',
		'Customer.ROP.s_OfferId',
		'OnTouchHandling.LogInfo',
		'Event.FarmNodeId',
		'Event.ProcessId',
		'Event.ContextId',
		'LogicProcessing.LogInfo',
		'Customer.CDP.DateOfBirth',
		'Customer.CDP.Gender',
		'EventInfo',
		'ExternalClientUserName',
		'ExternalClientIpAddress',
		'ExternalClientIfType',
		'Customer.User.ServiceProviderId',
		'OperationalHistory.DuplicateToMessageId'
                 );
@rating_irr_in = ();
@rating_output = ('bopk','ResponsibleAccount','ropk','orig','htz','rerid','BillingType','OfferSubscriptionID','cbax[]','cpp1[]');
 
@cbax = ('BalanceName','ChargedValue','NewBalance','ExpiryDate','CreditLimit');
@cpp1 = ('ChargeCode','ChargeType','ChargedPriceUnitType','ChargedPrice','ChargedSplitPrices','TaxInfo','TaxOfChargedPrice');

@rating_irr_out = ('ct=','ec=','ee=','ei=','UnitsBelowLimit','UnitsDeducted');

@add_on = ('');

$record{event_headers} = \@header; 
$record{RatingInput}   = \@rating_input;
$record{RatingIrrelevantInput} = \@rating_irr_in;
$record{RatingOutput}  = \@rating_output;
$record{RatingIrrelevantOutput} = \@rating_irr_out;
$record{cbax} = \@cbax;
$record{cpp1} = \@cpp1;

$record{ProjectAddOn} = \@add_on;


