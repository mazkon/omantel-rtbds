#!/bin/perl
## use Data::Printer;
##
## Definition of record for event 40001 
##


our %record;

%record = ();

@header = ('UseID',
           'EventID',
           'AccessKey',
           'AccessKeyType',
           'OwningCustomerID',
           'RootCustomerID',
           'ComposedCustomerID',
           'EventType',
           'OrigEventTime',
           'CreationEventTime',
           'EffectiveEventTime',
           'BillCycleID',
           'BillPeriodID',
           'ErrorCode',
           'RateEventType',
           'ExternalCorrelationID',
           'RatingInput{}',
           'RatingIrrelevantInput{}',
           'RatingOutput{}',
           'RatingIrrelevantOutput{}',
           'UniversalAttribute1',
           'UniversalAttribute2',
           'UniversalAttribute3',
           'UniversalAttribute4',
           'UniversalAttribute5',
           'ProjectAddOn{}');

@rating_input = (
                'CA.Modify.PackageItem.ChargeMode',
                'CA.Modify.PackageItem.BalanceAdjustMode',
		'CA.Modify.PackageItem.BalanceAdjustValue',
		'CA.Modify.PackageItem.BalanceAdjustChargeCode',
		'CA.Modify.PackageItem.FocSubscribeAvailable',
		'CA.Modify.PackageItem.FocModifyAvailable',
		'CA.Modify.PackageItem.EndOfValidity',
		'CA.Modify.PackageItem.UserNotification',
		'CA.Modify.PackageItem.ActivationMode',
		'CA.Modify.PackageItem.CUGList.append',
		'CA.Modify.PackageItem.CUGList.remove',
		'CA.Modify.PackageItem.CUGList.delete',
		'CA.Modify.PackageItem.FreeOfChargeAvailable',
		'CA.Modify.PackageItem.FavoriteAreaList.append',
		'CA.Modify.PackageItem.FavoriteAreaList.remove',
		'CA.Modify.PackageItem.FavoriteAreaList.delete',
		'CA.Modify.PackageItem.AccountExpiryDate',
		'CA.Modify.PackageItem.AccountExpiryDatePolicy',
		'CA.Modify.PackageItem.SecondsOfExtension',
		'CA.Modify.PackageItem.FnFList.append',
		'CA.Modify.PackageItem.FnFList.remove',
		'CA.Modify.PackageItem.FnFList.delete',
		'CA.Modify.PackageItem.FnFList.usePID',
		'CA.Modify.PackageItem.NumberListMode',
		'CA.Modify.PackageItem.ForbiddenList.append',
		'CA.Modify.PackageItem.ForbiddenList.remove',
		'CA.Modify.PackageItem.ForbiddenList.delete',
		'CA.Modify.PackageItem.ForbiddenList.usePID',
		'CA.Modify.PackageItem.HomeZoneList.append',
		'CA.Modify.PackageItem.HomeZoneList.remove',
		'CA.Modify.PackageItem.HomeZoneList.delete',
		'CA.Modify.PackageItem.CreditLimit',
		'CA.Modify.PackageItem.LifeTimeStart',
		'CA.Read.PackageItem.ChargeMode',
		'CA.Read.PackageItem.Prices',
		'CA.Subscribe.PackageItem.ChargeMode',
		'CA.Subscribe.PackageItem.UserNotification',
		'CA.Subscribe.PackageItem.Recharge',
		'CA.Subscribe.PackageItem.ChargeCode',
		'CA.Subscribe.PackageItem.ActivationTime',
		'CA.Subscribe.PackageItem.UnsubscribeTime',
		'CA.Subscribe.PackageItem.LifeTimeStart',
		'TMAF.Request.Package',
		'TMAF.Request.CouponGroup',
		'TMAF.Request.ROPKey',
		'TMAF.Request.PackageType',
		'TMAF.Request.SelfCare',
		'AdminUseCase',
		'AdminKind',
		'RechargingParameters.StornoIdofDelayedReward',
		'Entry_ss7DialedNumber',
		'SS7_normalizedCalled',
		'SS7_normalizedCaller',
		'Event.Duration',
		'Event.CallSetupTime',
		'Event.Volume',
		'Event.StopTime',
		'Event.ReguidingInfo',
		'Event.isSecondary',
		'wmca.EventOrigin',
		'Event.ReGuidingTargetOfferId',
		'Event.ReGuidingFallbackOfferId',
		'Event.OriginalEventId',
		'Customer.User.IMSI',
		'CallReferenceNumber',
		'Customer.ROP.s_OfferId',
		'OnTouchHandling.LogInfo',
		'Event.FarmNodeId',
		'Event.ProcessId',
		'Event.ContextId',
		'LogicProcessing.LogInfo',
		'Customer.CDP.DateOfBirth',
		'Customer.CDP.Gender',
		'EventInfo',
		'ExternalClientUserName',
		'ExternalClientIpAddress',
		'ExternalClientIfType',
		'Customer.User.ServiceProviderId',
		'CA.Create.Coupon.Name',
		'CA.Create.Coupon.NameUnique',
		'CA.Create.Coupon.ActivationStartTime',
		'CA.Create.Coupon.ActivationEndTime',
		'CA.Create.Coupon.Type',
		'CA.Create.Coupon.Priority',
		'CA.Create.Coupon.FilterRuleId',
		'CA.Create.Coupon.Merchant',
		'CA.Create.Coupon.Value',
		'CA.Create.Coupon.Applicability',
		'CA.Create.Coupon.TransparentData',
		'CA.Create.Coupon.BalanceAmount',
		'CA.Modify.Coupon.Key',
		'CA.Modify.Coupon.Name',
		'CA.Modify.Coupon.Applicability',
		'CA.Modify.Coupon.TransparentData',
		'CA.Modify.Coupon.BalanceAdjustMode',
		'CA.Modify.Coupon.BalanceAdjustValue',
		'CA.Delete.Coupon.Key',
		'CA.Delete.Coupon.Name');

@rating_irr_in = ('CAINPUT=');
@rating_output = ('bopk','ResponsibleAccount','ropk','orig','htz','rerid','BillingType','OfferSubscriptionID','cbax[]','addi1{}','paba[]','ibax[]','focc[]','pcp1[]','cpp1[]','Coupon{}');
 
@cbax   = ('BalanceName','ChargedValue','NewBalance','ExpiryDate','CreditLimit');
@addi1  = ('foc[]','res');
@foc    = ('ind','use','max'); 
@paba   = ('BalanceName','OldAmount','NewAmount','ChargedAmount','RechargedAmount','NewCarryOver','PreviousStartAmount');
@ibax   = ('BalanceName','IncrementValue','NewBalance','ExpiryDate','CreditLimit'); 
@focc   = ('CounterName','CounterChangeValue','ActualCounterValue');
@pcp1   = ('PerFactor','PerActivity','PerState');
@cpp1   = ('ChargeCode','ChargeType','ChargedPriceUnitType','ChargedPrice','TaxInfo','TaxOfChargedPrice');
@Coupon = ('Operation','Key','Name','CouponAttributes{}');

@record{CouponAttributes} = ('Applicability{}','BalanceAmount{}','TransparentData{}');
@record{Applicability}    = ('OldValue','NewValue');
@record{BalanceAmount}    = ('OldValue','NewValue');
@record{TransparentData}  = ('OldValue','NewValue');

@rating_irr_out = ('pac1{}','pop1{}','serr{}','sadc{}','dmod{}','rewd{}','pcrf[]','bndl{}');
@pac1 = ('SubscriptionID','SubscriptionState','SubscriptionCurrPeriod','SubscriptionNextPeriod');
@pop1 = ('EventCause','EventEffect');
@serr = ('Reason');
@sadc = ('ExtBalErrr'); 
@dmod = ('ModData','OldValue','NewValue');
@rewd = ('sub[]','act[]','exp[]','slc[]');
@sub  = ('ReSubAdd','RewSubDel');
@act  = ('ReSubAct','RewSubDeact');
@exp  = ('BalanaceName','RewExpOld','RewExpNew');
@slc  = ('OldSLCDate','NewSLCDate');
@pcrf = ('pcpKey','pcpState','pcpStateInvalidFrom','pcpNextState');
@bndl = ('BundleItems');


@record{pac1}  = \@pac1; 
@record{pop1}  = \@pop1;
@record{serr}  = \@serr; 
@record{sadc}  = \@sadc; 
@record{dmod}  = \@dmod; 
@record{rewd}  = \@rewd; 
@record{sub}   = \@sub;
@record{act}   = \@act;
@record{exp}   = \@exp;
@record{slc}   = \@slc;
@record{pcrf}  = \@pcrf;
@record{bndl}  = \@bndl;

@add_on = ('RPP_Resubscription.isRepurchased','RPP_Remove.pendingReservationAmount');

$record{event_headers} = \@header; 
$record{RatingInput}   = \@rating_input;
$record{RatingIrrelevantInput} = \@rating_irr_in;
$record{RatingOutput}  = \@rating_output;
$record{RatingIrrelevantOutput} = \@rating_irr_out;
$record{addi1} = \@addi1;
$record{cbax} = \@cbax;
$record{ibax} = \@ibax;
$record{ccp1} = \@ccp1;
$record{cdp1} = \@cdp1;
$record{Coupon} = \@Coupon;
$record{ProjectAddOn} = \@add_on;

