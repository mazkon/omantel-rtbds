#!/bin/perl

##
## Definition of record for event 1000888888881 
##


our %record;

%record = ();

@header = ('UseID',
           'EventID',
           'AccessKey',
           'AccessKeyType',
           'OwningCustomerID',
           'RootCustomerID',
           'ComposedCustomerID',
           'EventType',
           'OrigEventTime',
           'CreationEventTime',
           'EffectiveEventTime',
           'BillCycleID',
           'BillPeriodID',
           'ErrorCode',
           'RateEventType',
           'ExternalCorrelationID',
           'RatingInput{}',
           'RatingIrrelevantInput{}',
           'RatingOutput{}',
           'RatingIrrelevantOutput{}',
           'UniversalAttribute1',
           'UniversalAttribute2',
           'UniversalAttribute3',
           'UniversalAttribute4',
           'UniversalAttribute5',
           'ProjectAddOn{}');

@rating_input = (
		'CommonParameter.APN',
		'CommonParameter.MCC',
		'CommonParameter.MNC',
		'CommonParameter.RoamingInformationIncomingAParty',
		'CommonParameter.RoamingInformationEvaluatedAParty',
		'Event.is3G',
		'rating.originatorLocationInfo',
		'Event.LocationInfoType',
		'DMI.REQUEST.SERVICE_INFORMATION.PS_INFORMATION.3GPP_RAT_TYPE',
		'CommonParameter.QoSProfileID',
		'rating.RadioAccessTechnology',
		'Event.Order',
		'Event.UserSessionDuration',
		'rating.IMEI',
		'rating.IMEISV',
		'rating.isRoaming',
		'rating.RoamingZone',
		'rating.RoamingTimeZone',
		'CommonParameter.CustomerReguidingOrigKey',
		'Entry_ss7DialedNumber',
		'SS7_normalizedCalled',
		'SS7_normalizedCaller',
		'Event.Duration',
		'Event.CallSetupTime',
		'Event.Volume',
		'Event.StopTime',
		'Event.ReguidingInfo',
		'Event.isSecondary',
		'wmca.EventOrigin',
		'Event.ReGuidingTargetOfferId',
		'Event.ReGuidingFallbackOfferId',
		'Event.OriginalEventId',
		'Customer.User.IMSI',
		'CallReferenceNumber',
		'Event.ReguidingSourceCustomerInfo',
		'Event.ReGuidingSourceCustomerTargetOfferId',
		'Event.ReGuidingSourceCustomerFallbackOfferId',
		'Event.ErrorInfo.FailedTableName',
		'Event.ErrorInfo.FailedSearchKey',
		'Event.ErrorInfo.FailedSearchKeyContent',
		'FBC.SvcTypeVolumeAndTime',
		'FBC.SvcTypeVolume',
		'FBC.SvcTypeTime',
		'FBC.SvcTypeServiceSpecific',
		'FBC.SvcQuotaType',
		'FBC.RoamingZone',
		'FBC.isGrantFirstOk',
		'FBC.FoC',
		'FBC.BasicFeeWasCharged',
		'FBC.BasicFeeEnabled',
		'FBC.ServiceId',
		'FBC.timeBlockSize',
		'FBC.CategoryId',
		'DmiTickets.UsedVolumeQuota',
		'DmiTickets.UsedVolumeQuotaBeforeTS',
		'DmiTickets.UsedVolumeQuotaAfterTS',
		'DmiTickets.UsedTimeQuota',
		'DmiTickets.UsedTimeQuotaBeforeTS',
		'DmiTickets.UsedTimeQuotaAfterTS',
		'DmiTickets.QoSReleaseIndication',
		'DmiTickets.QoSNegotiatedData',
		'DmiTickets.QoSMaxRequestedBandwidthUL',
		'DmiTickets.QoSMaxRequestedBandwidthDL',
		'DmiTickets.QoSGuaranteedBitrateUL',
		'DmiTickets.QoSGuaranteedBitrateDL',
		'DmiTickets.QoSApnAggregateMaxBitrateUL',
		'DmiTickets.QoSApnAggregateMaxBitrateDL',
		'DmiTickets.QoSClassIdentifier',
		'DmiTickets.QoSBearerIdentifier',
		'DmiTickets.QoSAllocationRetentionPriorityLevel',
		'DmiTickets.QoSAllocationRetentionPriorityPreemptionCapability',
		'DmiTickets.QoSAllocationRetentionPriorityPreemptionVulnerability',
		'DmiTickets.TicketSequence',
		'EventPartial.Quota.Duration#',
		'EventPartial.Quota.Volume#',
		'EventPartial.Quota.StopTime#',
		'wmca.Event.QuotaDimensionSize',
		'wmca.Event.QuotaDimensionType1',
		'wmca.Event.QuotaPrecision1',
		'wmca.Event.QuotaScalingFactor1',
		'wmca.Event.QuotaDimensionType2',
		'wmca.Event.QuotaPrecision2',
		'wmca.Event.QuotaScalingFactor2',
		'wmca.Event.NumberOfSegments',
		'wmca.Event.UsedVolumeQuotaBeforeTS',
		'wmca.Event.UsedVolumeQuotaAfterTS',
		'wmca.Event.UsedTimeQuotaBeforeTS',
		'wmca.Event.UsedTimeQuotaAfterTS',
		'Customer.ROP.s_OfferId',
		'OnTouchHandling.LogInfo',
		'Event.FarmNodeId',
		'Event.ProcessId',
		'Event.ContextId',
		'LogicProcessing.LogInfo',
		'Customer.CDP.DateOfBirth',
		'Customer.CDP.Gender',
		'SS7_IMSI',
		'Customer.User.ServiceProviderId'
                );
@rating_irr_in = ();
@rating_output = ('bopk','ResponsibleAccount','ropk','orig','htz','rerid','BillingType','OfferSubscriptionID','cco1{}','cbax[]','ibax[]','ccp1[]','cdp1[]','sadc{}');
 
@cco1 = ('GrantResult','ChargeResult','ReasonInfo','CallStopTime','CallDuration','TotalUsedVolume','TotalUsedQuota');
@cbax = ('BalanceName','ChargedValue','NewBalance','ExpiryDate','CreditLimit');

@ibax = ('BalanceName','IncrementValue','NewBalance','ExpiryDate','CreditLimit');

@ccp1 = ('ChargeCode','ChargeBeginTime','ChargedCallDuration','UsedCallDuration','ChargedVolume','UsedVolume','ChargeType',
                  'ChargedPriceUnitType','ChargedPrice','TaxInfo','TaxOfChargedPrice');
@cdp1 = ('DiscountCode','DiscountPriceUnitType','DiscountPrice');
@sadc = ('ExtBalError','ExtBalComError','ExtBalResError');

@rating_irr_out = ('ok{}','err{}','pcrf[]');
@ok   = ('fus=','ful=','fui=','fuc=','fuq=','fut=','fud=','fss=','fst=','fsi=','fso=','fsa=','fsr=','cuc=');
@err  = ('fci','fec','fee','fei','foc');
@pcrf = ('pcpKeyi','packageId','pcpState','pcpStateInvalidFrom','pcpNextState');

@add_on =( 
	'DmtrUSTicket.SUB:0.Final',
	'DmtrUSTicket.SUB:0.Sum',
	'Infos4DiaEvent.3GPP_GGSN_Address',
	'Infos4DiaEvent.3GPP_Charging_ID',
	'DMTR_SUBREQUEST.USED_SERVICE_UNIT_0.CC_TOTAL_OCTETS',
	'DMTR_SUBREQUEST.USED_SERVICE_UNIT_1.CC_TOTAL_OCTETS');
$record{event_headers} = \@header; 
$record{RatingInput}   = \@rating_input;
$record{RatingIrrelevantInput} = \@rating_irr_in;
$record{RatingOutput}  = \@rating_output;
$record{RatingIrrelevantOutput} = \@rating_irr_out;
$record{cco1} = \@cco1;
$record{cbax} = \@cbax;
$record{ibax} = \@ibax;
$record{ccp1} = \@ccp1;
$record{cdp1} = \@cdp1;
$record{sadc} = \@sadc;
$record{ok}   = \@ok;
$record{err}   = \@err;
$record{ProjectAddOn} = \@add_on;





