#!/bin/perl

##
## Definition of record for event 10003
##


our %record;

%record = ();

@header = ('UseID',
           'EventID',
           'AccessKey',
           'AccessKeyType',
           'OwningCustomerID',
           'RootCustomerID',
           'ComposedCustomerID',
           'EventType',
           'OrigEventTime',
           'CreationEventTime',
           'EffectiveEventTime',
           'BillCycleID',
           'BillPeriodID',
           'ErrorCode',
           'RateEventType',
           'ExternalCorrelationID',
           'RatingInput{}',
           'RatingIrrelevantInput{}',
           'RatingOutput{}',
           'RatingIrrelevantOutput{}',
           'UniversalAttribute1',
           'UniversalAttribute2',
           'UniversalAttribute3',
           'UniversalAttribute4',
           'UniversalAttribute5',
           'ProjectAddOn{}');

@rating_input = (
		'SS7_isRoaming',
		'SS7_normalizedCalledWithoutMnpPrefix',
		'SS7_normalizedCallerWithoutMnpPrefix',
		'SS7_normalizedVlrNumber',
		'SS7_normalizedSmsMessageCenter',
		'SS7_expandedCallKind',
		'SS7_normalizedVisitedMscNumber',
		'SS7_IMSI',
		'SS7_AccessFlag',
		'SS7_msisdnOfSubscriber',
		'SS7_LoginType',
		'Event.NormalizedCalled',
		'Event.NormalizedCaller',
		'rating.originatorLocationInfo',
		'Event.LocationInfoType',
		'rating.originatorCellId',
		'rating.bearerService',
		'rating.isRoaming',
		'rating.RoamingZone',
		'rating.RoamingTimeZone',
		'Event.is3G',
		'rating.normalizedSmsMessageCenter',
		'rating.normalizedDestination',
		'rating.normalizedDestinationWithoutMnpPrefix',
		'rating.normalizedOriginator',
		'rating.normalizedVLR_Originator',
		'Network.Cause',
		'Network.EventType',
		'Entry_ss7DialedNumber',
		'SS7_normalizedCalled',
		'SS7_normalizedCaller',
		'Event.Duration',
		'Event.CallSetupTime',
		'Event.Volume',
		'Event.StopTime',
		'Event.ReguidingInfo',
		'Event.isSecondary',
		'wmca.EventOrigin',
		'Event.ReGuidingTargetOfferId',
		'Event.ReGuidingFallbackOfferId',
		'Event.OriginalEventId',
		'Customer.User.IMSI',
		'CallReferenceNumber',
		'Event.ReguidingSourceCustomerInfo',
		'Event.ReGuidingSourceCustomerTargetOfferId',
		'Event.ReGuidingSourceCustomerFallbackOfferId',
		'Event.ErrorInfo.FailedTableName',
		'Event.ErrorInfo.FailedSearchKey',
		'Event.ErrorInfo.FailedSearchKeyContent',
		'EventPartial.Quota.Duration#',
		'EventPartial.Quota.Volume#',
		'EventPartial.Quota.StopTime#',
		'wmca.Event.QuotaDimensionSize',
		'wmca.Event.QuotaDimensionType1',
		'wmca.Event.QuotaPrecision1',
		'wmca.Event.QuotaScalingFactor1',
		'wmca.Event.QuotaDimensionType2',
		'wmca.Event.QuotaPrecision2',
		'wmca.Event.QuotaScalingFactor2',
		'Customer.ROP.s_OfferId',
		'OnTouchHandling.LogInfo',
		'Event.FarmNodeId',
		'Event.ProcessId',
		'Event.ContextId',
		'LogicProcessing.LogInfo',
		'Customer.CDP.DateOfBirth',
		'Customer.CDP.Gender',
		'Customer.User.ServiceProviderId'
                );

@rating_irr_in = ();
@rating_output = ('bopk','ResponsibleAccount','ropk','orig','htz','rerid','BillingType','OfferSubscriptionID','cco1{}','cbax[]','ibax[]','ccp1[]','cdp1[]');
@rating_irr_out = ('cif=','fcf=','con','cuf','ctr','ocgpa=','eni=','imiei=','cuc=');
 
@cco1 =  ('GrantResult','ChargeResult','ReasonInfo','CallStopTime','CallDuration','TotalUsedVolume','TotalUsedQuota');
@cbax = ('BalanceName','ChargedValue','NewBalance','ExpiryDate','CreditLimit');

@ibax = ('BalanceName','IncrementValue','NewBalance','ExpiryDate','CreditLimit');

@ccp1 = ('ChargeCode','ChargeBeginTime','ChargedCallDuration','UsedCallDuration','ChargedVolume','UsedVolume','ChargeType',
                  'ChargedPriceUnitType','ChargedPrice','TaxInfo','TaxOfChargedPrice');
@cdp1 = ('DiscountCode','DiscountPriceUnitType','DiscountPrice');

@add_on = ();
$record{event_headers} = \@header; 
$record{RatingInput}   = \@rating_input;
$record{RatingIrrelevantInput} = \@rating_irr_in;
$record{RatingOutput}  = \@rating_output;
$record{RatingIrrelevantOutput} = \@rating_irr_out;
$record{cco1} = \@cco1;
$record{cbax} = \@cbax;
$record{ibax} = \@ibax;
$record{ccp1} = \@ccp1;
$record{cdp1} = \@cdp1;
$record{ProjectAddOn} = \@add_on;


