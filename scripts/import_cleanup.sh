#!/bin/sh
# Project: RTBDS
# Description:
# Compress and move processed source data files from process area to archive area
#
# PARAMETERS
#   type - project specific string e.g. RTCS or NPS or MNP
#   file_mask - mask for processed files e.g. NPS_DATA_*.dat
#
# USAGE
#   import_cleanup.sh NPS NPS_DATA_*.DAT
# 
# RETURN CODES
#      POSITIVE
# 64 - Source folder is empty
#      NEGATIVE
# 74 - source folder not found
# 75 - desctination folder can't be created
# 76 - can't move files to destination
# 77 - can't tar imported files
# 78 - can't move files to archive
#

SOURCE="/data/$1/PROCESSING"
DESTINATION="/data/$1/ARCHIVE"
FILES="$2"
PREFIX="$1"_

# check source directory
if ! [ -d "$SOURCE" ] ; then
    exit 74
fi

# check destination directory
if ! [ -d "$DESTINATION" ] ; then

  mkdir "$DESTINATION"

  # break if can't create destination directory
  if ! [ $? -eq 0 ] ; then
    exit 75
  fi

fi

# check source dir content
if ! [ "$(ls -A $SOURCE/${FILES})" ]; then
    # Source dir is empty, positive break process
    exit 64
fi

cd "$SOURCE"

# compress imported files and move them to archive
tar -cf - ${FILES} | bzip2 > ${PREFIX}data_import_$(date +%Y%m%d_%H%M%S).tar.bz2 

if ! [ $? -eq 0 ] ; then
    # can't compress files
    exit 77
else
    mv ${SOURCE}/*.tar.bz2 ${DESTINATION}
    rm ${FILES} 
    if ! [ $? -eq 0 ] ; then
       exit 78
    fi
fi
