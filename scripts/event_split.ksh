#!/bin/ksh

# Project: RTBDS
# Description:  untar all archives and  run perl script on it

# RETURN CODES
#      POSITIVE
# 64 - Source folder is empty
# 65 - Other process is running  
#
#      NEGATIVE
# 74 - ###EXAMPLE### source folder not found
# 75 - ###EXAMPLE### destination folder cant be created
# 76 - ###EXAMPLE### cant move files to destination

SOURCE=/data/RTCS/INCOMING/
EVENT_SUSPECTED=/data/RTCS/INCOMING/SUSPECTED/
TAR_DESTINATION=/data/RTCS/INCOMING/TMP/
SCRIPT=/data/scripts/
ARCHIVE=/data/RTCS/INCOMING/ARCHIVE/
LOG=/data/RTCS/INCOMING/log.txt
LOCK=/data/scripts/LOCK/event_split.lck

function print_log {
   echo  " $(date +%Y-%m-%d_%H:%M:%S) $1 ">> $LOG
}

if [[ -e $LOCK ]] 
then
   print_log  "Another process is running "
   exit 65
else 
   touch $LOCK
fi

#  check source directory by list files only excluded txt file ( log.txt) 
if [[ -z $(ls -al $SOURCE | grep ^- | grep -v .txt ) ]] 
then
   print_log "Nothing to do : tar file not found" 
   rm -f $LOCK
   exit 64
else
  # for each tar file 
  for FILE in $SOURCE/Run*.tar
  do 
    # extract some information from file name 
    DATA=`echo $FILE | cut -c5-12` 
    HOUR=`echo $FILE | cut -c14-19`
    read -rd "." FILE_NAME <<< "$FILE"
    FILE_BNAME=`basename ${FILE_NAME[0]}`
    tar -xf $FILE -C $TAR_DESTINATION/
    message=$(perl $SCRIPT/PERL/readManifest.pl $TAR_DESTINATION/$FILE_BNAME 2>&1)
    STAT=$?
    if [ $STAT == 0 ] 
    then 
       rm -rf $TAR_DESTINATION/$FILE_BNAME
       mv $FILE $ARCHIVE
       print_log "${FILE_NAME[0]} proceed corectly"
    else 
       print_log " Error in procesing file $FILE with message $message"
       rm -rf $TAR_DESTINATION/$FILE_BNAME
       mv $FILE $EVENT_SUSPECTED
    fi 
  done 
fi 

rm -f $LOCK
