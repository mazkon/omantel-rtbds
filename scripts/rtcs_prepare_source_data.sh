#!/bin/sh
#
# Project: RTBDS
# 
# Date: 2016-05-04
# 
# Description:
# Move event source data files from process area to temporary working directory
#
# PARAMETERS
#   event_type - event number e.g. 10001
#   working_dir - working directory
#
# USAGE
#   rtcs_prepare_source_data.sh 10001 tmp53364
#
# RETURN CODES
#      POSITIVE
# 64 - Source folder is empty
#
#      NEGATIVE
# 74 - source folder not found
# 75 - destination folder cant be created
# 76 - cant move files to destination
#

SOURCE="/data/RTCS/PROCESSING"
DESTINATION="/data/RTCS/PROCESSING/$2"
FILES="$1_*.csv"

# check source directory
if ! [ -d "$SOURCE" ] ; then
    exit 74
fi

# check destination directory
if ! [ -d "$DESTINATION" ] ; then

  mkdir "$DESTINATION"

  # break if can't create destination directory
  if ! [ $? -eq 0 ] ; then
    exit 75
  fi

fi

# check source dir content
if ! [ "$(ls -A $SOURCE/${FILES})" ]; then
     # Source dir is empty, positive break process
    exit 64
fi

mv ${SOURCE}/${FILES} ${DESTINATION}

# check files was moved
if ! [ $? -eq 0 ] ; then
    exit 76
fi
