## Input data from external systems

- /data  
- /data/input
- /data/input/RTCS
- /data/input/MNP
- /data/input/NPS
- /data/input/POS

Project folders shoud have mask `775` and group should be set to `dstage`.

## Processing and archiving

### RTCS

- /data/RTCS
- /data/RTCS/PARSING
- /data/RTCS/PROCESSING
- /data/RTCS/ARCHIVE

### MNP

- /data/MNP
- /data/MNP/PROCESSING
- /data/MNP/ARCHIVE

### POS

- /data/POS
- /data/POS/PROCESSING
- /data/POS/ARCHIVE

## Scripts

- /data/scripts

## Nazewnictwo jobów i sekwencji

### RTCS

Joby importujące poszczególne eventy

- J_RTCS_IMPORT_EVENT_10001
- J_RTCS_IMPORT_EVENT_10002
- J_RTCS_IMPORT_EVENT_10003
- J_RTCS_IMPORT_EVENT_xxxxx

Sekwencja spinająca i uruchamiająca joby

- S_RTCS_IMPORT

### MNP

Joby importujące poszczególne pliki, gdzie XXXXX, YYYYY to nazwy dla konkretnych źródeł lub plików

- J_MNP_IMPORT_XXXXX
- J_MNP_IMPORT_YYYYY

Sekwencja spinająca i uruchamiająca joby

- S_MNP_IMPORT

### POS

Joby importujące poszczególne pliki, gdzie XXXXX, YYYYY to nazwy dla konkretnych źródeł lub plików

- J_POS_IMPORT_XXXXX
- J_POS_IMPORT_YYYYY

Sekwencja spinająca i uruchamiająca joby

- J_POS_IMPORT
